C -> S
OPTIONS rtsp://rm.bbc.co.uk/radio4/factual/wed0902_20060104.ra RTSP/1.0
CSeq: 2
User-Agent: LibVLC/2.1.0 (LIVE555 Streaming Media v2012.12.18)

S -> C
RTSP/1.0 200 OK
CSeq: 2
Date: Mon, 09 Jun 2014 13:07:39 GMT
Server: Helix Server Version 11.1.7.3406 (linux-rhel4-i686) (RealServer compatible)
Public: OPTIONS, DESCRIBE, ANNOUNCE, PLAY, PAUSE, SETUP, GET_PARAMETER, SET_PARAMETER, TEARDOWN
TurboPlay: 1
RealChallenge1: eeb14262a7a02fc7cc4feaefc5aa62d2
StatsMask: 3

C -> S
DESCRIBE rtsp://rm.bbc.co.uk/radio4/factual/wed0902_20060104.ra RTSP/1.0
CSeq: 3
User-Agent: LibVLC/2.1.0 (LIVE555 Streaming Media v2012.12.18)
Accept: application/sdp

S -> C
RTSP/1.0 200 OK
CSeq: 3
Date: Mon, 09 Jun 2014 13:07:39 GMT
Last-Modified: Wed, 04 Jan 2006 09:46:26 GMT
Content-base: rtsp://rm.bbc.co.uk/radio4/factual/wed0902_20060104.ra/
Vary: User-Agent, ClientID
Content-type: application/sdp
x-real-usestrackid: 1
Content-length: 3068

v=0
o=- 1136367986 1136367986 IN IP4 212.58.252.8
s=Wednesday 09:02
i=BBC Radio 4 (C) British Broadcasting Corporation 2006
c=IN IP4 0.0.0.0
t=0 0
a=SdpplinVersion:1610641560
a=StreamCount:integer;1
a=control:*
a=IsRealDataType:integer;1
a=Flags:integer;18
a=Title:buffer;"V2VkbmVzZGF5IDA5OjAyAA=="
a=Author:buffer;"QkJDIFJhZGlvIDQA"
a=Copyright:buffer;"KEMpIEJyaXRpc2ggQnJvYWRjYXN0aW5nIENvcnBvcmF0aW9uIDIwMDYA"
a=range:npt=0-2582.056000
m=audio 0 RTP/AVP 101
b=AS:49
b=RR:1653
b=RS:551
a=control:streamid=0
a=range:npt=0-2582.056000
a=length:npt=2582.056000
a=rtpmap:101 x-pn-realaudio/1000
a=fmtp:101 
a=mimetype:string;"audio/x-pn-realaudio"
a=Helix-Adaptation-Support:1
a=MaxBitRate:integer;44100
a=AvgBitRate:integer;44100
a=MaxPacketSize:integer;640
a=AvgPacketSize:integer;640
a=StartTime:integer;0
a=EndOneRuleEndAll:integer;1
a=Preroll:integer;4642
a=ActualPreroll:integer;2321
a=SeekGreaterOnSwitch:integer;0
a=MinimumSwitchOverlap:integer;200
a=EndTime:integer;2579969
a=OpaqueData:buffer;"TUxUSQAMAAUABQACAAIAAQABAAAAAAADAAMABAAEAAYAAABeLnJh/QAFAAAucmE1f///3QAFAAAATgATAAACWABlzoAAAl2fAAAAAAAKAlgAPAAAAABWIgAAViIAAAAQAAJnZW5yY29vawECAAAAAAAQAQAAAwQAABcAAAAAAAEAAwAAAF4ucmH9AAUAAC5yYTUAAAAQAAUAAABOABEAAAHgAFFyAAAB5H8AAAAAAAoB4AAwAAAAAFYiAABWIgAAABAAAmdlbnJjb29rAQIAAAAAABABAAADBAAAEAAAAAAAAQADAAAAVi5yYf0ABQAALnJhNQAAABAABQAAAEYAAQAAAWAANkUAAAFC/wAAAAAACAFgACAAAAAAKxEAACsRAAAAEAABZ2VucmNvb2sBAgAAAAAACAEAAAEBAAAMAAAAXi5yYf0ABQAALnJhNQAAABAABQAAAE4AFQAAAdEAna/QAAOqtwAAAAAAEAHRAF0AAAAArEQAAKxEAAAAEAACZ2VucmNvb2sBAgAAAAAAEAEAAAMIAAAgAAAAAAACAAQAAABeLnJh/QAFAAAucmE1AAAAEAAFAAAATgAXAAACgADZCAAABQv+AAAAAAAQAoAAgAAAAACsRAAArEQAAAAQAAJnZW5yY29vawECAAAAAAAQAQAAAwgAACUAAAAAAAIABAAAAF4ucmH9AAUAAC5yYTV////dAAUAAABOABMAAAJYAGXOgAACXZ8AAAAAAAoCWAA8AAAAAFYiAABWIgAAABAAAmdlbnJjb29rAQIAAAAAABABAAADBAAAFwAAAAAAAQAD"
a=RMFF 1.0 Flags:buffer;"AAwAAgAAAAIAAAACAAAAAgAAAAIAAAACAAA="
a=StreamName:string;"audio/x-pn-multirate-realaudio logical stream"
a=intrinsicDurationType:string;"intrinsicDurationContinuous"
a=ASMRuleBook:string;"#($OldPNMPlayer),AverageBandwidth=20672,priority=5,PNMKeyframeRule=T;#($OldPNMPlayer),AverageBandwidth=0,priority=5,PNMNonKeyframeRule=T;#($Bandwidth < 16538),AverageBandwidth=11025,Priority=5;#($Bandwidth < 16538),AverageBandwidth=0,Priority=5,OnDepend=\"2\", OffDepend=\"2\";#($Bandwidth >= 16538) && ($Bandwidth < 20672),AverageBandwidth=16538,Priority=5;#($Bandwidth >= 16538) && ($Bandwidth < 20672),AverageBandwidth=0,Priority=5,OnDepend=\"4\", OffDepend=\"4\";#($Bandwidth >= 20672) && ($Bandwidth < 32041),AverageBandwidth=20672,Priority=5;#($Bandwidth >= 20672) && ($Bandwidth < 32041),AverageBandwidth=0,Priority=5,OnDepend=\"6\", OffDepend=\"6\";#($Bandwidth >= 32041) && ($Bandwidth < 44100),AverageBandwidth=32041,Priority=5;#($Bandwidth >= 32041) && ($Bandwidth < 44100),AverageBandwidth=0,Priority=5,OnDepend=\"8\", OffDepend=\"8\";#($Bandwidth >= 44100),AverageBandwidth=44100,Priority=5;#($Bandwidth >= 44100),AverageBandwidth=0,Priority=5,OnDepend=\"10\", OffDepend=\"10\";"

C -> S
OPTIONS rtsp://rm.bbc.co.uk:554 RTSP/1.0
CSeq: 1
User-Agent: RealMedia Player Version 6.0.9.1235 (linux-2.0-libc6-i386-gcc2.95)
ClientChallenge: 9e26d33f2984236010ef6253fb1887f7
PlayerStarttime: [28/03/2003:22:50:23 00:00]
CompanyID: KnKV4M4I/B2FjJ1TToLycw==
GUID: 00000000-0000-0000-0000-000000000000
RegionData: 0
ClientID: Linux_2.4_6.0.9.1235_play32_RN01_EN_586

S -> C
RTSP/1.0 200 OK
CSeq: 1
Date: Mon, 09 Jun 2014 13:07:39 GMT
Server: Helix Server Version 11.1.7.3406 (linux-rhel4-i686) (RealServer compatible)
Public: OPTIONS, DESCRIBE, ANNOUNCE, PLAY, PAUSE, SETUP, GET_PARAMETER, SET_PARAMETER, TEARDOWN
TurboPlay: 1
RealChallenge1: c40cd2050aac1a562e86f6eb5653f330
StatsMask: 3

C -> S
DESCRIBE rtsp://rm.bbc.co.uk:554/radio4/factual/wed0902_20060104.ra RTSP/1.0
Cseq: 2
Accept: application/sdp
Bandwidth: 10485800
GUID: 00000000-0000-0000-0000-000000000000
RegionData: 0
ClientID: Linux_2.4_6.0.9.1235_play32_RN01_EN_586
SupportsMaximumASMBandwidth: 1
Language: en-US
Require: com.real.retain-entity-for-setup

S -> C
RTSP/1.0 200 OK
Cseq: 2
Date: Mon, 09 Jun 2014 13:07:39 GMT
Last-Modified: Wed, 04 Jan 2006 09:46:26 GMT
Content-base: rtsp://rm.bbc.co.uk:554/radio4/factual/wed0902_20060104.ra/
ETag: 2022036201-2
Vary: User-Agent, ClientID
Content-type: application/sdp
x-real-usestrackid: 1
Content-length: 3024

v=0
o=- 1136367986 1136367986 IN IP4 212.58.252.8
s=Wednesday 09:02
i=BBC Radio 4 (C) British Broadcasting Corporation 2006
t=0 0
a=SdpplinVersion:1610641560
a=StreamCount:integer;1
a=IsRealDataType:integer;1
a=Flags:integer;18
a=Title:buffer;"V2VkbmVzZGF5IDA5OjAyAA=="
a=Author:buffer;"QkJDIFJhZGlvIDQA"
a=Copyright:buffer;"KEMpIEJyaXRpc2ggQnJvYWRjYXN0aW5nIENvcnBvcmF0aW9uIDIwMDYA"
a=range:npt=0-2582.056000
m=audio 0 RTP/AVP 101
b=AS:49
b=RR:1653
b=RS:551
a=control:streamid=0
a=range:npt=0-2582.056000
a=length:npt=2582.056000
a=rtpmap:101 x-pn-realaudio/1000
a=mimetype:string;"audio/x-pn-realaudio"
a=Helix-Adaptation-Support:1
a=MaxBitRate:integer;44100
a=AvgBitRate:integer;44100
a=MaxPacketSize:integer;640
a=AvgPacketSize:integer;640
a=StartTime:integer;0
a=EndOneRuleEndAll:integer;1
a=Preroll:integer;4642
a=ActualPreroll:integer;2321
a=SeekGreaterOnSwitch:integer;0
a=MinimumSwitchOverlap:integer;200
a=EndTime:integer;2579969
a=OpaqueData:buffer;"TUxUSQAMAAUABQACAAIAAQABAAAAAAADAAMABAAEAAYAAABeLnJh/QAFAAAucmE1f///3QAFAAAATgATAAACWABlzoAAAl2fAAAAAAAKAlgAPAAAAABWIgAAViIAAAAQAAJnZW5yY29vawECAAAAAAAQAQAAAwQAABcAAAAAAAEAAwAAAF4ucmH9AAUAAC5yYTUAAAAQAAUAAABOABEAAAHgAFFyAAAB5H8AAAAAAAoB4AAwAAAAAFYiAABWIgAAABAAAmdlbnJjb29rAQIAAAAAABABAAADBAAAEAAAAAAAAQADAAAAVi5yYf0ABQAALnJhNQAAABAABQAAAEYAAQAAAWAANkUAAAFC/wAAAAAACAFgACAAAAAAKxEAACsRAAAAEAABZ2VucmNvb2sBAgAAAAAACAEAAAEBAAAMAAAAXi5yYf0ABQAALnJhNQAAABAABQAAAE4AFQAAAdEAna/QAAOqtwAAAAAAEAHRAF0AAAAArEQAAKxEAAAAEAACZ2VucmNvb2sBAgAAAAAAEAEAAAMIAAAgAAAAAAACAAQAAABeLnJh/QAFAAAucmE1AAAAEAAFAAAATgAXAAACgADZCAAABQv+AAAAAAAQAoAAgAAAAACsRAAArEQAAAAQAAJnZW5yY29vawECAAAAAAAQAQAAAwgAACUAAAAAAAIABAAAAF4ucmH9AAUAAC5yYTV////dAAUAAABOABMAAAJYAGXOgAACXZ8AAAAAAAoCWAA8AAAAAFYiAABWIgAAABAAAmdlbnJjb29rAQIAAAAAABABAAADBAAAFwAAAAAAAQAD"
a=RMFF 1.0 Flags:buffer;"AAwAAgAAAAIAAAACAAAAAgAAAAIAAAACAAA="
a=StreamName:string;"audio/x-pn-multirate-realaudio logical stream"
a=intrinsicDurationType:string;"intrinsicDurationContinuous"
a=ASMRuleBook:string;"#($OldPNMPlayer),AverageBandwidth=20672,priority=5,PNMKeyframeRule=T;#($OldPNMPlayer),AverageBandwidth=0,priority=5,PNMNonKeyframeRule=T;#($Bandwidth < 16538),AverageBandwidth=11025,Priority=5;#($Bandwidth < 16538),AverageBandwidth=0,Priority=5,OnDepend=\"2\", OffDepend=\"2\";#($Bandwidth >= 16538) && ($Bandwidth < 20672),AverageBandwidth=16538,Priority=5;#($Bandwidth >= 16538) && ($Bandwidth < 20672),AverageBandwidth=0,Priority=5,OnDepend=\"4\", OffDepend=\"4\";#($Bandwidth >= 20672) && ($Bandwidth < 32041),AverageBandwidth=20672,Priority=5;#($Bandwidth >= 20672) && ($Bandwidth < 32041),AverageBandwidth=0,Priority=5,OnDepend=\"6\", OffDepend=\"6\";#($Bandwidth >= 32041) && ($Bandwidth < 44100),AverageBandwidth=32041,Priority=5;#($Bandwidth >= 32041) && ($Bandwidth < 44100),AverageBandwidth=0,Priority=5,OnDepend=\"8\", OffDepend=\"8\";#($Bandwidth >= 44100),AverageBandwidth=44100,Priority=5;#($Bandwidth >= 44100),AverageBandwidth=0,Priority=5,OnDepend=\"10\", OffDepend=\"10\";"

C -> S
SETUP rm.bbc.co.uk/radio4/factual/wed0902_20060104.ra/streamid=0 RTSP/1.0
Cseq: 3
RealChallenge2: 2a35c9a7cf40e7f25d9e604dacd5d93101d0a8e3, sd=2cce56ad
If-Match: 2022036201-2
Transport: x-pn-tng/tcp;mode=play,rtp/avp/tcp;unicast;mode=play

S -> C
RTSP/1.0 200 OK
Cseq: 3
Date: Mon, 09 Jun 2014 13:07:39 GMT
Session: 2022036201-2;timeout=80
Reconnect: true
RDTFeatureLevel: 0
StatsMask: 3
RealChallenge3: cf3a2884edd4b5fe7cb9f536d056b9e04f213d09,sdr=c2eb7fdb
Transport: x-pn-tng/tcp;interleaved=0

C -> S
SET_PARAMETER rtsp://rm.bbc.co.uk:554/radio4/factual/wed0902_20060104.ra RTSP/1.0
Cseq: 4
Session: 2022036201-2;timeout=80
Subscribe: stream=0;rule=10,stream=0;rule=11

S -> C
RTSP/1.0 200 OK
Cseq: 4
Date: Mon, 09 Jun 2014 13:07:39 GMT
Session: 2022036201-2;timeout=80

C -> S
PLAY rtsp://rm.bbc.co.uk:554/radio4/factual/wed0902_20060104.ra RTSP/1.0
Cseq: 5
Session: 2022036201-2;timeout=80
Range: npt=0-

S -> C
RTSP/1.0 200 OK
Cseq: 5
Date: Mon, 09 Jun 2014 13:07:39 GMT
Session: 2022036201-2;timeout=80
RTP-Info: url=rm.bbc.co.uk/radio4/factual/wed0902_20060104.ra/streamid=0;seq=0;rtptime=0
Range: npt=0-

... RDT data ...



 




