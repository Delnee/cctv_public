/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.organise;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.effanee.packetize.h264.H264Depacketize;
import net.effanee.packetize.h264.H264FileWriter;
import net.effanee.packetize.h264.H264Packet;
import net.effanee.packetize.rtp.RtpPacket;
import net.effanee.protocol.rtp.client.RtpFileReader;
import net.effanee.protocol.rtp.client.RtpFileWriter;
import net.effanee.protocol.rtp.client.RtpFileWriterListener;

/**
 * Register as a listener for the creation of new RTP output files and wait for
 * their arrival.
 * 
 * Iterate the list of new files. Read the RTP file, defragment the NAL
 * Units and write the H.264 stream as a 0x00 00 00 01 prefixed stream of
 * NAL Units, into the required directory structure. Using the UPnP devices
 * friendly name. Delete the RTP file after processing.
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class RtpProcessor implements Runnable, RtpFileWriterListener{

	private static final Logger LOGGER = Logger.getLogger(RtpProcessor.class.getName());
	
	private final RtpFileWriter rtpFileWriter;
	private final Path path;
	private final String friendlyName;
	private final long maxDiscSpace;
	
	private final Queue<Path> newFiles = new ArrayBlockingQueue<>(10);
	private final H264Depacketize h264Depacketize = new H264Depacketize();
	private final H264FileWriter writer = new H264FileWriter();
	private final DirectoryStructure directoryStructure = new DirectoryStructure();
	
	// Used to ensure owning thread waits sensibly for the arrival of new files.
	private final Lock rLock = new ReentrantLock();
	private final Condition processFile= rLock.newCondition(); 
	private final AtomicBoolean isProcessingFile = new AtomicBoolean(false);
	
	// A reference to the owning thread.
	private Thread thisThread;
	
	/**
	 * Create a new RTP processor.
	 * 
	 * @param rtpFileWriter - the RtpFileWriter to register with to receive
	 * notifications when new files are generated.
	 * @param path - the directory where the H.264 files will be written.
	 * @param friendlyName - the name of the camera.
	 * @param maxDiscSpace - the maximum disc space the H.264 files may occupy.
	 */
	public RtpProcessor(RtpFileWriter rtpFileWriter, Path path, String friendlyName, long maxDiscSpace) {
		
		this.rtpFileWriter = rtpFileWriter;
		this.path = path;
		this.friendlyName = friendlyName;
		this.maxDiscSpace = maxDiscSpace;
	}

	/**
	 * Add the new file to the processing queue.
	 */
	@Override
	public void newFile(Path path) {
		
		newFiles.add(path);
		
		// Wake the process method.
		rLock.lock();
		try {
			// If files are not currently being processed, start processing them.
			if (!isProcessingFile.get()) {
				
				LOGGER.log(Level.FINEST, "A new file "
						+ path.getFileName()
						+ " has arrived.");
				
				isProcessingFile.getAndSet(true);

				// Start processing files.
				processFile.signalAll();
			}
			// If files are currently being processed, just add to the queue.
			else {
				LOGGER.log(Level.FINEST, "A new file "
						+ path.getFileName()
						+ " has arrived and been added to the queue.");
				return;
			}
		}
		finally {
			rLock.unlock();
		}
	}
	
	/**
	 * Register with the RtpFileWriter to receive notifications when new files
	 * are generated. Start the processing loop.
	 */
	@Override
	public void run() {

		// At this point we can get a reference to the executing thread,
		// which can be used later to stop the thread.
		thisThread = Thread.currentThread();
		
		// Register an interest in receiving notifications when a new file is
		// created.
		rtpFileWriter.register(this);
		
		// Process files until interrupted.
		process();
	}

	/**
	 * Wait for files to arrive and then begin processing them.
	 */
	private void process(){
		
		while (!Thread.currentThread().isInterrupted()) {

			rLock.lock();
			try {
				while (!isProcessingFile.get()){
					LOGGER.log(Level.FINEST, "Waiting for a new file to arrive.");
					processFile.await();
				}
			}
			catch (InterruptedException e) {
				// Assume the exception is caused by the stop() method being
				// invoked and exit.
				LOGGER.log(Level.INFO, "The thread was expectedly interrupted.");
				break;
			}
			catch (Exception e) {
				LOGGER.log(Level.SEVERE, "The thread experienced an unexpected exception.", e);
				break;
			}
			finally {
				rLock.unlock();
			}
			
			// Process the files.
			try{
				LOGGER.log(Level.FINEST, "Started processing files.");
				processFiles();
				LOGGER.log(Level.FINEST, "Stopped processing files.");
			}
			finally{
				isProcessingFile.getAndSet(false);	
			}
		}
	}

	/**
	 * Iterate the list of new files. Read the RTP file, defragment the NAL
	 * Units and write the H.264 stream as a 0x00 00 00 01 prefixed stream of
	 * NAL Units, into the required directory structure. Delete the RTP file
	 * after processing. 
	 */
	private void processFiles(){
		
		// Process all waiting files.
		Path rtpPath = null;
		Path h264Path = path;
		while((rtpPath = newFiles.poll()) != null){

			// Get the RTP packets.
			List<RtpPacket>rtpPackets = new RtpFileReader().getRtpPackets(rtpPath);
			
			// Defragment the packets.
			List<H264Packet> h264Packets = h264Depacketize.getDefragmentedPackets(rtpPackets);
			
			// Append/write the file.
			try {
				// Choke the invocations of the purge method, to only run when
				// the file name changes.
				if(!h264Path.equals(directoryStructure.getFile(path, rtpPath.getFileName().toString(), friendlyName))){
					if(directoryStructure.getDirectorySize(path) > maxDiscSpace){
						directoryStructure.purge(path);
					}					
				}
				
				h264Path = directoryStructure.getFile(path, rtpPath.getFileName().toString(), friendlyName);
				writer.writeFile(h264Packets, h264Path);
			}
			catch (IOException e) {
				LOGGER.log(Level.SEVERE, "Could not write the file "
						+ h264Path
						+ " due to an unexpected exception.", e);
			}
			
			// Delete the temporary RTP file.
			rtpPath.toFile().delete();
		}
	}
	
	/**
	 * Stop the file processing.
	 */
	public void stop(){
		
		// Don't unregister, the rtpFileWriter may have already been stopped.
		// rtpFileWriter.unregister(this);
		
		// Wait for 1 second to be notified of the last file.
		try {Thread.sleep(1000);}
		catch (InterruptedException e) {e.printStackTrace();}
		
		// Give the file 1 second to be written.
		int wait = 0;
		while(isProcessingFile.get() & wait++ < 10){
			try {Thread.sleep(100);}
			catch (InterruptedException e) {e.printStackTrace();}
		}

		thisThread.interrupt();
		LOGGER.log(Level.FINEST, "Closed RTP output file processor.");
	}
}
