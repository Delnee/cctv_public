/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.organise;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>Used to manage the directory structure of the H.264 output.
 * 
 * Files are stored in a hierarchical structure based on the time the output was
 * created:-</p>
 * <code>
 * 	baseDirectory/2016/04/26/18 - baseDirectory/year/month/day/hour
 * </code>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class DirectoryStructure {

	private static final Logger LOGGER = Logger.getLogger(DirectoryStructure.class.getName());
	
	private static final String RTP_FILE_EXTENSION = ".rtp";
	private static final String H264_FILE_EXTENSION = ".h264";
	private static final double PURGE_PERCENTAGE = 0.1;
	
	private final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH-mm-ss");
	
	/**
	 * A new object to structure files.
	 */
	public DirectoryStructure() {
	}
	
	/**
	 * <p>Create a new file name, if one does not already exist, based on the
	 * supplied file name. The supplied file name must be in the format:-</p>
	 * <code>
	 * 		2016-04-26T18-25-51.rtp
	 * </code>
	 * <p>The date is extracted from the name and used to construct the directory
	 * structure:-</p>
	 * <code>
	 * 		baseDirectory/2016/04/26/18 - baseDirectory/year/month/day/hour
	 * </code>
	 * <p>A file is returned within this directory based on the friendly name:-</p>
	 * <code>
	 * 		friendlyname.h264
	 * </code>
	 * 
	 * @param baseDirectory - the directory within which to create a structure.
	 * @param fileName - the RTP file name.
	 * @param friendlyName - the UPnP friendly name of the device.
	 * @return a file.
	 * @throws IOException
	 */
	public Path getFile(Path baseDirectory, String fileName, String friendlyName) throws IOException{
		
		fileName = stripExtension(fileName);
		Calendar fileDate = parseDate(fileName);
	    Path directoryPath = createDirectory(baseDirectory, fileDate);
	    Path filePath = createFile(directoryPath, friendlyName);
	    
		return filePath;
	}
	
	/**
	 * <p>Removes an rtp file extension.</p>
	 * <code>
	 * 		2016-04-26T18-25-51.rtp to 2016-04-26T18-25-51
	 * </code>
	 * @param fileName - the name to strip.
	 * @return the extensionless file name.
	 */
	private String stripExtension(String fileName){

		if(fileName.endsWith(RTP_FILE_EXTENSION)){
			return fileName.substring(0, fileName.length() - RTP_FILE_EXTENSION.length());
		}
		else{
			return fileName;
		}
	}
	
	/**
	 * Parse a string into a date.
	 * 
	 * @param fileName - the file name string.
	 * @return the date.
	 */
	private Calendar parseDate(String fileName){

		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime(dateFormatter.parse(fileName));
			return cal;
		}
		catch (ParseException e) {
			LOGGER.log(Level.SEVERE, "Could not convert the file name " + fileName
										+ " into a date.", e);
			return null;
		}
	}

	/**
	 * <p>Create, or return an existing directory, based on the supplied date.</p>
	 * <code>
	 * 		2016-04-26T18-25-51 to baseDirectory/year/month/day/hour
	 * </code>
	 * @param baseDirectory - the directory to start the creation.
	 * @param fileDate - the date to base the structure upon.
	 * @return a new or existing directory.
	 */
	private Path createDirectory(Path baseDirectory, Calendar fileDate){

	    String subDirectory = String.format("/%1$tY/%1$tm/%1$td/%1$tH", fileDate);
	    
	    Path directoryPath = FileSystems.getDefault().getPath(baseDirectory + subDirectory);
	    
	    if(!directoryPath.toFile().exists()){
	    	directoryPath.toFile().mkdirs();
	    }
	    
	    return directoryPath;
	}
	
	/**
	 * <p>Create, or return an existing file, based on the supplied UPnP friendly
	 * name:-</p>
	 * <code>
	 * 		friendlyname.h264
	 * </code>
	 * @param baseDirectory - the directory within which the file should be 
	 * created.
	 * @param friendlyName - the basis of the file name.
	 * @return a new or existing file.
	 * @throws IOException
	 */
	private Path createFile(Path baseDirectory, String friendlyName) throws IOException{

	    Path filePath = FileSystems.getDefault().getPath(baseDirectory + "/" + friendlyName + H264_FILE_EXTENSION);
	    
	    if(!filePath.toFile().exists()){
			filePath.toFile().createNewFile();
			
			LOGGER.log(Level.INFO, "Created new file " + filePath);
	    }
	    
	    return filePath;
	}
	
	/**
	 * Purge the directory where the output files are stored. Files are purged
	 * based on their size. When this method is invoked, 10% of the files by
	 * size are deleted. The oldest files are deleted first.
	 * 
	 * @param baseDirectory - the directory to purge.
	 */
	public void purge(Path baseDirectory){
		
		// Get the files and sort them into a list, where the oldest appear at
		// the start of the list.
		List<FileAttributes> files = getFiles(baseDirectory);
		Collections.sort(files, (file1, file2) -> Long.compare(
													file1.getCreationTime(),
													file2.getCreationTime()));
		
		// Get the list of files to delete.
		List<FileAttributes> filesToDelete = getFilesToDelete(files);

		// Delete the files.
		deleteFiles(filesToDelete, baseDirectory);
	}
	
	/**
	 * Get the size of the supplied directory.
	 * 
	 * @param baseDirectory - the directory to examine.
	 * @return the sum of the size of the files in the directory and its
	 * sub-directories.
	 */
	public long getDirectorySize(Path baseDirectory){
		
		List<FileAttributes> files = getFiles(baseDirectory);
		
        return getFilesSize(files);
    }
	
	/**
	 * Get a list of files under the supplied directory.
	 * 
	 * @param baseDirectory - the directory to search under.
	 * @return a list of file attributes, for the files under the supplied path.
	 */
	private List<FileAttributes> getFiles(Path baseDirectory){
		
		final List<FileAttributes> files = new ArrayList<>();
		
        try {
			Files.walkFileTree(baseDirectory, new SimpleFileVisitor<Path>(){

				@Override
				public FileVisitResult visitFile(Path file,
						BasicFileAttributes attrs) throws IOException {
					
					// Store the file attributes.
					files.add(new FileAttributes(attrs.creationTime().toMillis(), file.toString(), attrs.size()));
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFileFailed(Path file,
						IOException e) throws IOException {
					
					LOGGER.log(Level.SEVERE, file + " could not be visited.", e);
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult postVisitDirectory(Path dir,
						IOException e) throws IOException {

					if (e != null){
						LOGGER.log(Level.SEVERE, "The iteration of the directory " + dir + " terminated prematurely.", e);
					}
			        return FileVisitResult.CONTINUE;
				}
			});
		}
		catch (IOException e) {
			LOGGER.log(Level.SEVERE, e.getMessage());
		}
        
        return files;
    }

	/**
	 * Return a list of files to delete, consisting of 10% of the total size of
	 * the files supplied. The supplied files should be in date order, with the
	 * oldest first in the list. So, the returned list will consist of the
	 * oldest files that make up 10% of the total.
	 * 
	 * @param files - the list of files.
	 * @return the 10%, by size of files to delete.
	 */
	private List<FileAttributes> getFilesToDelete(List<FileAttributes> files){

		// Remove 10%.
		long filesSelected = 0l;
		long tenPercent = Math.round(getFilesSize(files) * PURGE_PERCENTAGE);
		List<FileAttributes> toDelete = new ArrayList<>();
		
		for(FileAttributes fileAttrs: files){
		
			toDelete.add(fileAttrs);
			filesSelected += fileAttrs.getSize();
			if(filesSelected > tenPercent){
				// Found 10%.
				break;
			}
		}
        
        return toDelete;
    }
	
	/**
	 * Delete the supplied files and any parent directories that are empty.
	 * 
	 * @param files - the list of files to delete.
	 * @param baseDirectory - the directory not to delete.
	 */
	private void deleteFiles(List<FileAttributes> files, Path baseDirectory){
		
		Path filePath = null;
		File file = null;
		File parentDirectory = null;
		
		for(FileAttributes fileAttrs: files){
			
			// Get the file and delete it.
			filePath = FileSystems.getDefault().getPath(fileAttrs.getPath());
			file = filePath.toFile();
			parentDirectory = file.getParentFile();
			file.delete();
			
			// Delete any empty parent directories.
			while(parentDirectory.isDirectory() && parentDirectory.list().length == 0){
				
				// Don't delete the base directory.
				if(!parentDirectory.equals(baseDirectory.toFile())){
					LOGGER.log(Level.INFO, "Deleting file " + parentDirectory);
					parentDirectory.delete();
				}
				parentDirectory = parentDirectory.getParentFile();
			}
		}
    }
	
	/**
	 * Get the size of the supplied files list.
	 * 
	 * @param files - the list of files to sum.
	 * @return the sum of the files size.
	 */
	private long getFilesSize(List<FileAttributes> files){
		
		long filesSize = 0l;
		
		for(FileAttributes fileAttrs: files){
			filesSize += fileAttrs.getSize();
		}
        
        return filesSize;
    }
	
	/**
	 * Store the file attributes of interest.
	 *
	 * @author Francis J. Hammell [hammell.francis@gmail.com]
	 * <p><em>Copyright</em>
	 * \u00A9 Francis J. Hammell 2016</p>
	 */
	private class FileAttributes{
		
		private final long creationTime;
		private final String path;
		private final long size;
		
		FileAttributes(long creationTime, String path, long size){
			
			this.creationTime = creationTime;
			this.path = path;
			this.size = size;
		}

		long getCreationTime() {
			return creationTime;
		}

		String getPath() {
			return path;
		}

		long getSize() {
			return size;
		}
		
		@Override
		public String toString() {
			return "FileAttributes ["
					+ ("creationTime=" + creationTime + ",")
					+ ("path=" + path + ",")
					+ ("size=" + size)
					+ "]";
		}
	}
	// End FileAttributes class.
}
