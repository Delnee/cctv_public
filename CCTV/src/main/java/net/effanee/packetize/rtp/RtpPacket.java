/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.packetize.rtp;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.effanee.packetize.h264.H264Packet;

/**
 * <p>Extract the RTP packet payload, (which in this case is an H.264 Network
 * Abstraction Layer, (NAL), unit).</p>
 * 
 * <h2>RFC 3550 - RTP: A Transport Protocol for Real-Time Applications</h2>
 *
 * <p>The RTP header has the following format:-</p>
 * <pre>
 *  0                   1                   2                   3
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |V=2|P|X|  CC  |M|     PT      |          sequence number       |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                          timestamp                            |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |            synchronization source (SSRC) identifier           |
 * +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
 * |             contributing source (CSRC) identifiers            |
 * |                            ....                               |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * </pre>
 * 
 * <p>The first twelve octets are present in every RTP packet, while the list of
 * CSRC identifiers is present only when inserted by a mixer.</p>
 *
 * <pre>
 * field                                expected
 * version (V): 2 bits                  10
 * padding (P): 1 bit                   0 or 1
 * extension (X): 1 bit                 0
 * CSRC count (CC): 4 bits              0000
 * marker (M): 1 bit                    0 or 1
 * payload type (PT): 7 bits            96 (m=video 0 RTP/AVP 96)
 * sequence number: 16 bits             increments by one for each RTP packet sent
 * timestamp: 32 bits                   a clock that increments monotonically
 * SSRC: 32 bits                        identifies the synchronization source
 * </pre>
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class RtpPacket {

	private static final Logger LOGGER = Logger.getLogger(RtpPacket.class.getName());
	
	private static final byte VERSION = (byte) 0b00000010;
	
	// Packet.
	byte[] packet = null;
	
	// Packet fields.
	private byte version = 0;
	private byte padding = 0;
	private byte extension = 0;
	private byte csrcCount = 0;
	private byte marker = 0;
	private byte payloadType = 0;
	private int sequenceNumber = 0;
	private long timestamp = 0;
	private long ssrc = 0;
	
	// Packet properties.
	private int startPayload = 0;
	private int endPayload = 0;
	private int packetLength = 0;
	
	private H264Packet h264Packet = null;
	
	/**
	 * Create a new RTP packet object.
	 * 
	 * @param packet - the packet bytes.
	 */
	public RtpPacket(byte[] packet) {

		this.packet = packet;
		parsePacket();
	}

	/**
	 * Parse the packet, extracting the RTP fields and storing the packet
	 * properties.
	 * 
	 * @param packet - An RTP packet.
	 */
	private void parsePacket(){

		// Ensure header is at least 12 bytes.
		if(!(packet.length > 11)){
			LOGGER.log(Level.SEVERE, "The RTP packet header was only " + packet.length + " bytes long.");
			throw new IllegalArgumentException();
		}
		
		// Get bits 0-1, version.
		if(getBits(packet[0], 0, 1) != VERSION){
			LOGGER.log(Level.SEVERE, "The RTP version was " + getBits(packet[0], 0, 1)
					+ " and it should be " + VERSION + ".");
			throw new IllegalArgumentException();
		}

		// Get bit 2, padding.
		padding = getBits(packet[0], 2, 2);
		
		// Get bit 3, extension.
		extension = getBits(packet[0], 3, 3);
		
		// Get bits 4-7, csrcCount.
		csrcCount = getBits(packet[0], 4, 7);
		
		// Get bit 8, marker.
		marker = getBits(packet[1], 0, 0);
		
		// Get bits 4-7, payload type.
		payloadType = getBits(packet[1], 1, 7);
		
		// Get bytes 2-3, sequenceNumber.
		sequenceNumber = ((packet[2] & 0xFF) << 8) | ((packet[3] & 0xFF) << 0);
		
		// Get bytes 4-7, timestamp.
		timestamp = bytesToInt(Arrays.copyOfRange(packet, 4, 8));
		
		// Get bytes 8-11, ssrc.
		ssrc = bytesToInt(Arrays.copyOfRange(packet, 8, 12));
		
		// Ignore possible further contributing source (CSRC) identifier fields.
		
		// Calculate position of the first byte of the payload.
		startPayload = 12 + (1 * csrcCount);
		
		// Calculate position of the last byte of the payload.
		if (padding == 0){
			endPayload = packet.length - 1;
		}
		else{
			int paddingCount = packet[packet.length - 1];
			endPayload = packet.length - 1 - paddingCount;
		}
		
		// Store the packet length.
		packetLength = packet.length;
		
		// Create the H.264 Network Abstraction Layer, (NAL), unit.
		h264Packet = new H264Packet(this.getPayload(), getSequenceNumber());
	}

	/**
	 * Get the H.264 Network Abstraction Layer, (NAL), unit.
	 * 
	 * @return the H264Packet.
	 */
	public H264Packet getH264Packet() {
		return h264Packet;
	}

	/**
	 * Get the RTP packet sequence number.
	 * 
	 * @return the sequenceNumber.
	 */
	public int getSequenceNumber() {
		return sequenceNumber;
	}

	/**
	 * Get the RTP packet payload first byte position. Starting at position 0.
	 * 
	 * @return the startPayload.
	 */
	public int getStartPayload() {
		return startPayload;
	}

	/**
	 * Get the RTP packet payload last byte position. Starting at position 0.
	 * 
	 * @return the endPayload.
	 */
	public int getEndPayload() {
		return endPayload;
	}

	/**
	 * Get a copy of the payload from the encapsulating RTP packet.
	 * 
	 * @return the payload.
	 */
	public byte[] getPayload() {
		
		return Arrays.copyOfRange(packet, getStartPayload(), getEndPayload() + 1);
	}

	/**
	 * Extract a contiguous range of bits from a byte, and return the result as
	 * a byte where the requested bits are right aligned in the new byte.
	 *   
	 * @param b - the byte containing the bits.
	 * @param startBit - the start position of the first contiguous bit, the left
	 * most bit is numbered 0, positions are inclusive.
	 * @param endBit - the start position of the last contiguous bit, the right
	 * most bit is numbered 7, positions are inclusive.
	 * @return the bits, right aligned in the new byte.
	 */
	protected byte getBits(byte b, int startBit, int endBit){
		
		int bits = 0;
		int mask = getBitMask((endBit - startBit) + 1);
		
		// Right align the bits we want.
		bits = (byte)(b >>> (7 - endBit));
		// Remove ones to the left of the bits we want.
		bits = (bits & mask);
		
		return (byte)bits;
	}
	
	/**
	 * Create a bit mask consisting of right aligned ones. This mask is intended
	 * to be used with 8 bit bytes, so the valid parameter values are 1 - 8. 
	 * 
	 * @param noOfBits - a value in the range 1 to 8.
	 * @return a bit mask with the requested number of contiguous ones in the
	 * right most positions.
	 */
	protected int getBitMask(int noOfBits){
		
		int mask = 0;
		for(int i = 0; i < noOfBits; i++){
			mask = (int) (mask + Math.pow(2, i));
		}
		
		return mask;
	}
	
	/**
	 * Convert a 4 byte array into an unsigned integer representation. 
	 * 
	 * @param bytes - the four bytes of the integer, with the most significant
	 * byte first. 
	 * @return the new integer.
	 */
	protected long bytesToInt(byte[] bytes){
		
		// Need to promote to long to get full range of 32 bits.
		long intValue = ((0xFFl & bytes[0]) << 24)
				| ((0xFFl & bytes[1]) << 16)
				| ((0xFFl & bytes[2]) << 8)
				| (0xFFl & bytes[3]);
		
		return intValue;
	}
	
	@Override
	public String toString() {
		return "RtpPacket ["
				+ ("version=" + version + ",")
				+ ("padding=" + padding + ",")
				+ ("extension=" + extension + ",")
				+ ("csrcCount=" + csrcCount + ",")
				+ ("marker=" + marker + ",")
				+ ("payloadType=" + payloadType + ",")
				+ ("sequenceNumber=" + sequenceNumber + ",")
				+ ("timestamp=" + timestamp + ",")
				+ ("ssrc=" + ssrc + ",")
				+ ("startPayload=" + startPayload + ",")
				+ ("endPayload=" + endPayload + ",")
				+ ("packetLength=" + packetLength)
				+ "]";
	}
}
