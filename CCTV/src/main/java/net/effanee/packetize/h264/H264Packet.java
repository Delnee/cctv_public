/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.packetize.h264;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.effanee.packetize.rtp.RtpPacket;

/**
 * <p>Create a H.264 Network Abstraction Layer Unit, (NALU), packet from the
 * supplied bytes and RTP sequence number. in packetization mode 1, the RTP
 * sequence number is used as the NALU decoding order number.</p> 
 * 
 * <p>Assumes:-</p>
 * <ul>
 * 		<li>the RTP packetization mode used = 1.</li>
 * 		<li>only NALU types 7, 8 and 28 (fragmented 1 and 5) are received.</li>
 * </ul>
 * <h2>RFC 6184 - RTP Payload Format for H.264 Video</h2>
 *
 * <h3>1.3. Network Abstraction Layer Unit Types</h3>
 * 
 * <p>The NAL unit type octet has the following format:-</p>
 * 
 * <pre>
 * +---------------+
 * |0|1|2|3|4|5|6|7|
 * +-+-+-+-+-+-+-+-+
 * |F|NRI|  Type   |
 * +---------------+
 * </pre>
 * 
 * <p>The semantics of the components of the NAL unit type octet, as
 * specified in the H.264 specification, are described briefly below:-</p>
 * <pre>
 * F:       1 bit
 *          forbidden_zero_bit. The H.264 specification declares a
 *          value of 1 as a syntax violation.
 * 
 * NRI:     2 bits
 *          nal_ref_idc. A value of 00 indicates that the content of
 *          the NAL unit is not used to reconstruct reference pictures
 *          for inter picture prediction. Such NAL units can be
 *          discarded without risking the integrity of the reference
 *          pictures. Values greater than 00 indicate that the decoding
 *          of the NAL unit is required to maintain the integrity of the
 *          reference pictures.
 * 
 * Type:    5 bits
 *          nal_unit_type. This component specifies the NAL unit
 *          payload type as defined below:-
 *          
 * NAL Unit                                         NAL Reference                           Packetization
 * Type     Content of NAL Unit                     Indicator (NRI)     Packet Type         Mode
 * ------------------------------------------------------------------------------------------------------
 * 0        Unspecified                             -                   Reserved            -
 * 1        Coded slice of a non-IDR picture        10                  Single NAL Unit     0, 1
 * 2        Coded slice data partition A            10                  Single NAL Unit     0, 1
 * 3        Coded slice data partition B            01                  Single NAL Unit     0, 1
 * 4        Coded slice data partition C            01                  Single NAL Unit     0, 1
 * 5        Coded slice of an IDR picture           11                  Single NAL Unit     0, 1
 * 6        Supplemental enhancement information    00                  Single NAL Unit     0, 1
 * 7        Sequence parameter set                  11                  Single NAL Unit     0, 1
 * 8        Picture parameter set                   11                  Single NAL Unit     0, 1
 * 9        Access unit delimiter                   00                  Single NAL Unit     0, 1
 * 10       End of sequence                         00                  Single NAL Unit     0, 1
 * 11       End of stream                           00                  Single NAL Unit     0, 1
 * 12       Filler data                             00                  Single NAL Unit     0, 1
 * 13       Sequence parameter set extension        -                   Single NAL Unit     0, 1
 * 14       Prefix NAL unit                         -                   Single NAL Unit     0, 1
 * 15       Subset sequence parameter set           -                   Single NAL Unit     0, 1
 * 16       Reserved                                -                   Single NAL Unit     0, 1
 * 17       Reserved                                -                   Single NAL Unit     0, 1
 * 18       Reserved                                -                   Single NAL Unit     0, 1
 * 19       Coded slice auxiliary without partition -                   Single NAL Unit     0, 1
 * 20       Coded slice extension                   -                   Single NAL Unit     0, 1
 * 21       Coded slice extension for depth view    -                   Single NAL Unit     0, 1
 * 22       Reserved                                -                   Single NAL Unit     0, 1
 * 23       Reserved                                -                   Single NAL Unit     0, 1
 * 24       Single-time aggregation packet          *                   STAP-A              1
 * 25       Single-time aggregation packet          *                   STAP-B              2
 * 26       Multi-time aggregation packet           *                   MTAP16              2
 * 27       Multi-time aggregation packet           *                   MTAP24              2
 * 28       Fragmentation unit                      $                   FU-A                1, 2
 * 29       Fragmentation unit                      $                   FU-B                2
 * 30       No Recommendation                       -                   Reserved            -
 * 31       No Recommendation                       -                   Reserved            -
 * 
 * Packetization Modes:-
 * 
 * 0 - Single NAL Unit Mode
 * 1 - Non-Interleaved Mode
 * 2 - Interleaved Mode (Optional)
 * 
 * * - NRI MUST be the maximum of all the NAL units carried in the aggregation
 *     packet.
 * $ - NRI field MUST be set according to the value of the NRI field in the
 *     fragmented NAL unit.
 * </pre>
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class H264Packet {

	public enum NalPacketType {

		TYPE_1(1, "Coded slice of a non-IDR picture"),
		TYPE_5(5, "Coded slice of an IDR picture"),
		TYPE_7(7, "Sequence parameter set"),
		TYPE_8(8, "Picture parameter set"),
		TYPE_28(28, "Fragmentation unit"),
		TYPE_0(0, "Other");

		private final int key;
		private final String description;

		private NalPacketType(int key, String description) {
			this.key = key;
			this.description = description;
		}
		
		public int key() {
			return key;
		}

		public String description() {
			return description;
		}
		
		@Override
		public String toString() {
			return "NalPacketType ["
					+ ("enum=" + this.name() + ", ")
					+ ("key=" + key + ", ")
					+ ("description=" + description)
					+ "]";
		}
	}
	
	private static final Logger LOGGER = Logger.getLogger(H264Packet.class.getName());
	
	private static final byte FORRBIDDEN_BIT = (byte) 0b00000000;
	
	// Packets.
	RtpPacket rtpPacket = null;
	byte[] packet = null;
	
	// Packet fields.
	private byte forbiddenBit = 0;
	private byte nalReferenceIndicator = 0;
	private byte packetType = 0;
	
	// Packet properties.
	private int packetLength = 0;
	private int decodingOrderNumber = 0;
	
	/**
	 * Create a new H.264 Network Abstraction Layer Unit, (NALU), object.
	 * 
	 * @param packet - the H264 NAL unit packet.
	 * @param rtpSequenceNumber - the RTP sequence number, which is used in
	 * packetization mode 1, as the H.264 decoding order number.
	 */
	public H264Packet(byte[] packet, int rtpSequenceNumber) {

		this.packet = packet;
		this.decodingOrderNumber = rtpSequenceNumber;
		parsePacket();
	}

	/**
	 * Parse the packet, extracting the NALU fields and storing the packet
	 * properties.
	 */
	private void parsePacket(){
		
		// Ensure header is at least 2 bytes.
		if(!(packet.length > 1)){
			LOGGER.log(Level.SEVERE, "The NALU packet header was only " + packet.length + " bytes long.");
			throw new IllegalArgumentException();
		}
		
		// Get bits 0-1, forbidden zero bit.
		if(getBits(packet[0], 0, 0) != FORRBIDDEN_BIT){
			LOGGER.log(Level.SEVERE, "The NALU forbidden zero bit was set.");
			throw new IllegalArgumentException();
		}

		// Get bits 1 - 2, NAL reference indicator.
		nalReferenceIndicator = getBits(packet[0], 1, 2);
		
		// Get bits 3 - 7, packet type.
		packetType= getBits(packet[0], 3, 7);
		
		// Store the packet length.
		packetLength = packet.length;
	}

	/**
	 * Get a copy of the packet payload.
	 * 
	 * @return a copy of the packet payload.
	 */
	public byte[] getPacket() {
		
		return Arrays.copyOfRange(packet, 0, packet.length);
	}

	/**
	 * Get the forbidden bit.
	 * 
	 * @return the forbiddenBit.
	 */
	public byte getForbiddenBit() {
		return forbiddenBit;
	}

	/**
	 * Get the NAL reference indicator.
	 * 
	 * @return the nalReferenceIndicator.
	 */
	public byte getNalReferenceIndicator() {
		return nalReferenceIndicator;
	}

	/**
	 * Get the packet length.
	 * 
	 * @return the packet length.
	 */
	public int getPacketLength() {

		return packetLength;
	}
	
	/**
	 * Get the decoding order number.
	 * 
	 * @return the decoding order number.
	 */
	public int getDecodingOrderNumber() {

		return decodingOrderNumber;
	}	
	
	/**
	 * Get the packet type.
	 * 
	 * @return the packet type.
	 */
	public byte getPacketTypeNumber() {

		return packetType;
	}
	
	/**
	 * Get the packet type.
	 * 
	 * @return the packet type.
	 */
	public NalPacketType getPacketType() {

		for (NalPacketType nalPacketType: NalPacketType.values()){
			
			if (nalPacketType.key == packetType){
				return nalPacketType;
			}
		}
		return NalPacketType.TYPE_0;
	}

	/**
	 * Extract a contiguous range of bits from a byte, and return the result as
	 * a byte where the requested bits are right aligned in the new byte.
	 *   
	 * @param b - the byte containing the bits.
	 * @param startBit - the start position of the first contiguous bit, the left
	 * most bit is numbered 0, positions are inclusive.
	 * @param endBit - the start position of the last contiguous bit, the right
	 * most bit is numbered 7, positions are inclusive.
	 * @return the bits, right aligned in the new byte.
	 */
	protected byte getBits(byte b, int startBit, int endBit){
		
		int bits = 0;
		int mask = getBitMask((endBit - startBit) + 1);
		
		// Right align the bits we want.
		bits = (byte)(b >>> (7 - endBit));
		// Remove ones to the left of the bits we want.
		bits = (bits & mask);
		
		return (byte)bits;
	}
	
	/**
	 * Create a bit mask consisting of right aligned ones. This mask is intended
	 * to be used with 8 bit bytes, so the valid parameter values are 1 - 8. 
	 * 
	 * @param noOfBits - a value in the range 1 to 8.
	 * @return a bit mask with the requested number of contiguous ones in the
	 * right most positions.
	 */
	protected int getBitMask(int noOfBits){
		
		int mask = 0;
		for(int i = 0; i < noOfBits; i++){
			mask = (int) (mask + Math.pow(2, i));
		}
		
		return mask;
	}
	
	/**
	 * Convert a 4 byte array into an unsigned integer representation. 
	 * 
	 * @param bytes - the four bytes of the integer, with the most significant
	 * byte first. 
	 * @return the new integer.
	 */
	protected long bytesToInt(byte[] bytes){
		
		// Need to promote to long to get full range of 32 bits.
		long intValue = ((0xFFl & bytes[0]) << 24)
				| ((0xFFl & bytes[1]) << 16)
				| ((0xFFl & bytes[2]) << 8)
				| (0xFFl & bytes[3]);
		
		return intValue;
	}
	
	@Override
	public String toString() {
		return "H264Packet ["
				+ ("forbiddenBit=" + forbiddenBit + ",")
				+ ("nalReferenceIndicator=" + nalReferenceIndicator + ",")
				+ ("packetType=" + packetType + ",")
				+ ("decodingOrderNumber=" + decodingOrderNumber + ",")
				+ ("packetLength=" + packetLength)
				+ "]";
	}
}
