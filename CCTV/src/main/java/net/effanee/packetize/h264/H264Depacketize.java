/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.packetize.h264;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.effanee.packetize.h264.H264Packet.NalPacketType;
import net.effanee.packetize.rtp.RtpPacket;

/**
 * <p>Take the supplied list of H.264 Network Abstraction Layer Units, (NALUs), and
 * reassemble/disassemble the fragments/composite units. In the non-interleaved
 * mode, (packetization mode 1), NAL units are transmitted in NAL unit decoding
 * order. Non-Interleaved mode allows payload types:-</p>
 * <pre>
 * 		1 to 23 - Single NAL unit packet,
 * 		24 - STAP-A,
 * 		28 - FU-A.
 * </pre>
 * <p>This implementation assumes:-</p>
 * <pre>
 * 		the RTP packetization mode used = 1 (Non-Interleaved Mode).
 * 		only NALU types 1, 5, 7, 8 and 28 are received.
 * </pre>
 * <p>Payloads are received in the order:-</p>
 * <pre>
 * 		7, 8, 5, 1, 1, 1...
 * 		7, 8, 5, 1, 1, 1...
 * 		etc...
 * </pre>
 * <p>with payload types 5 and 1 being split over several packets, encapsulated
 * with payload type 28. It is likely that the supplied List{@literal<RtpPacket>} ends
 * part way through a fragmented type 28 payload. Therefore, the returned
 * List{@literal<H264Packet>} does not include any trailing type 5 or 1 payloads that
 * have been fragmented. They are retained and prepended to the next invocation
 * of this instance.</p>
 * 
 * <p>This means, instances of this class are stateful, and should be used to
 * depacketize RTP packets in Decoding Order Number sequence, from the same
 * source. <em>The same instance of this class should be used to decode all
 * packets from the same source.</em></p>
 * 
 * <p>Each list of defragmented packets is prefixed with the last packet type 7,
 * (sequence parameter set), and 8,(picture parameter set) found in the stream.
 * This ensures that if the list of packets is used to start a new file, the
 * first packets encountered in the file describe its contents to the decoder.</p>
 * 
 * <h2>RFC 6184 - RTP Payload Format for H.264 Video</h2>
 *
 * <h3>5.8. Fragmentation Units (FUs)</h3>
 * 
 * <p>Payload type 28 allows fragmenting a NAL unit into several RTP packets.
 * Fragmentation is defined only for a single NAL unit and not for any
 * aggregation packets. A fragment of a NAL unit consists of an integer number
 * of consecutive octets of that NAL unit. Each octet of the NAL unit MUST be
 * part of exactly one fragment of that NAL unit. Fragments of the same NAL unit
 * MUST be sent in consecutive order with ascending RTP sequence numbers (with
 * no other RTP packets within the same RTP packet stream being sent between the
 * first and last fragment). Similarly, a NAL unit MUST be reassembled in RTP
 * sequence number order.</p>
 * 
 * <p>An FU-A consists of a fragmentation unit indicator of one octet, a
 * fragmentation unit header of one octet, and a fragmentation unit payload.</p>
 * 
 * <pre>
 *  0                   1                   2                   3
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * | FU indicator |   FU header   |                                |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-                                |
 * |                                                               |
 * |                            FU payload                         |
 * |                                                               |
 * |                               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                               :...OPTIONAL RTP padding        |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * </pre>
 * 
 * The FU header has the following format:-
 * 
 * <pre>
 * +---------------+
 * |0|1|2|3|4|5|6|7|
 * +-+-+-+-+-+-+-+-+
 * |S|E|R|   Type  |
 * +---------------+
 * </pre>
 * 
 * <p>S: 1 bit. When set to one, the Start bit indicates the start of a fragmented
 * NAL unit. When the following FU payload is not the start of a fragmented NAL
 * unit payload, the Start bit is set to zero.</p>
 * 
 * <p>E: 1 bit When set to one, the End bit indicates the end of a fragmented NAL
 * unit, i.e., the last byte of the payload is also the last byte of the
 * fragmented NAL unit. When the following FU payload is not the last fragment
 * of a fragmented NAL unit, the End bit is set to zero.</p>
 * 
 * <p>R: 1 bit The Reserved bit MUST be equal to 0 and MUST be ignored by the
 * receiver.</p>
 * 
 * <p>Type: 5 bits The NAL unit payload type.</p>
 * 
 * <p>A fragmented NAL unit MUST NOT be transmitted in one FU; that is, the Start
 * bit and End bit MUST NOT both be set to one in the same FU header.</p>
 * 
 * <p>The FU payload consists of fragments of the payload of the fragmented NAL
 * unit so that if the fragmentation unit payloads of consecutive FUs are
 * sequentially concatenated, the payload of the fragmented NAL unit can be
 * reconstructed. The NAL unit type octet of the fragmented NAL unit is not
 * included as such in the fragmentation unit payload, but rather the
 * information of the NAL unit type octet of the fragmented NAL unit is conveyed
 * in the F and NRI fields of the FU indicator octet of the fragmentation unit
 * and in the type field of the FU header. An FU payload MAY have any number of
 * octets and MAY be empty.</p>
 * 
 * <p>If a fragmentation unit is lost, the receiver SHOULD discard all following
 * fragmentation units in transmission order corresponding to the same
 * fragmented NAL unit.</p>
 * 
 * <p>This mode is in use when the value of the OPTIONAL packetization-mode media
 * type parameter is equal to 1. This mode SHOULD be supported. It is primarily
 * intended for low-delay applications. Only single NAL unit packets, STAP-As,
 * and FU-As MAY be used in this mode. The transmission order of NAL units MUST
 * comply with the NAL unit decoding order.</p>
 * 
 * <p>The de-packetization process is implementation dependent. Other schemes may
 * also be used as long as the output for the same input is the same as the
 * process described below. The same output means that the resulting NAL units
 * and their order are identical.</p>
 * 
 * <h3>Non-Interleaved Mode (packetization mode 1)</h3>
 * 
 * <p>The receiver includes a receiver buffer to compensate for transmission delay
 * jitter. The receiver stores incoming packets in reception order into the
 * receiver buffer. Packets are de-packetized in RTP sequence number order. If a
 * de-packetized packet is a single NAL unit packet, the NAL unit contained in
 * the packet is passed directly to the decoder.</p>
 * 
 * <p>For all the FU-A packets containing fragments of a single NAL unit, the
 * de-packetized fragments are concatenated in their sending order to recover
 * the NAL unit, which is then passed to the decoder.</p>
 * 
 * <h3>5.5. Decoding Order Number (DON)</h3>
 * 
 * <p>In the non-interleaved packetization mode, (packetization mode 1),
 * the transmission order of NAL units in single NAL unit packets,
 * STAP-As, and FU-As MUST be the same as their NAL unit decoding order,
 * i.e. (the RTP sequence number order).</p>
 * 
 * <p>The NAL units within an STAP MUST appear in the NAL unit decoding
 * order. Thus, the decoding order is first provided through the
 * implicit order within an STAP and then provided through the RTP
 * sequence number for the order between STAPs, FUs, and single NAL unit
 * packets.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class H264Depacketize {

	// H.264 FU-A packets contain fragments of a single NAL unit. Each single
	// unit begins with a single START packet, followed by zero or more
	// CONTINUATION packets and finishes with one END packet.
	public enum FragmentPosition {

		START,
		CONTINUATION,
		END;
	}
	
	private static final Logger LOGGER = Logger.getLogger(H264Depacketize.class
			.getName());

	// The encapsulated RTP packets.
	private List<RtpPacket> rtpPackets = null;
	
	// This array is stateful, between getDefragmentedPackets() invocations.
	private byte[] fragments = null;
	
	H264Packet h264PacketType7 = null;
	H264Packet h264PacketType8 = null;

	/**
	 * Create a new H.264 depacketization object.
	 */
	public H264Depacketize() {
	}

	/**
	 * Iterate over the RTP packets, reassemble any fragmented packets, and
	 * return a list of reassembled Network Abstraction Layer Unit packets.
	 * The returned packets will have been decoded, so the decoding order number
	 * will have been set to the sequence number of the last packet in the list
	 * of fragments.
	 * 
	 * This method is maintains state between invocations. See class description.
	 * 
	 * @param rtpPackets - a list of RTP packets.
	 * @return a list of defragmented NALU packets.
	 */
	public List<H264Packet> getDefragmentedPackets(List<RtpPacket> rtpPackets){

		this.rtpPackets = rtpPackets;
		return defragment();
	}
	
	/**
	 * Iterate over the RTP packets, reassemble any fragmented packets, and
	 * return a list of reassembled Network Abstraction Layer Unit packets.
	 * 
	 * @return a list of defragmented NALU packets.
	 */
	private List<H264Packet> defragment() {

		H264Packet h264Packet = null;
		List<H264Packet> packets = new ArrayList<>();
		
		for (RtpPacket rtpPacket : rtpPackets) {
			
			h264Packet = rtpPacket.getH264Packet();
			
			// Is a fragmented packet.
			if(h264Packet.getPacketType() == NalPacketType.TYPE_28){
				
				// The first 2 bytes in a fragmentation packet are the header.
				// Hence the start position of 2, in the various array copying
				// methods.
				if (fragments == null){
					// Add first packet to array.
					fragments = addPacket(h264Packet, fragments);
				}
				else{
					// Append to existing array.
					fragments = appendPacket(h264Packet, fragments);
				}
				
				// Process fragmentation unit.
				FragmentPosition fragmentPosition = getFragmentPosition(h264Packet);
				
				if(fragmentPosition == FragmentPosition.END){
					
					// Store the reassembled packet.
					packets.add(createNalUnitPacket(fragments, h264Packet));
					
					// Reset the array.
					fragments = null;
				}
			}
			// Is not a fragmented packet.
			else{
				// The packet can just be added to the list.
				packets.add(rtpPacket.getH264Packet());
				storeParameterSets(rtpPacket.getH264Packet());
			}
		}

		LOGGER.log(Level.FINEST, "Depacketized " + rtpPackets.size()
								+ " RTP packets to " + packets.size()
								+ " H.264 packets.");
		
		prependParameterSets(packets);
		
		return packets;
	}
	
	/**
	 * Prepend the list of packets with the packets that describe the video
	 * parameters.
	 * 
	 * @param packets - the list of packets.
	 */
	private void prependParameterSets(List<H264Packet> packets) {
		
		if(h264PacketType7 != null
		&& h264PacketType8 != null){

			packets.add(0, h264PacketType8);
			packets.add(0, h264PacketType7);
			
//			if(packets.get(0).getPacketType() != NalPacketType.TYPE_7){
//				packets.add(0, h264PacketType7);
//			}
//			if(packets.get(1).getPacketType() != NalPacketType.TYPE_8){
//				packets.add(1, h264PacketType8);
//			}
		}
	}
	
	/**
	 * Store H.264 packet types 7, (sequence parameter set), and 8,(picture
	 * parameter set).
	 * 
	 * @param h264Packet - the H.264 packet to assess.
	 */
	@SuppressWarnings("incomplete-switch")
	private void storeParameterSets(H264Packet h264Packet) {
		
		switch (h264Packet.getPacketType()) {
			case TYPE_7:
				h264PacketType7 = h264Packet;
				break;
			case TYPE_8:
				h264PacketType8 = h264Packet;
				break;
		}
	}
	
	/**
	 * Create a new Network Abstraction Layer Unit, (NALU), from the payload in
	 * the supplied fragments. Prepended with a new NAL unit header created
	 * from the supplied H.264 NALU.
	 * 
	 * @param fragments - the NAL unit payload.
	 * @param h264Packet - the NAL unit packet from which a header will be
	 * created.
	 * @return a new NAL unit packet.
	 */
	private H264Packet createNalUnitPacket(byte[] fragments, H264Packet h264Packet) {

		// Create a new header byte, append the packet bytes, and create a new
		// H264Packet with a Decoding Order Number equal to the last packet in
		// fragmented sequence.
		
		byte[] combined = new byte[fragments.length + 1];
		combined[0] = createNalUnitHeader(h264Packet);
		System.arraycopy(fragments, 0, combined, 1, fragments.length);
		
		return new H264Packet(combined, h264Packet.getDecodingOrderNumber());
	}

	/**
	 * Create a header for the defragmented packet.
	 * 
	 * @param h264Packet
	 * @return a new header based on information from the first and second bytes
	 * of the supplied packet.
	 */
	private byte createNalUnitHeader(H264Packet h264Packet) {

		// Get first 3 bits from the NAL Unit header. 0xE0 = 11100000
		byte nalHeader = (byte) (h264Packet.getPacket()[1] & 0xE0);
		
		// Get last 5 bits from the NAL Unit fragmentation header. 0xF1 = 00011111
		byte nalFragmentationHeader = (byte) (h264Packet.getPacket()[1] & 0x1F); 
		
		// Combine and return.
		return (byte) (nalHeader | nalFragmentationHeader);
	}

	/**
	 * Add the payload of the supplied packet to the beginning of the fragments
	 * array. 
	 * 
	 * @param h264Packet - the packet containing the payload.
	 * @param fragments - the fragments array.
	 * @return the array containing the payload.
	 */
	private byte[] addPacket(H264Packet h264Packet, byte[] fragments) {
		
		// The first 2 bytes in a fragmentation packet are the header.
		// Hence the start position of 2, in the various array copying
		// methods.
		
		return Arrays.copyOfRange(h264Packet.getPacket(), 2, h264Packet.getPacketLength());
	}

	/**
	 * Concatenate the payload of the supplied packet to the accumulated
	 * fragments. 
	 * 
	 * @param h264Packet - the packet containing the payload.
	 * @param fragments - the accumulated fragments.
	 * @return the concatenated array.
	 */
	private byte[] appendPacket(H264Packet h264Packet, byte[] fragments) {
		
		// The first 2 bytes in a fragmentation packet are the header.
		// Hence the start position of 2, in the various array copying
		// methods.
		
		byte[] combined = new byte[fragments.length + h264Packet.getPacketLength() - 2];
		System.arraycopy(fragments, 0, combined, 0, fragments.length);
		System.arraycopy(h264Packet.getPacket(), 2,
				combined, fragments.length, h264Packet.getPacketLength() - 2);
		
		return combined;
	}
	
	/**
	 * Get the fragmentation position - either START, CONTINUATION or END.
	 * 
	 * @param h264Packet - the packet to examine.
	 * @return the fragmentation position.
	 */
	private FragmentPosition getFragmentPosition(H264Packet h264Packet) {
		
		// Get bit 0, fragment start indicator.
		byte fragmentStart = getBits(h264Packet.getPacket()[1], 0, 0);
		// Get bit 1, fragment end indicator.
		byte fragmentEnd = getBits(h264Packet.getPacket()[1], 1, 1);
		
		if(fragmentStart == 1){
			// Fragment start.
			return FragmentPosition.START;
			
		}
		else if(fragmentEnd == 1){
			// Fragment end.
			return FragmentPosition.END;
		}
		else{
			// Assume fragment continuation.
			return FragmentPosition.CONTINUATION;
		}
	}
	
	/**
	 * Extract a contiguous range of bits from a byte, and return the result as
	 * a byte where the requested bits are right aligned in the new byte.
	 *   
	 * @param b - the byte containing the bits.
	 * @param startBit - the start position of the first contiguous bit, the left
	 * most bit is numbered 0, positions are inclusive.
	 * @param endBit - the start position of the last contiguous bit, the right
	 * most bit is numbered 7, positions are inclusive.
	 * @return the bits, right aligned in the new byte.
	 */
	protected byte getBits(byte b, int startBit, int endBit){
		
		int bits = 0;
		int mask = getBitMask((endBit - startBit) + 1);
		
		// Right align the bits we want.
		bits = (byte)(b >>> (7 - endBit));
		// Remove ones to the left of the bits we want.
		bits = (bits & mask);
		
		return (byte)bits;
	}
	
	/**
	 * Create a bit mask consisting of right aligned ones. This mask is intended
	 * to be used with 8 bit bytes, so the valid parameter values are 1 - 8. 
	 * 
	 * @param noOfBits - a value in the range 1 to 8.
	 * @return a bit mask with the requested number of contiguous ones in the
	 * right most positions.
	 */
	protected int getBitMask(int noOfBits){
		
		int mask = 0;
		for(int i = 0; i < noOfBits; i++){
			mask = (int) (mask + Math.pow(2, i));
		}
		
		return mask;
	}
}
