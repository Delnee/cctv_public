/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.packetize.h264;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>Write the supplied NAL Units to a file.</p>
 * 
 * <h2>ITU-T H.264</h2>
 * 
 * <h3>Annex B Byte stream format</h3>
 * 
 * <p>The byte stream format consists of a sequence of byte stream NAL unit syntax
 * structures. Each byte stream NAL unit syntax structure contains one start
 * code prefix followed by one NAL Unit.</p>
 * 
 * <p>A start code prefix can be either of the byte sequences:-</p>
 * <code>
 * 		0x000001 or 0x00000001
 * </code>
 * <p>This class uses the four byte variant when writing the packet payload to
 * file. Only sections beginning with a Sequence Parameter Set are written.
 * This ensures that every new file starts with a Sequence Parameter Set that
 * describes the content. This relies on the packet sequence being ordered as
 * follows:-</p>
 * <pre>
 *                                          Type
 *  1 * Sequence parameter set              7
 *  1 * Picture parameter set               8
 *  1 * Coded slice of an IDR picture       5
 *  M * Coded slice of a non-IDR picture    1
 * </pre>
 * <p>This is achieved by removing the last occurrence of a Sequence Parameter Set
 * packet - and all following packets - from the supplied list of H.264 packets
 * before writing to file. The held back packets are prepended to the next write
 * operation.</p>
 * 
 * <p>A H.264 Raw Byte Sequence Payload may contain the byte sequences:-</p>
 * <code>
 * 0x000000, 0x000001, 0x000002, 0x000003
 * </code>
 * <p>If the Raw Byte Sequence Payload is to be formatted as a NAL Unit, these
 * sequences must be escaped as below:-</p>
 * <code>
 * 0x00000300, 0x00000301, 0x00000302, 0x00000303
 * </code>
 * <p><em>Note:- The target camera, (HikVision DS-2CD2032-I), appears to have already escaped
 * these byte sequences.</em></p>
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class H264FileWriter{

	private static final Logger LOGGER = Logger.getLogger(H264FileWriter.class.getName());
	
	// Initial ByteBuffer size.
	private static final int BUFFER_SIZE = 1000000;
	
	// NAL Unit prefix.
	private static final byte[] NALU_PREFIX = {0x0, 0x0, 0x0, 0x1};
	
	// Raw Byte Sequence Payload escape bytes.
	private static final byte[] ESCAPED_BYTES_PREFIX = {0x0, 0x0};
	private static final byte ESCAPE_BYTE = 0x3;
	private static final byte ESCAPED_BYTE_0 = 0x0;
	private static final byte ESCAPED_BYTE_1 = 0x1;
	private static final byte ESCAPED_BYTE_2 = 0x2;
	private static final byte ESCAPED_BYTE_3 = 0x3;
	
	// Current packets are written to the file.
	private List<H264Packet> currentH264Packets = new ArrayList<>(0);
	
	// Previous packets are held back to be prepended to the next file.
	private List<H264Packet> pendingH264Packets = new ArrayList<>(0);
	
	// The file.
	private FileChannel fileChannel;
	
	/**
	 * Create an H.264 file writer.
	 */
	public H264FileWriter(){
	}

//	/**
//	 * Split the list of packets, so that the last "Sequence Parameter Set/
//	 * Picture Parameter Set" combination, (NALUs 7 and 8), is at the beginning
//	 * of the next write operation. This should ensure that a new file will
//	 * always begin with a set of useful parameters.
//	 */
//	private void split(){
//
//		int lastOccurrence = getLastSps();
//		
//		if (lastOccurrence != -1){
//			
//			// Get a temporary list of the next list of pending packets.
//			List<H264Packet> trailingH264Packets = new ArrayList<>(
//					currentH264Packets.subList(lastOccurrence, currentH264Packets.size()));
//			
//			// Remove the next list of pending packets from the end of the list
//			// of packets that are about to be written.
//			currentH264Packets.removeAll(
//					currentH264Packets.subList(lastOccurrence, currentH264Packets.size()));
//			
//			// Combine the previous and current packets.
//			pendingH264Packets.addAll(currentH264Packets);
//			currentH264Packets = pendingH264Packets;
//			
//			// Store the new pending packets.
//			pendingH264Packets = trailingH264Packets;
//		}
//		else{
//			
//			// Write everything.
//			
//			// Combine the previous and current packets.
//			pendingH264Packets.addAll(currentH264Packets);
//			currentH264Packets = pendingH264Packets;
//			
//			// Empty the pending packets.
//			pendingH264Packets.clear();
//		}
//	}
	
//	/**
//	 * Iterate from the end of the NALU packet list, to get the last occurrence
//	 * of NALU type 7 - Sequence Parameter Set.
//	 * 
//	 * @return the last occurrence, or -1 if there is no occurrence.
//	 */
//	private int getLastSps(){
//		
//		H264Packet h264Packet = null;
//		ListIterator<H264Packet> li = currentH264Packets.listIterator(currentH264Packets.size());
//		int lastOccurrence = currentH264Packets.size();
//		
//		while(li.hasPrevious()) {
//			h264Packet = li.previous();
//			lastOccurrence--;
//			if(h264Packet.getPacketType() == NalPacketType.TYPE_7){
//				break;
//			}
//		}
//		
//		return lastOccurrence == currentH264Packets.size() ? -1 : lastOccurrence;
//	}
	
	/**
	 * A H.264 Raw Byte Sequence Payload may not contain the byte sequences:-
	 * 
	 * 		0x000000, 0x000001, 0x000002, 0x000003
	 * 
	 * They must be escaped as:-
	 * 
	 * 		0x00000300, 0x00000301, 0x00000302, 0x00000303
	 * 
	 * This method logs any escaped byte sequences found in the supplied byte
	 * array.
	 * 
	 * @param packetBytes the byte array to examine.
	 */
	@SuppressWarnings("unused")
	private void escapeBytes(byte[] packetBytes){
		
		if(packetBytes.length > 3){
			
			for (int i = 3; i < packetBytes.length; i++){
				
				if(ESCAPED_BYTES_PREFIX[0] == packetBytes[i - 3]
				&& ESCAPED_BYTES_PREFIX[1] == packetBytes[i - 2]
				&& ESCAPE_BYTE == packetBytes[i - 1]){
					
					switch (packetBytes[i - 0]) {
					
					case ESCAPED_BYTE_0:
						LOGGER.log(Level.FINEST, "Found an escaped byte 0.");
						break;
					case ESCAPED_BYTE_1:
						LOGGER.log(Level.FINEST, "Found an escaped byte 1.");
						break;
					case ESCAPED_BYTE_2:
						LOGGER.log(Level.FINEST, "Found an escaped byte 2.");
						break;
					case ESCAPED_BYTE_3:
						LOGGER.log(Level.FINEST, "Found an escaped byte 3.");
						break;
					}
				}
			}
		}
	}
	
	/**
	 * Write the supplied H.264 packets to the supplied file. Packets are split,
	 * into those that will be written now, and those that will be written on
	 * the next invocation of this method. 
	 * 
	 * @param h264Packets - the list of H.264 packets to write.
	 * @param path - the file to write to.
	 * @throws IOException 
	 */
	public void writeFile(List<H264Packet> h264Packets, Path path) throws IOException {

		this.currentH264Packets = h264Packets;
		//split();
		
		// Open the file.
		openWriter(path);
		
		// Write the current packets.
		write();
		
		// Close the file.
		closeWriter();
	}

	/**
	 * Open a non-blocking, write/append, file.
	 * 
	 * @throws IOException 
	 */
	private void openWriter(Path path) throws IOException{

		try {
			
			if(path.toFile().exists()){
				// Append to an existing file.
				fileChannel = FileChannel.open(path,
						EnumSet.of(StandardOpenOption.WRITE,
						StandardOpenOption.APPEND));
				LOGGER.log(Level.FINEST, "Opened the existing File Channel "
											+ path.getFileName()
											+ " for appending.");				
			}
			else{
				// Create a new file.
				fileChannel = FileChannel.open(path,
						EnumSet.of(StandardOpenOption.CREATE,
						StandardOpenOption.WRITE));
				LOGGER.log(Level.FINEST, "Opened the new File Channel "
											+ path.getFileName());
			}
		}
		catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Could not open the file "
										+ path.getFileName()
										+ " for the NAL unit bytes.", e);
			throw e;
		}
	}

	/**
	 * Write the H.264 packets, preceded by a delimiter.
	 *  
	 * @throws IOException 
	 */
	private void write() throws IOException{

		ByteBuffer byteBuffer = ByteBuffer.allocate(BUFFER_SIZE);
		H264Packet h264Packet = null;
		
		ListIterator<H264Packet> li = currentH264Packets.listIterator();
		while(li.hasNext()) {
			
			// Get the packet bytes.
			h264Packet = li.next();
			
			// Check ByteBuffer is large enough.
			if(byteBuffer.capacity() < h264Packet.getPacket().length
										+ NALU_PREFIX.length){
				expandBuffer(byteBuffer, h264Packet.getPacket().length);
			}
			
			//escapeBytes(h264Packet.getPacket());
			
			// Populate the buffer with the contents.
			byteBuffer.clear();
			byteBuffer.put(NALU_PREFIX);
			byteBuffer.put(h264Packet.getPacket());
			
			try {
				// Write the buffer bytes.
				byteBuffer.flip();
				fileChannel.write(byteBuffer);
			}
			catch (IOException e) {
				LOGGER.log(Level.SEVERE, "Could not write the file for the H264 NAL Unit bytes.", e);
				throw e;
			}
		}		
	}

	/**
	 * Close the file.
	 */
	private void closeWriter(){
		
		try {
			fileChannel.force(false);
			fileChannel.close();
			LOGGER.log(Level.FINEST, "Closed the File Channel.");
		}
		catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Could not close the temporary file for the H264 NAL Unit bytes.", e);
		}
	}
	
	/**
	 * Expand the ByteBuffer.
	 * 
	 * @param buffer
	 * @param capacity
	 * @return
	 */
	private ByteBuffer expandBuffer(ByteBuffer buffer, int capacity){
		
		// TODO This needs testing!
		
		buffer.limit(buffer.position());
		buffer.rewind();
		
		ByteBuffer expandedBuffer = ByteBuffer.allocate(capacity);
		expandedBuffer.put(buffer);
		
		return expandedBuffer;
	}
	
	@Override
	public String toString() {
		return "H264FileWriter ["
				+ ("pendingH264Packets=" + pendingH264Packets.size() + ",")
				+ ("currentH264Packets=" + currentH264Packets.size())
				+ "]";
	}	
}
