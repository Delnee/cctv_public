/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.upnp.discovery;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.InterfaceAddress;
import java.net.MalformedURLException;
import java.net.MulticastSocket;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.StandardProtocolFamily;
import java.net.StandardSocketOptions;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.DatagramChannel;
import java.nio.channels.MembershipKey;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.Charsets;

/**
 * <p>Search for UPnP devices on the local network. IPv4 only.</p>
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2015</p>
 */
public class Search {

	private static final Logger LOGGER = Logger.getLogger(Search.class.getName());	
	
	// Search request properties for UPnP root devices.
	
	// Reserved multicast address and port.
	private static final String UPNP_GROUP_ADDRESS = "239.255.255.250";
	private static final int UPNP_GROUP_PORT = 1900;
	// Local port to bind to for sending/receiving datagrams.
	private static final int LOCAL_PORT = 1901;
	// Maximum UPnP advertisement response wait time value between 1 and 5.
	private static final int RESPONSE_WAIT_TIME = 5;
	// Search query.
	// private static final String SERVICE_TYPE_CONTENT_DIR = "urn:schemas-upnp-org:service:ContentDirectory:1";
	private static final String SERVICE_TYPE_ALL = "upnp:rootdevice";
	private static final String QUERY = "M-SEARCH * HTTP/1.1\r\n"
										+ "HOST: " + UPNP_GROUP_ADDRESS + ":" + UPNP_GROUP_PORT + "\r\n"
										+ "MAN: \"ssdp:discover\"\r\n"
										+ "MX: " + RESPONSE_WAIT_TIME + "\r\n"
										+ "ST: " + SERVICE_TYPE_ALL + "\r\n"
										+ "\r\n";
	
	private static final Pattern LOCATION_PATTERN = Pattern.compile("(?iu)^Location:(.+?)$", Pattern.MULTILINE);
	// XML element names are case sensitive.
	private static final Pattern FRIENDLY_ELEMENT_PATTERN = Pattern.compile("(?iu)<friendlyName>(.+?)</friendlyName>");
	
	/**
	 * Self execute this class.
	 * 
	 * @param args - nothing.
	 */
	public static void main(String[] args){
		
		Map<String, String>friendlyNames = new Search().getUpnpServers();
		
		for(Map.Entry<String, String> keyValue: friendlyNames.entrySet()){
			System.out.println(keyValue.getKey() + " - " + keyValue.getValue());
		}
	}
	
	/**
	 * Get the friendly names of site local UPnP devices, and their associated
	 * IP address.
	 * 
	 * @return a map of device hosts and friendly names.
	 */
	public Map<String, String> getUpnpServers(){
		
		Map<String, String> servers = new HashMap<>();
				
		InetAddress interfaceAddr = getSiteLocalIPv4Address();
		
		if(interfaceAddr == null){
			LOGGER.log(Level.WARNING, "Could not get an IP NIC interface address for this device.");
			return servers;
		}

		try {
			sendQuery(interfaceAddr);
			LOGGER.log(Level.INFO, "Sent UPnP search query.");
		}
		catch (IOException e) {
			LOGGER.log(Level.WARNING, "Querying the local network for UPnP devices caused an exception.", e);
			return servers;
		}
		
		servers = getUpnpServers(interfaceAddr);
		LOGGER.log(Level.INFO, "Retrieved " + servers.size() + " search responses.");
		
		return servers;
	}	
	
	/**
	 * Send a multicast UPnP search query.
	 * 
	 * @param interfaceAddr the IP address of the interface to send from.
	 * @throws IOException
	 */
	private void sendQuery(InetAddress interfaceAddr) throws IOException{
		
		//Get the UPnP group address/port.
		InetAddress groupAddr = InetAddress.getByName(UPNP_GROUP_ADDRESS);
		InetSocketAddress socketAddr = new InetSocketAddress(groupAddr, UPNP_GROUP_PORT);
		
		//Create a multicast socket.
		MulticastSocket socket = new MulticastSocket(null);
	    socket.bind(new InetSocketAddress(interfaceAddr, LOCAL_PORT));
	    socket.setTimeToLive(1);
	    
	    //Get the query and send it.
	    byte[] httpQuery = QUERY.getBytes();
	    socket.send(new DatagramPacket(httpQuery, httpQuery.length, socketAddr));
	    
	    //Close socket.
	    socket.disconnect();
		socket.close();	
	}
	
	/**
	 * Get a map of UPnP friendly names and associated IP addresses from the
	 * local network. 
	 * 
	 * @param interfaceAddr the NIC address to use for the query/response. 
	 * @return a map of device hosts and friendly names.
	 */
	private Map<String, String> getUpnpServers(InetAddress interfaceAddr){

		ReceiveResponse receiver = new ReceiveResponse(interfaceAddr);
		
		//Look into using AsynchronousChannels instead of a separate thread?
		try {
	        Thread thread = new Thread(receiver);
	        //thread.setDaemon(true);
	        thread.start();
	        //Pause, for duration of the response wait time, before continuing.
			Thread.sleep(RESPONSE_WAIT_TIME * 1000);
		}
		catch (InterruptedException e) {
			//Don't care...
			LOGGER.log(Level.WARNING, "Waiting for the UPnP responses was prematurely interrupted.", e);
		}
		finally{
			receiver.stop();	
		}

		//Get the HTTP search responses.
		List<String> responses = receiver.getResponses();
		
		// Parse the responses.
		List<URL> urls = getLocations(responses);
		
		// Get the device descriptions from the default URLs.
		Map<URL, String> deviceDescriptions = getDeviceDescriptions(urls);

		// Get the device friendly names from the device description responses.
		Map<String, String>friendlyNames = getFriendlyNames(deviceDescriptions);
		
		return friendlyNames;
	}

	/**
	 * Parse the device descriptions and extract the devices friendly name.
	 * 
	 * @param deviceDescriptions -  the device descriptions to parse.
	 * @return a map of device hosts and friendly names.
	 */
	private Map<String, String> getFriendlyNames(Map<URL, String> deviceDescriptions){
	
		Map<String, String>friendlyNames = new HashMap<>();
		
		for(Map.Entry<URL, String> keyValue: deviceDescriptions.entrySet()){
			
			Matcher m = FRIENDLY_ELEMENT_PATTERN.matcher(keyValue.getValue());
			if(m.find()){
				
				friendlyNames.put(keyValue.getKey().getHost(), m.group(1).trim());
			}
		}
		
		return friendlyNames;
	}
	
	/**
	 * Query the URLs to get the device description HTML page.
	 * 
	 * @param urls - the URLs to query.
	 * @return a map of URLs and their associated HTML device descriptions.
	 */
	private Map<URL, String> getDeviceDescriptions(List<URL> urls){
		
		Map<URL, String> deviceDescriptions = new HashMap<>();
		
		String htmlLine = null;
		for(URL url: urls){
		
			StringBuilder content = new StringBuilder();
			
			// Open URL.
			try (BufferedReader reader = new BufferedReader(
					new InputStreamReader(
					url.openConnection().getInputStream(), Charsets.UTF_8))) {
				
				// Read and accumulate HTML lines.
				while((htmlLine = reader.readLine()) != null){
					content.append(htmlLine);
				}
				
				// Save.
				deviceDescriptions.put(url, content.toString());
			}
			catch (IOException e) {
				LOGGER.log(Level.WARNING, "Could not retrieve the UPnP device description.", e);
			}
		}
		
		return deviceDescriptions;
	}
	
	/**
	 * Extract the URL of the device description from the responses.
	 * 
	 * @param responses - the responses to examine.
	 * @return a list of device description URLs.
	 */
	private List<URL> getLocations(List<String> responses){
		
		List<URL> urls = new ArrayList<>();
		URL url = null;
		
		for(String response: responses){
			
			Matcher m = LOCATION_PATTERN.matcher(response);
			if(m.find()){
				
				try {
					url = new URL(m.group(1).trim());
					urls.add(url);
				}
				catch (MalformedURLException e) {
					LOGGER.log(Level.INFO, "Could not convert the UPnP search result"
											+ m.group(1).trim()
											+ " to a URL.", e.getMessage());
				}
			}
		}
		return urls;
	}
	
	/**
	 * Get a site local IPv4 address for the local NIC.
	 * 
	 * Should be in the range:-
	 * <ul>
	 * <li>10.0.0.0 - 10.255.255.255</li>
	 * <li>172.16.0.0 - 172.31.255.255</li>
	 * <li>192.168.0.0 - 192.168.255.255</li>
	 * </ul>
	 * 
	 * @return the IPv4 address, or null if none can be found.
	 */
	InetAddress getSiteLocalIPv4Address(){
		
		List<InetAddress> addresses = getInterfaceAddresses();
		InetAddress ip4Address = null;
		
		for (InetAddress address: addresses){
			
			if (address instanceof Inet4Address){
				LOGGER.log(Level.FINE, "Obtained the following site local IPv4 NIC address: " + address.getHostAddress());
				ip4Address = address;
				break;
			}
		}
		
		return ip4Address;
	}
	
	/**
	 * Get the IP interface addresses for all NICs. Where the interface:-
	 * 
	 * <ul>
	 * <li>is not the loopback interface</li>
	 * <li>is active.</li>
	 * </ul>
	 * 
	 * @return a list of <code>InetAddress</code> addresses.
	 */
	List<InetAddress> getInterfaceAddresses(){

		List<InetAddress> addresses = new ArrayList<>();
		try {
			Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
			while (interfaces.hasMoreElements()) {
				
				NetworkInterface networkInterface = interfaces.nextElement();
				if (!networkInterface.isLoopback() && networkInterface.isUp()) {
					
					for (InterfaceAddress address: networkInterface.getInterfaceAddresses()) {
						addresses.add(address.getAddress());
						LOGGER.log(Level.FINE, "Obtained the following local NIC address: " + address.getAddress().getHostAddress());
					}
				}
			}
		}
		catch (SocketException e) {
			LOGGER.log(Level.SEVERE, "An IP network interface address could not be obtained for this device.", e);
		}
		return addresses;
	}
	
	/**
	 * Processes the receipt of search responses. An instance of this class
	 * should be passed to a thread, and the <code>stop</code> method invoked
	 * to halt the process when required.
	 * 
	 * The responses can be retrieved at any time from the <code>getReponses
	 * </code> method
	 * 
	 * @author fjh
	 */
	private final class ReceiveResponse implements Runnable{
		
		//The thread running the instance of this class.
		private Thread thread;
		
		private final InetAddress interfaceAddr;
		private DatagramChannel channel;
		
		//The UPnP search responses.
		private final List<String> responses = new CopyOnWriteArrayList<String>();
		
		public ReceiveResponse(InetAddress interfaceAddr) {
			this.interfaceAddr = interfaceAddr;
		}
		
		/**
		 * Read responses to a UPnP search request. Will run until the <code>
		 * stop</code> method is invoked.
		 */
		@Override
		public void run() {
			
			//At this point we can get a reference to the executing thread,
			//which can be used later to stop the thread.
			this.setThread(Thread.currentThread());
			
			//Wait until stop is called.
			MembershipKey key = openSocket();
			if(key == null){
				LOGGER.log(Level.WARNING, "Exiting UPnP search thread.");
				return;
			}
			waitOnData(key);
		}
		 
		/**
		 * Open a multicast datagram channel socket, and join the group
		 * listening for UPnP search responses.
		 * 
		 * @return the channel key, needed to later close membership of the
		 * channel group.
		 */
		private MembershipKey openSocket(){
			
			MembershipKey key = null;
			try {
				NetworkInterface interfaceName = NetworkInterface.getByInetAddress(interfaceAddr);
				channel = DatagramChannel.open(StandardProtocolFamily.INET)
						.setOption(StandardSocketOptions.SO_REUSEADDR , true)
						.setOption(StandardSocketOptions.IP_MULTICAST_TTL, 1)
						.bind(new InetSocketAddress(interfaceAddr, LOCAL_PORT));
						
				key = channel.join(InetAddress.getByName(UPNP_GROUP_ADDRESS), interfaceName);				
			}
			catch (IOException e) {
				LOGGER.log(Level.SEVERE, "Could not get open a DatagramChannel to listen for UPnP search responses.", e);
			}
			return key;
		}
		
		/**
		 * Leaves the channel group and closes the channel socket.
		 * 
		 * @param key the key needed to close the channel group.
		 */
		private void closeSocket(MembershipKey key){
			
			try {
				key.drop();
				channel.disconnect();
				channel.close();
			}
			catch (IOException e) {
				LOGGER.log(Level.WARNING, "Could not close the DatagramChannel performing UPnP searches.", e);
			}
		}		
		
		/**
		 * Receive responses from UPnP devices. Blocks on receive. Adds
		 * responses to the <code>List<String> responses</code> instance
		 * property.
		 * 
		 * @param key the channel key, used to leave the channel group.
		 */
		private void waitOnData(MembershipKey key){
	
			ByteBuffer data = ByteBuffer.allocate(2000);
			//UPnP uses UTF-8 as its character encoding for message serialisation. 
			Charset charset = Charset.forName("UTF-8");
			CharsetDecoder decoder = charset.newDecoder();
			String message = null;
			while(!Thread.currentThread().isInterrupted()){
                
                try {
					channel.receive(data);
					data.flip();
                    
                    //Process the message.
                    message = decoder.decode(data).toString();
                    responses.add(message);
                    
                    data.clear();
                }
                catch (ClosedByInterruptException e){
                	if(Thread.currentThread().isInterrupted()){
        				//Assume the exception is caused by the stop method being
        				//invoked and continue.
                		closeSocket(key);
                	}
                	else{
                		LOGGER.log(Level.SEVERE, "The thread reading UPnP query results was unexpectedly interrupted.", e);
                	}
                }
                catch (IOException e) {
                	LOGGER.log(Level.SEVERE, "The thread reading UPnP query results experienced an unexpected IO exception.", e);
				}
			}
		}

		/**
		 * Returns a copy of the responses received since this runnable was
		 * started.
		 * 
		 * @return
		 */
		List<String> getResponses(){
			
			List<String> copy = new ArrayList<String>(responses);
			return copy;
		}
		
		private Thread getThread() {
			return thread;
		}

		private void setThread(Thread thread) {
			this.thread = thread;
		} 
		 
		/**
		 * Stop <code>ReceiveResponse</code> instance running.
		 */
		void stop(){
			getThread().interrupt();
		}
	 }//Class ReceiveResponse end.
}
