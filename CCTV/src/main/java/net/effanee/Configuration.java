/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileSystems;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>Configuration information. The properties file should be located
 * immediately below the cctv.jar directory, on the path:-</p>
 * <pre>
 * 	./config/cctv.properties
 * </pre>
 * 
 * <p>If the properties file location is not set, the default is used:-</p>
 * <pre>
 * {@literal
 * 	<start directory>/config/cctv.properties
 * }
 * </pre>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class Configuration {

	private static final Logger LOGGER = Logger.getLogger(Configuration.class.getName());
	
	private static final String USERNAME_PASSWORD_SEPARATOR = ":";
	
	private static String PROPERTIES_FILE = "config/cctv.properties";
	private static final String KEY_FRIENDLY_NAME = "rtsp.client.stream1.friendly.name";
	private static final String KEY_URL_WITH_CREDENTIALS = "rtsp.client.stream1.urlWithCredentials";
	private static final String KEY_RTP_CLIENT_PORT = "rtp.client.stream1.port";
	private static final String KEY_RTCP_CLIENT_PORT = "rtcp.client.stream1.port";
	private static final String KEY_RTP_CLIENT_OUTPUT_DIRECTORY = "rtp.client.stream1.output.directory";
	private static final String KEY_RTP_CLIENT_PACKETS_PER_SECOND = "rtp.client.stream1.packetsPerSecond";
	private static final String KEY_RTP_CLIENT_LOG_PACKETS_PER_SECOND = "rtp.client.stream1.logPacketsPerSecond";
	private static final String KEY_RTP_CLIENT_PAYLOAD_SIZE = "rtp.client.stream1.payloadSize";
	private static final String KEY_H264_MAXIMUM_DISC_SPACE = "h264.maximumDiscSpace";
	
	private Configuration () {
		// Should not be instantiated.
	}

	/**
	 * Set the properties file location.
	 * 
	 * @param fileLocation - the location of the configuration file.
	 * @return true if the properties file exists, else false.
	 */
	public static boolean setFileLocation(String fileLocation){
		
		if (fileLocation != null) {
			File file = new File(fileLocation);
			
			if (file.exists()) {
				PROPERTIES_FILE = fileLocation;
				return true;				
			}
		}
		return false;
	}	
	
	/**
	 * Load the properties file.
	 * 
	 * @return the loaded properties file, or an empty properties list, if the
	 * file could not be loaded.
	 */
	private static Properties getProperties(){
		
		Properties props = new Properties();
		
		try (FileInputStream in = new FileInputStream(new File(PROPERTIES_FILE))) {
			props.load(in);
		}
		catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Could not load the cctv.properties file.", e);
		}	

		return props;
	}

	/**Get the RTP client packets per second setting from the
	 * <code>cctv.properties</code> file.
	 * 
	 * @return the packets per second, or -1 if it is not set.
	 */
	public static int getRtpClientPacketsPerSecond(){
		
		Properties props = getProperties();
		String packetsPerSecondString = props.getProperty(KEY_RTP_CLIENT_PACKETS_PER_SECOND);
		
		int packetsPerSecond = -1;
		try {
			packetsPerSecond = Integer.parseInt(packetsPerSecondString);
		}
		catch (Exception e) {
			// No action.
		}
		return packetsPerSecond;
	}
	
	/**Get whether the RTP client packets per second should be logged setting
	 * from the <code>cctv.properties</code> file.
	 * 
	 * @return should the packets per second be logged, or false if it is not set.
	 */
	public static boolean getLogPacketsPerSecond(){
		
		Properties props = getProperties();
		String logPacketsPerSecondString = props.getProperty(KEY_RTP_CLIENT_LOG_PACKETS_PER_SECOND);
		
		boolean logPacketsPerSecond = false;
		try {
			logPacketsPerSecond = Boolean.parseBoolean(logPacketsPerSecondString);
		}
		catch (Exception e) {
			// No action.
		}
		return logPacketsPerSecond;
	}
	
	/**Get the RTP client payload size setting from the
	 * <code>cctv.properties</code> file.
	 * 
	 * @return the payload size, or -1 if it is not set.
	 */
	public static int getRtpClientPayloadSize(){
		
		Properties props = getProperties();
		String payloadSizeString = props.getProperty(KEY_RTP_CLIENT_PAYLOAD_SIZE);
		
		int payloadSize = -1;
		try {
			payloadSize = Integer.parseInt(payloadSizeString);
		}
		catch (Exception e) {
			// No action.
		}
		return payloadSize;
	}	

	/**Get the maximum disc space that files may consume from the
	 * <code>cctv.properties</code> file.
	 * 
	 * @return the maximum disc space, or -1 if it is not set.
	 */
	public static long getMaximumDiscSpace(){
		
		Properties props = getProperties();
		String maximumDiscSpaceString = props.getProperty(KEY_H264_MAXIMUM_DISC_SPACE);
		
		long maximumDiscSpace = -1l;
		try {
			maximumDiscSpace = Long.parseLong(maximumDiscSpaceString);
		}
		catch (Exception e) {
			// No action.
		}
		return maximumDiscSpace;
	}

	/**Get the RTP client port address setting from the <code>cctv.properties</code>
	 * file.
	 * 
	 * @return the port number, or -1 if it is not set.
	 */
	public static int getRtpClientPort(){
		
		Properties props = getProperties();
		String portString = props.getProperty(KEY_RTP_CLIENT_PORT);
		
		int port = -1;
		try {
			port = Integer.parseInt(portString);
		}
		catch (Exception e) {
			// No action.
		}
		return port;
	}

	/**Get the RTCP client port address setting from the <code>cctv.properties</code>
	 * file.
	 * 
	 * @return the port number, or -1 if it is not set.
	 */
	public static int getRtcpClientPort(){
		
		Properties props = getProperties();
		String portString = props.getProperty(KEY_RTCP_CLIENT_PORT);
		
		int port = -1;
		try {
			port = Integer.parseInt(portString);
		}
		catch (Exception e) {
			// No action.
		}
		return port;
	}

	/**Get the CCTV friendly name setting from the <code>cctv.properties</code>
	 * file.
	 * 
	 * @return the friendly name, or null if it is not set.
	 */
	public static String getFriendlyName(){
		
		Properties props = getProperties();
		return props.getProperty(KEY_FRIENDLY_NAME);
	}
	
	/**Get the URI with credentials.
	 * 
	 * @return the URI or null if the URI cannot be created.
	 */
	public static URI getUrlWithCredentials(){
		
		Properties props = getProperties();
		String uriString = props.getProperty(KEY_URL_WITH_CREDENTIALS);
		
		URI uri = null;
		try {
			uri = new URI(uriString);
		}
		catch (URISyntaxException e) {
			LOGGER.log(Level.SEVERE, "Could not create a valid URL from the property " + KEY_URL_WITH_CREDENTIALS + ".", e);
			return null;
		}
		
		return uri;
	}

	/**Get the output directory.
	 * 
	 * @return the directory or null if the directory cannot be located.
	 */
	public static Path getOutputDirectory(){
		
		Properties props = getProperties();
		String pathString = props.getProperty(KEY_RTP_CLIENT_OUTPUT_DIRECTORY);
		
		Path path = null;
		try {
			path = FileSystems.getDefault().getPath(pathString);
		}
		catch (InvalidPathException e) {
			LOGGER.log(Level.SEVERE, "Could not find a valid output directory from the property " + KEY_RTP_CLIENT_OUTPUT_DIRECTORY + ".", e);
			return null;
		}
		
		return path;
	}
	
	/**Get the CCTV user name setting from the <code>cctv.properties</code>
	 * file.
	 * 
	 * @return the user name, or null if it is not set.
	 */
	public static String getUserName(){
		
		return getUserNamePassword()[0];
	}	

	/**Get the CCTV password setting from the <code>cctv.properties</code>
	 * file.
	 * 
	 * @return the password, or null if it is not set.
	 */
	public static String getPassword(){
		
		return getUserNamePassword()[1];
	}

	/**Get the CCTV user/password setting from the <code>cctv.properties</code>
	 * file.
	 * 
	 * @return the user/password pair, or null if it is not set.
	 */
	private static String[] getUserNamePassword(){
		
		URI uri = getUrlWithCredentials();
		String[] namePassword = uri.getUserInfo().split(USERNAME_PASSWORD_SEPARATOR);
		
		return namePassword;
	}	
}