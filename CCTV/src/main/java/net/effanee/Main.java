/**
 * This file is part of CCTV.
 *
 * CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.effanee;

import java.util.logging.Level;
import java.util.logging.Logger;

import net.effanee.signal.AwaitSignal;

/**
 * <p>Loads the application. Can be closed by sending the POSIX signals:-</p>
 * <ul>
 * <li><code>SIGTERM (15) [the default for Linux kill services]</code></li>
 * <li><code>SIGINT (2) [from the Windows/Linux console - "Ctrl + c"]</code></li>
 * </ul>
 * <p>to the program. These signals are common between Windows and Linux.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class Main {
	
	private static final Logger LOGGER = Logger.getLogger(Main.class.getName());
	
	/**
	 * Start here.
	 * 
	 * An exit status of 0 indicates a good exit. A status of -1 is bad.
	 * 
	 * @param args command line arguments. None are supported at the moment.
	 */
	public static void main(String[] args){

		LOGGER.log(Level.INFO, "Starting.");
		
		// Used for UPnP search.
		System.setProperty("java.net.preferIPv4Stack" , "true");
		
		// Create and store the stream.
		CctvController controller = new CctvController();
		controller.startRecording();
		
		// Wait on receiving a POSIX signal.
		int exitStatus = AwaitSignal.waitOnSignal();
		
//		try {
//			System.out.println("Start sleep");
//			// 2 minutes.
//			Thread.sleep(1000 * 60 * 2);
//			// 2 hours
//			//Thread.sleep(1000 * 60 * 60 * 2);
//			System.out.println("Stop sleep");
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
		
		// Close and stop recording the stream.
		controller.stopRecording();
		
		shutdown();
		
		LOGGER.log(Level.INFO, "Stopping.");
		System.exit(exitStatus);		
	}

	/**
	 * Shutdown tasks.
	 */
	private static void shutdown(){
		
		//LOGGER.log(Level.INFO, "Running shutdown tasks."); 
	}
}
