/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.signal;

// sun.* packages may be removed without notice.
// Suppress Eclipse warnings via Properties -> Java Build Path -> Libraries ->
// JRE System Library -> Access rules: Accessible - sun/misc/**
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import sun.misc.Signal;
import sun.misc.SignalHandler;

/**
 * <p>Conditionally wait, until interrupted by one of the following POSIX
 * signals:-</p>
 * <ul>
 * <li><code>SIGTERM (15) [the default for Linux kill services]</code></li>
 * <li><code>SIGINT (2) [from the Windows/Linux console - "Ctrl + c"]</code></li>
 * </ul>
 * <p>These signals are common between Windows and Linux. This class is static,
 * so multiple waiting threads all share the same wait condition, and all
 * waiting threads will be allowed to progress when one of the above handled
 * signals is received. Signals may be received either from the operating
 * system, via the registered Virtual Machines sun.misc.SignalHandler or, from
 * this classes issueSigInt/issueSigTerm methods.</p>
 * 
 * <p>This class is intended to be used application wide.<p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2015</p>
 */
public final class AwaitSignal {

	private static final Logger LOGGER = Logger.getLogger(AwaitSignal.class.getName());
	
	private static final Lock rLock = new ReentrantLock();
	private static final Condition canExit= rLock.newCondition(); 
	private static final AtomicBoolean isExiting = new AtomicBoolean(false);
	
	public static final int GOOD_EXIT = 0;
	public static final int BAD_EXIT = -1;
	
	// Initialise the signal handlers.
	static {
		createSignalHandlers();
	}
	
	private AwaitSignal(){
		// Class should not be instantiated.
	}
	
	/**
	 * Wait on the receipt of a SIGTERM or SIGINT signal. Block progress of the
	 * invoking thread until one of these signals is received.
	 * 
	 * @return 0 on a good exit, else -1.
	 */
	public static int waitOnSignal(){
		
		int exitStatus = GOOD_EXIT;
		
		rLock.lock();
		try{
			LOGGER.log(Level.INFO, "Waiting on SIGTERM or SIGINT.");
			
			// Loop to avoid exiting on "spurious wake-ups".
			while(!isExiting.get()){
				// Wait here until a SIGTERM or SIGINT signal is received to
				// indicate a shutdown.
				// The rLock is released on the await() invocation below, so all
				// all waiting threads will sleep here, until they are
				// separately awakened, and reacquire the lock.
				canExit.await();
			}
		}
		catch (InterruptedException e) {
			LOGGER.log(Level.SEVERE, "Received an unexpected interrupt, (maybe a POSIX signal other than SIGTERM or SIGINT), exiting.", e);
			exitStatus = BAD_EXIT;
		}
		finally {
			// Each exiting thread must release its lock, to allow any other
			// waiting threads to acquire the lock and proceed.
			rLock.unlock();
		}
		
		LOGGER.log(Level.INFO, "Exit status is " + exitStatus + ".");
		
		return exitStatus;
	}
	
	/**
	 * Raise the SIGINT signal in the current Java VM process. Has the effect of
	 * releasing any waiting threads.
	 */
	public static void issueSigInt(){
		
		Signal signal = new Signal("INT");
		
		LOGGER.log(Level.INFO, "Received signal " + signal.getName() + ".");

		Signal.raise(signal);
	}

	/**
	 * Raise the SIGTERM signal in the current Java VM process. Has the effect of
	 * releasing any waiting threads.
	 */
	public static void issueSigTerm(){
		
		Signal signal = new Signal("TERM");
		
		LOGGER.log(Level.INFO, "Received signal " + signal.getName() + ".");

		Signal.raise(signal);
	}

	/**
	 * Register a signal handler for the SIGTERM and SIGINT POSIX signals.
	 */
	private static void createSignalHandlers(){
		
		SignalHandler handler = new SignalHandlerImpl();
		
		Signal.handle(new Signal("TERM"), handler);
		Signal.handle(new Signal("INT"), handler);
		
		LOGGER.log(Level.FINE, "Registered SIGTERM and SIGINT handler.");
	}
	
	/**
	 * Implement the SignalHandler interface. When a signal handler is
	 * registered a new Signal Dispatcher thread is created that listens for
	 * signals. When a signal is received, the matching signal handler is run in
	 * its own separate thread.
	 * 
	 * Uses the sun.misc.SignalHandler. Oracle:-
	 * 
	 * <a href="http://www.oracle.com/technetwork/java/faq-sun-packages-142232.html">
	 * Why Developers Should Not Write Programs That Call 'sun' Packages</a>
	 * 
	 * Extract:-
	 * 
	 * <q> The sun.* packages are not part of the supported, public interface.
	 * A Java program that directly calls into sun.* packages is not guaranteed
	 * to work on all Java-compatible platforms. In fact, such a program is not
	 * guaranteed to work even in future versions on the same platform.</q>
	 * 
	 * (Using a shutdown hook may be a better alternative).
	 */
	private static class SignalHandlerImpl implements SignalHandler {
	
		// Multiple signals may be received, and the same signal may be received
		// more than once.
		public void handle(Signal signal) {

			LOGGER.log(Level.INFO, "Received signal " + signal.getName() + ".");

			rLock.lock();
			try {
				// Use isExiting to ensure that the signalAll() method is only
				// invoked once.
				if (!isExiting.get()) {
					
					LOGGER.log(Level.INFO, "Set isExiting to true.");
					
					// Change state to exiting.
					isExiting.getAndSet(true);

					// The rLock must be held before the canExit() condition can
					// be signalled. The threads blocked on the await() method,
					// in the above waitOnSignal() method, will not recommence
					// until they can secure the rLock, which is freed in the
					// finally clause below.
					canExit.signalAll();
				}
				else {
					return;
				}
			}
			finally {
				rLock.unlock();
			}
		}
	}
}
