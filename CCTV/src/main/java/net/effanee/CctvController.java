/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee;

import java.nio.ByteBuffer;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.effanee.buffer.RingBuffer;
import net.effanee.organise.RtpProcessor;
import net.effanee.protocol.rtp.client.RtcpConnection;
import net.effanee.protocol.rtp.client.RtpConnection;
import net.effanee.protocol.rtp.client.RtpFileWriter;
import net.effanee.protocol.rtsp.client.RtspManager;

/**
 * <p>This class communicates with the CCTV camera and processes its output stream.
 * It initiates a new stream and translates it into a series of H.264 files in
 * the directory specified in the configuration file. Streams must be explicitly
 * closed using the stopRecording() method.</p>
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class CctvController {

	private static final Logger LOGGER = Logger.getLogger(CctvController.class.getName());

	// Buffer size in seconds.
	private static final int BUFFER_SIZE = 5;
	
	private RtspManager rtspManager;
	private RtpConnection rtpConnection;
	private RtcpConnection rtcpConnection;
	private RtpFileWriter rtpWriter;
	private RtpProcessor rtpProcessor;
	
	private RingBuffer<ByteBuffer> buffer;

	private int serverRtpPort;
	private int serverRtcpPort;
	
	/**
	 * Create a new CCTV controller.
	 */
	public CctvController() {
	}

	/**
	 * Connect to the camera and request the transmission of a new media stream.
	 */
	public void startRecording() {
		
		// Use RSTP to create a new stream.
		createStream();
		LOGGER.log(Level.INFO, "RTSP connected to camera.");
		
		// Use RTP to receive the data.
		createDataConnection();
		LOGGER.log(Level.INFO, "Created RTP connection to camera.");
		
		// Use RTCP to control the data.
		createControlConnection();
		LOGGER.log(Level.INFO, "Created RTCP connection to camera.");
        
		// Write the received data to a sequence of files.
		writeData();
		LOGGER.log(Level.INFO, "Created RTP writer to store raw camera data in file.");
        
        // Process the received data, when notified a new file has been created.
		processData();
		LOGGER.log(Level.INFO, "Created RTP processor to read raw data from file and write H.264 files.");

		// Ask the camera to start sending data.
		rtspManager.sendPlay();
		LOGGER.log(Level.INFO, "RTSP request camera starts sending data.");
	}

	/**
	 * Close the stream from the camera, and write any remaining data.
	 */
	public void stopRecording() {

		// Use RSTP to close the stream.
		rtspManager.sendTeardown();
		LOGGER.log(Level.INFO, "RTSP request camera stops sending data.");
		
		// Give the stream chance to close.
		sleepMilliSeconds(500);
		
		// Close connections.
		rtpConnection.stop();
		rtcpConnection.stop();
		LOGGER.log(Level.INFO, "Close RTP and RTCP connections.");
		
		// Closing the buffer will cause the RTPWriter to also exit, when the
		// buffer is emptied.
		buffer.close();
		LOGGER.log(Level.INFO, "Stop writing raw data.");
		
		// Close the processor.
		rtpProcessor.stop();
		LOGGER.log(Level.INFO, "Stop writing H.264 files.");
	}
	
	/**
	 * Create a new connection to the camera.
	 */
	private void createStream(){
		
		rtspManager = new RtspManager(Configuration.getUrlWithCredentials(),
									Configuration.getRtpClientPort(),
									Configuration.getRtcpClientPort());
		// Create stream.
		rtspManager.sendOptions();
		rtspManager.sendDescribe();
		rtspManager.sendSetup();
		
		// Get ports.
		setServerPorts(rtspManager.getServerRtpPort(), rtspManager.getServerRtcpPort());
	}
	
	/**
	 * Create a new data connection to the camera.
	 */
	private void createDataConnection(){
		
		// Create a ring buffer of ByteBuffer objects.
		Supplier<ByteBuffer> generator  = () -> ByteBuffer.allocate(Configuration.getRtpClientPayloadSize());
		buffer = new RingBuffer<>(Configuration.getRtpClientPacketsPerSecond() * BUFFER_SIZE, generator);
		
		// Create RTP connection thread.
		rtpConnection = new RtpConnection(Configuration.getUrlWithCredentials(),
											Configuration.getRtpClientPort(),
											getServerRtpPort(),
											buffer,
											Configuration.getLogPacketsPerSecond());
        new Thread(rtpConnection).start();
	}
	
	/**
	 * Create a new control connection to the camera.
	 */
	private void createControlConnection(){
		
		// Create RTCP connection thread.
		rtcpConnection = new RtcpConnection(Configuration.getUrlWithCredentials(),
											Configuration.getRtcpClientPort(),
											getServerRtcpPort(),
											Configuration.getRtpClientPayloadSize());
        new Thread(rtcpConnection).start();
	}
	
	/**
	 * Create a new writer to write the camera data to file.
	 */
	private void writeData(){
		
		// Write the received data to a file.
        rtpWriter = new RtpFileWriter(buffer,
						        		Configuration.getOutputDirectory(),
						        		Configuration.getRtpClientPacketsPerSecond());
        new Thread(rtpWriter).start();
	}
	
	/**
	 * Create a new processor to parse the data files.
	 */
	private void processData(){
		
        rtpProcessor = new RtpProcessor(rtpWriter,
						        		Configuration.getOutputDirectory(),
						        		Configuration.getFriendlyName(),
						        		Configuration.getMaximumDiscSpace());
        new Thread(rtpProcessor).start();
	}
	
	private void sleepMilliSeconds(int milliSeconds){
		
		try {
			Thread.sleep(milliSeconds);
		}
		catch (InterruptedException e) {
			// Ignore.
		}
	}
	
	private void setServerPorts(int serverRtpPort, int serverRtcpPort){
		
		this.serverRtpPort = serverRtpPort;
		this.serverRtcpPort = serverRtcpPort;
	}

	private int getServerRtpPort() {
		return serverRtpPort;
	}

	private int getServerRtcpPort() {
		return serverRtcpPort;
	}
}
