/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * ("at your option") any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.http;

/**
 * <h2>RFC 2616 - Hypertext Transfer Protocol -- HTTP/1.1</h2>
 * 
 * <h3>6.1.1 Status Code and Reason Phrase</h3>
 * 
 * <p>The Status-Code element is a 3-digit integer result code of the attempt to
 * understand and satisfy the request. The Reason-Phrase is intended to give a
 * short textual description of the Status-Code. The Status-Code is intended for
 * use by automata and the Reason-Phrase is intended for the human user. The
 * client is not required to examine or display the Reason-Phrase.</p>
 * 
 * <p>The first digit of the Status-Code defines the class of response. The last
 * two digits do not have any categorization role. There are 5 values for the
 * first digit:-</p>
 * <pre>
 * 1xx: Informational - Request received, continuing process
 * 2xx: Success - The action was successfully received, understood, and accepted
 * 3xx: Redirection - Further action must be taken in order to complete the request
 * 4xx: Client Error - The request contains bad syntax or cannot be fulfilled
 * 5xx: Server Error - The server failed to fulfill an apparently valid request
 * </pre>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2015</p>
 */
public enum HttpResponseClass {

	CODE_1(1, "Informational"),
	CODE_2(2, "Success"),
	CODE_3(3, "Redirection"),
	CODE_4(4, "Client Error"),
	CODE_5(5, "Server Error");

	private final int responseClass;
	private final String category;

	private HttpResponseClass(int statusCode, String reasonPhrase) {
		this.responseClass = statusCode;
		this.category = reasonPhrase;
	}
	
	public int statusCode() {
		return responseClass;
	}
	
	public String reasonPhrase(){
		return category;
	}
	
	@Override
	public String toString() {
		return HttpResponseClass.class.getSimpleName()
				+ "["
				+ ("responseClass=" + responseClass + ", ")
				+ ("category=" + category)
				+ "]";
	}
}
