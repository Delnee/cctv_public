/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * ("at your option") any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.http;

/**
 * <h2>RFC 2616 - Hypertext Transfer Protocol -- HTTP/1.1</h2>
 * 
 * <h3>3.1 HTTP Version</h3>
 * 
 * <p>HTTP uses a "major.minor" numbering scheme to indicate versions of the
 * protocol. The version of an HTTP message is indicated by an HTTP-Version
 * field in the first line of the message.</p>
 * <pre>
 * HTTP-Version = "HTTP" "/" 1*DIGIT "." 1*DIGIT
 * </pre>
 * <p>Note that the major and minor numbers MUST be treated as separate integers
 * and that each MAY be incremented higher than a single digit. Thus, HTTP/2.4
 * is a lower version than HTTP/2.13.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2015</p>
 */
public enum HttpVersion {
	
	VERSION_1_0 ("HTTP/1.0"),
	VERSION_1_1 ("HTTP/1.1"),
	VERSION_UNKNOWN ("UNKNOWN");

	private final String fieldName;
	
	HttpVersion(String fieldName) {
        this.fieldName = fieldName;
    }
	
	public String fieldName(){
		return fieldName;
	}
	
	@Override
	public String toString() {
		return HttpVersion.class.getSimpleName()
				+ "["
				+ ("fieldName=" + fieldName)
				+ "]";
	}
}
