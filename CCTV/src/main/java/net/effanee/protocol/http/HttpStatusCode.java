/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * ("at your option") any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.http;

/**
 * <h2>RFC 2616 - Hypertext Transfer Protocol -- HTTP/1.1</h2>
 * 
 * <h3>6.1.1 Status Code and Reason Phrase</h3>
 * 
 * <p>The Status-Code element is a 3-digit integer result code of the attempt to
 * understand and satisfy the request. The Reason-Phrase is intended to give a
 * short textual description of the Status-Code. The Status-Code is intended for
 * use by automata and the Reason-Phrase is intended for the human user. The
 * client is not required to examine or display the Reason-Phrase.</p>
 * 
 * <p>The first digit of the Status-Code defines the class of response. The last
 * two digits do not have any categorization role. There are 5 values for the
 * first digit:-</p>
 * <pre>
 * 1xx: Informational - Request received, continuing process
 * 2xx: Success - The action was successfully received, understood, and accepted
 * 3xx: Redirection - Further action must be taken in order to complete the request
 * 4xx: Client Error - The request contains bad syntax or cannot be fulfilled
 * 5xx: Server Error - The server failed to fulfill an apparently valid request
 * </pre>
 * 
 * <p>The reason phrases listed here are only recommendations -- they MAY be
 * replaced by local equivalents without affecting the protocol.</p>
 * <pre>
 * Status-Code =
 * "100" ; Section 10.1.1: Continue
 * "101" ; Section 10.1.2: Switching Protocols
 * "200" ; Section 10.2.1: OK
 * "201" ; Section 10.2.2: Created
 * "202" ; Section 10.2.3: Accepted
 * "203" ; Section 10.2.4: Non-Authoritative Information
 * "204" ; Section 10.2.5: No Content
 * "205" ; Section 10.2.6: Reset Content
 * "206" ; Section 10.2.7: Partial Content
 * "300" ; Section 10.3.1: Multiple Choices
 * "301" ; Section 10.3.2: Moved Permanently
 * "302" ; Section 10.3.3: Found
 * "303" ; Section 10.3.4: See Other
 * "304" ; Section 10.3.5: Not Modified
 * "305" ; Section 10.3.6: Use Proxy
 * "307" ; Section 10.3.8: Temporary Redirect
 * "400" ; Section 10.4.1: Bad Request
 * "401" ; Section 10.4.2: Unauthorized
 * "402" ; Section 10.4.3: Payment Required
 * "403" ; Section 10.4.4: Forbidden
 * "404" ; Section 10.4.5: Not Found
 * "405" ; Section 10.4.6: Method Not Allowed
 * "406" ; Section 10.4.7: Not Acceptable
 * "407" ; Section 10.4.8: Proxy Authentication Required
 * "408" ; Section 10.4.9: Request Time-out
 * "409" ; Section 10.4.10: Conflict
 * "410" ; Section 10.4.11: Gone
 * "411" ; Section 10.4.12: Length Required
 * "412" ; Section 10.4.13: Precondition Failed
 * "413" ; Section 10.4.14: Request Entity Too Large
 * "414" ; Section 10.4.15: Request-URI Too Large
 * "415" ; Section 10.4.16: Unsupported Media Type
 * "416" ; Section 10.4.17: Requested range not satisfiable
 * "417" ; Section 10.4.18: Expectation Failed
 * "500" ; Section 10.5.1: Internal Server Error
 * "501" ; Section 10.5.2: Not Implemented
 * "502" ; Section 10.5.3: Bad Gateway
 * "503" ; Section 10.5.4: Service Unavailable
 * "504" ; Section 10.5.5: Gateway Time-out
 * "505" ; Section 10.5.6: HTTP Version not supported
 * 
 * extension-code = 3DIGIT
 * Reason-Phrase = *{@literal<}TEXT, excluding CR, LF{@literal>}
 * </pre>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2015</p>
 */
public enum HttpStatusCode {

	CODE_100(100, "Continue", HttpResponseClass.CODE_1),
	CODE_101(101, "Switching Protocols", HttpResponseClass.CODE_1),
	CODE_200(200, "OK", HttpResponseClass.CODE_2),
	CODE_201(201, "Created", HttpResponseClass.CODE_2),
	CODE_202(202, "Accepted", HttpResponseClass.CODE_2),
	CODE_203(203, "Non-Authoritative Information", HttpResponseClass.CODE_2),
	CODE_204(204, "No Content", HttpResponseClass.CODE_2),
	CODE_205(205, "Reset Content", HttpResponseClass.CODE_2),
	CODE_206(206, "Partial Content", HttpResponseClass.CODE_2),
	CODE_300(300, "Multiple Choices", HttpResponseClass.CODE_3),
	CODE_301(301, "Moved Permanently", HttpResponseClass.CODE_3),
	CODE_302(302, "Moved Temporarily", HttpResponseClass.CODE_3),
	CODE_303(303, "See Other", HttpResponseClass.CODE_3),
	CODE_304(304, "Not Modified", HttpResponseClass.CODE_3),
	CODE_305(305, "Use Proxy", HttpResponseClass.CODE_3),
	CODE_307(307, "Temporary Redirect", HttpResponseClass.CODE_3),
	CODE_400(400, "Bad Request", HttpResponseClass.CODE_4),
	CODE_401(401, "Unauthorized", HttpResponseClass.CODE_4),
	CODE_402(402, "Payment Required", HttpResponseClass.CODE_4),
	CODE_403(403, "Forbidden", HttpResponseClass.CODE_4),
	CODE_404(404, "Not Found", HttpResponseClass.CODE_4),
	CODE_405(405, "Method Not Allowed", HttpResponseClass.CODE_4),
	CODE_406(406, "Not Acceptable", HttpResponseClass.CODE_4),
	CODE_407(407, "Proxy Authentication Required", HttpResponseClass.CODE_4),
	CODE_408(408, "Request Time-out", HttpResponseClass.CODE_4),
	CODE_409(409, "Conflict", HttpResponseClass.CODE_4),
	CODE_410(410, "Gone", HttpResponseClass.CODE_4),
	CODE_411(411, "Length Required", HttpResponseClass.CODE_4),
	CODE_412(412, "Precondition Failed", HttpResponseClass.CODE_4),
	CODE_413(413, "Request Entity Too Large", HttpResponseClass.CODE_4),
	CODE_414(414, "Request-URI Too Large", HttpResponseClass.CODE_4),
	CODE_415(415, "Unsupported Media Type", HttpResponseClass.CODE_4),
	CODE_416(415, "Requested range not satisfiable", HttpResponseClass.CODE_4),
	CODE_417(415, "Expectation Failed", HttpResponseClass.CODE_4),
	CODE_500(500, "Internal Server Error", HttpResponseClass.CODE_5),
	CODE_501(501, "Not Implemented", HttpResponseClass.CODE_5),
	CODE_502(502, "Bad Gateway", HttpResponseClass.CODE_5),
	CODE_503(503, "Service Unavailable", HttpResponseClass.CODE_5),
	CODE_504(504, "Gateway Time-out", HttpResponseClass.CODE_5),
	CODE_505(505, "HTTP Version Not Supported", HttpResponseClass.CODE_5),
	CODE_599(599, "Unknown", HttpResponseClass.CODE_5);

	private final int statusCode;
	private final String reasonPhrase;
	private final HttpResponseClass responseClass;

	private HttpStatusCode(int statusCode, String reasonPhrase, HttpResponseClass responseClass) {
		this.statusCode = statusCode;
		this.reasonPhrase = reasonPhrase;
		this.responseClass = responseClass;
	}
	
	public int statusCode() {
		return statusCode;
	}
	
	public String reasonPhrase(){
		return reasonPhrase;
	}
	
	public  HttpResponseClass getResponseClass(){
		return  responseClass;
	}
	
	@Override
	public String toString() {
		return HttpStatusCode.class.getSimpleName()
				+ "["
				+ ("statusCode=" + statusCode + ", ")
				+ ("reasonPhrase=" + reasonPhrase + ", ")
				+ ("responseClass=" + responseClass)
				+ "]";
	}
}
