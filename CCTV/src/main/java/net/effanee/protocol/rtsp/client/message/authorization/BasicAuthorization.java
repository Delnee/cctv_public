/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.authorization;

import org.apache.commons.codec.binary.Base64;

import net.effanee.protocol.rtsp.client.message.header.WwwAuthenticate.Scheme;
/**
 * <h2>RFC2617 - HTTP Authentication: Basic and Digest Access Authentication</h2>
 * 
 * <h3>4.1 Authentication of Clients using Basic Authentication</h3>
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class BasicAuthorization extends Authorization {

	private String credentials;
	
	public BasicAuthorization(Scheme scheme, String userId, String password) {

		super(userId, password);
		updateCredentials(scheme);
	}
	
	@Override
	public void updateCredentials(Scheme scheme){
		
		setScheme(scheme);
		
		String plainBasicCookie = getUserId() + KEY_VALUE_SEPARATOR + getPassword();
		String encodedBasicCookie = Base64.encodeBase64String(plainBasicCookie.getBytes());
		credentials = encodedBasicCookie;
	}
	
	@Override
	public String getCredentialsValue() {
		
		return getScheme().getAuthScheme().key() + " " + credentials;
	}

	@Override
	public String toString() {
		return "BasicAuthorization ["
				+ ("credentials=" + getCredentialsValue())
				+ ", "
				+ super.toString()
				+ "]";
	}
}
