/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.header;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.effanee.protocol.rtsp.client.RstpProtocolException;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * <p>Concrete Header class.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class WwwAuthenticate extends Header{
	
	public enum AuthScheme {

		BASIC("Basic"),
		DIGEST("Digest");

		private final String key;

		private AuthScheme(String token) {
			this.key = token;
		}
		
		public String key() {
			return key;
		}
		
		@Override
		public String toString() {
			return "AuthScheme ["
					+ ("enum=" + this.name() + ", ")
					+ ("token=" + key)
					+ "]";
		}
	}
	
	public class Scheme{
		
		private static final String REALM_KEY = "realm";
		private static final String NONCE_KEY = "nonce";
		private static final String STALE_KEY = "stale";
		
		private final AuthScheme authScheme;
		private final  Map<String, String> params;
		
		Scheme(AuthScheme authScheme, Map<String, String> params){
			
			this.authScheme = authScheme;
			this.params = params;
		}
		
		public AuthScheme getAuthScheme() {
			return authScheme;
		}
		
		public String getRealmKey() {
			return params.get(REALM_KEY);
		}

		public String getNonceKey() {
			return params.get(NONCE_KEY);
		}

		public boolean getStaleKey() {
			boolean stale = Boolean.parseBoolean(params.get(STALE_KEY));
			return stale;
		}
		
		@Override
		public String toString() {
			return "Scheme ["
					+ ("authScheme=" + authScheme + ", ")
					+ ("params=" + params)
					+ "]";
		}
	}
	
	private static final String SEPARATOR = " ";
	private static final String PARAM_SEPARATOR = ",";
	private static final String KEY_VALUE_SEPARATOR = "=";
//	private static final AuthScheme FAVOURED = AuthScheme.BASIC;
//	private static final AuthScheme UNFAVOURED = AuthScheme.DIGEST;
	
	private Scheme scheme;
	
	WwwAuthenticate(String[] lines) throws RstpProtocolException {
		
		//TODO ensure that the string lines equals the selected auth method.
		//the value filed in the super class.
		super(HeaderKey.WWW_AUTHENTICATE, lines);
		setPrefereredScheme(lines);
	}

	public Scheme getScheme(){
		
		return scheme;
	}
	
	//WWW-Authenticate: Digest realm="4419b61f978b", nonce="082dc9a99d51d5c9dcdc68b7468f34f0", stale="FALSE"
	//WWW-Authenticate: Basic realm="4419b61f978b"
	//Scheme Realm Auth-Param (key="quoted string")
	private void setPrefereredScheme(String[] lines) throws RstpProtocolException{
		
		List<String> values = getValues(lines);
		List<Scheme> schemes = getSchemes(values);
		scheme = pickPreferred(schemes);
	}
	
	private Scheme pickPreferred(List<Scheme> schemes) throws RstpProtocolException{
		
		if(schemes.size() == 0){
			throw new RstpProtocolException("The RTSP response Header "
					+ HeaderKey.WWW_AUTHENTICATE.key()
					+ " cannot be parsed to an authentication scheme.");
		}
		
		//return pickScheme(schemes);
		return schemes.get(0);
	}
	
//	private Scheme pickScheme(List<Scheme> schemes) throws RstpProtocolException{
//		
//		Scheme favoured = null;
//		Scheme unfavoured = null;
//		
//		for(Scheme scheme: schemes){
//			if(scheme.getAuthScheme() == FAVOURED){
//				favoured = scheme;
//			}
//			else if(scheme.getAuthScheme() == UNFAVOURED){
//				unfavoured = scheme;
//			}
//		}
//		
//		if(favoured != null){
//			return favoured;
//		}
//		else if(unfavoured != null){
//			return unfavoured;
//		}
//		
//		throw new RstpProtocolException("The RTSP response Header "
//				+ HeaderKey.WWW_AUTHENTICATE.key()
//				+ " cannot be parsed to an authentication scheme.");
//	}
	
	private List<Scheme> getSchemes(List<String> values) throws RstpProtocolException {
		
		List<Scheme> schemes = new ArrayList<Scheme>();
		for (String value : values) {
			String[] tokens = getTokens(value, SEPARATOR);
			if (tokens[0].trim().equalsIgnoreCase(AuthScheme.BASIC.key())) {
				
				Scheme scheme = getBasicScheme(tokens);
				if(scheme != null){
					schemes.add(scheme);
				}
			}
			else if (tokens[0].trim().equalsIgnoreCase(AuthScheme.DIGEST.key())) {
				
				Scheme scheme = getDigestScheme(tokens);
				if(scheme != null){
					schemes.add(scheme);
				}
			}			
		}
		return schemes;
	}
	
	//WWW-Authenticate: Basic realm="4419b61f978b"
	private Scheme getBasicScheme(String[] tokens) throws RstpProtocolException{
		
		if(tokens.length < 2){
			throw new RstpProtocolException("The RTSP response Header "
					+ HeaderKey.WWW_AUTHENTICATE.key()
					+ " has value "
					+ StringUtils.join(tokens, SEPARATOR)
					+ " which cannot be parsed to an authentication scheme.");
		}
		
		//Remove the scheme, we know it is BASIC.
		tokens = ArrayUtils.remove(tokens, 0);
		
		Map<String, String> params = new HashMap<String, String>();
		for(String token: tokens){
			params.putAll(getKeyValue(token));
		}
		
		if(params.size() == 0){
			throw new RstpProtocolException("The RTSP response Header "
					+ HeaderKey.WWW_AUTHENTICATE.key()
					+ " has value "
					+ StringUtils.join(tokens, SEPARATOR)
					+ " which cannot be parsed to an authentication scheme.");
		}
		
		return new Scheme(AuthScheme.BASIC, params);
	}
	
	//Digest realm="4419b61f978b", nonce="082dc9a99d51d5c9dcdc68b7468f34f0", stale="FALSE"
	private Scheme getDigestScheme(String[] tokens) throws RstpProtocolException{
		
		if(tokens.length < 2){
			throw new RstpProtocolException("The RTSP response Header "
					+ HeaderKey.WWW_AUTHENTICATE.key()
					+ " has value "
					+ StringUtils.join(tokens, SEPARATOR)
					+ " which cannot be parsed to an authentication scheme.");
		}
		
		//Remove the scheme, we know it is DIGEST.
		//(realm="4419b61f978b",) (nonce="082dc9a99d51d5c9dcdc68b7468f34f0",) (stale="FALSE")
		tokens = ArrayUtils.remove(tokens, 0);
		
		//Add together params separated by SEPARATOR, and break on PARAM_SEPARATOR.
		//(realm="4419b61f978b") (nonce="082dc9a99d51d5c9dcdc68b7468f34f0") (stale="FALSE")
		String paramLine = StringUtils.join(tokens, SEPARATOR);
		tokens = getTokens(paramLine, PARAM_SEPARATOR);
		
		//Split key/value pairs.
		//{nonce=082dc9a99d51d5c9dcdc68b7468f34f0, realm=4419b61f978b, stale=FALSE}
		Map<String, String> params = new HashMap<String, String>();
		for(String token: tokens){
			params.putAll(getKeyValue(token.trim()));
		}
		
		if(params.size() == 0){
			throw new RstpProtocolException("The RTSP response Header "
					+ HeaderKey.WWW_AUTHENTICATE.key()
					+ " has value "
					+ StringUtils.join(tokens, SEPARATOR)
					+ " which cannot be parsed to an authentication scheme.");
		}
		
		return new Scheme(AuthScheme.DIGEST, params);
	}
	
	private Map<String, String> getKeyValue(String token){
		
		Map<String, String> keyValue = new HashMap<String, String>();
		
		String[] tokens = StringUtils.split(token, KEY_VALUE_SEPARATOR);
		if (tokens.length > 1) {
			
			String key = tokens[0].trim();
			String value = StringUtils.remove(tokens[1].trim(), "\"");
			keyValue.put(key, value);
		}
		
		return keyValue;
	}
	
	private List<String> getValues(String[] lines) {
		
		List<String> schemes = new ArrayList<String>();
		for (String line : lines) {
			String[] tokens = getTokens(line, KEY_SEPARATOR);
			if (tokens[0].trim().equalsIgnoreCase(HeaderKey.WWW_AUTHENTICATE.key())) {
				schemes.add(tokens[1].trim());
			}
		}
		return schemes;
	}

	private String[] getTokens(String line, String separator){
		
		String[] tokens = StringUtils.split(line, separator);
		return tokens;
	}
	
	@Override
	public String toString() {
		return "WwwAuthenticate ["
				+ ("scheme=" + scheme)
				+ ", "
				+ super.toString()
				+ "]";
	}
}
