/**
 * This file is part of CCTV.
 *
 * CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.effanee.protocol.rtsp.client.message.request;

import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

import net.effanee.protocol.rtsp.client.Context;
import net.effanee.protocol.rtsp.client.message.response.Response;

/**
 * <p>Abstract Request class. Used to provide common behaviour.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public abstract class Request {

	//private static final Logger LOGGER = Logger.getLogger(Request.class.getName());
	
	protected Context context;
	private Response response;
	private String requestLines;
	
	/**
	 * Set the bytes of the header.
	 * 
	 * @param request - a String containing the bytes.
	 */
	protected void setHeaderBytes(String request) {
		
		this.requestLines = request;
	}
	
	/**
	 * Get the header bytes.
	 * 
	 * @return the header bytes.
	 */
	public byte[] getBytes() {
		
		return getHeaderBytes();
	}

	private byte[] getHeaderBytes() {
		
		ByteBuffer buffer = StandardCharsets.UTF_8.encode(requestLines);
		byte[] bytes = new byte[buffer.remaining()];
		buffer.get(bytes);
		
		return bytes;
	}
	
	/**
	 * Get camera URI.
	 * 
	 * @return
	 */
	public URI getUri(){
		
		return context.getUri();
	}

	public int getClientRtpPort() {

		return context.getClientRtpPort();
	}

	public int getClientRtcpPort() {

		return context.getClientRtcpPort();
	}	
	
	/**
	 * Get CSeq value.
	 * 
	 * @return
	 */
	public int getSequenceValue(){
		
		return context.getSequenceValue();
	}
	
	/**
	 * Is authentication required?
	 * 
	 * @return
	 */
	public boolean requiresAuthentication(){
	
		return context.requiresAuthentication();
	}
	
	/**
	 * Get credentials.
	 * 
	 * @return
	 */
	public String getAuthorizationValue(){
		
		return context.getAuthorisationValue();
	}

	/**
	 * Has an RTSP session been created?
	 * 
	 * @return
	 */
	public boolean hasSession(){
		
		return context.hasSession();
	}
	
	/**
	 * Get the session key.
	 * 
	 * @return
	 */
	public String getSessionValue(){
		
		return context.getSessionValue();
	}
	
	/**
	 * Get the control URI, the end-point to which RTSP messages are sent.
	 * 
	 * @return
	 */
	public String getControlUri(){
		
		return context.getControlUri();
	}
	
	/**
	 * Get the header.
	 * 
	 * @return
	 */
	public String getRequestLines(){

		return requestLines;
	}
	
	/**
	 * Get the Response.
	 * 
	 * @return
	 */
	public Response getResponse() {
		
		return response;
	}

	/**
	 * Set the Response.
	 * 
	 * @param response - the Response.
	 */
	public void setResponse(Response response) {
		
		this.response = response;
	}
	
	@Override
	public String toString() {
		return "Request ["
				+ ("context=" + context + ", ")
				+ ("request=" + requestLines)
				+ "]";
	}
}
