/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.state;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>The State Machine ensures the RTSP protocol moves between states correctly.
 * If an attempt is made to transition between states that is incorrect, a
 * IllegalStateException is thrown.</p>
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class StateMachine {

	private static final Logger LOGGER = Logger.getLogger(StateMachine.class.getName());
	
	// States.
	private final State init;
	private final State ready;
	private final State playing;
	private final State recording;
	
	// Current state.
	private State state;
	
	/**
	 * Create a new state machine whose initial state is InitState.
	 */
	public StateMachine() {

		init = new InitState(this);
		ready = new ReadyState(this);
		playing = new PlayingState(this);
		recording = new RecordingState(this);
		
		state = init;
	}

	/**
	 * Execute non-state changing transition.
	 */
	public void options(){
		state.options();
		LOGGER.log(Level.FINEST, "Transitioned to state " + state.toString());
	}
	
	/**
	 * Execute non-state changing transition.
	 */
	public void describe(){
		state.describe();
		LOGGER.log(Level.FINEST, "Transitioned to state " + state.toString());
	}

	/**
	 * Execute state changing transition.
	 */
	public void setup() {
		state.setup();
		LOGGER.log(Level.FINEST, "Transitioned to state " + state.toString());
	}

	/**
	 * Execute state changing transition.
	 */
	public void play() {
		state.play();
		LOGGER.log(Level.FINEST, "Transitioned to state " + state.toString());
	}

	/**
	 * Execute state changing transition.
	 */
	public void teardown() {
		state.teardown();
		LOGGER.log(Level.FINEST, "Transitioned to state " + state.toString());
	}
	
	/**
	 * Execute state changing transition.
	 */
	public void record() {
		state.record();
		LOGGER.log(Level.FINEST, "Transitioned to state " + state.toString());
	}

	/**
	 * Execute state changing transition.
	 */
	public void pause() {
		state.pause();
		LOGGER.log(Level.FINEST, "Transitioned to state " + state.toString());
	}

	/**
	 * Execute non-state changing transition.
	 */
	public void announce(){
		state.announce();
		LOGGER.log(Level.FINEST, "Transitioned to state " + state.toString());
	}

	/**
	 * Execute non-state changing transition.
	 * 
	 *  @param key - the key of the parameter to get.
	 */
	public void getParameter(String key){
		state.getParameter(key);
		LOGGER.log(Level.FINEST, "Transitioned to state " + state.toString());
	}

	/**
	 * Execute non-state changing transition.
	 * 
	 * @param key - the key of the parameter to set.
	 * @param value - the value of the parameter to set.
	 */
	public void setParameter(String key, String value){
		state.setParameter(key, value);
		LOGGER.log(Level.FINEST, "Transitioned to state " + state.toString());
	}

	/**
	 * Set this objects state.
	 */
	void setStateToInit(){
		this.state = init;
	}
	
	/**
	 * Set this objects state.
	 */
	void setStateToReady(){
		this.state = ready;
	}
	
	/**
	 * Set this objects state.
	 */
	void setStateToPlaying(){
		this.state = playing;
	}
	
	/**
	 * Set this objects state.
	 */
	void setStateToRecording(){
		this.state = recording;
	}
	
	/**
	 * Get this objects state.
	 * @return this objects state.
	 */
	State getState(){
		return this.state;
	}
}
