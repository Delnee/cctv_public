/**
 * This file is part of CCTV.
 *
 * CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.effanee.protocol.rtsp.client.message.request;

import net.effanee.protocol.rtsp.client.Context;
import net.effanee.protocol.rtsp.client.message.header.OptionsRequestHeader;

import org.apache.commons.lang3.StringUtils;

/**
 * <p>Concrete Request class.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class OptionsRequest extends Request{
	
	private OptionsRequestHeader requestHeader;

	/**
	 * Create new RTSP request method.
	 * 
	 * @param context
	 */
	OptionsRequest(Context context) {
		
		super.context = context;
		setHeaderBytes();
	}
	
	/**
	 * Set the bytes of the header.
	 * 
	 * @param request - a String containing the bytes.
	 */
	protected void setHeaderBytes() {
		
		requestHeader = new OptionsRequestHeader();
		requestHeader.setHeaders(this);
		super.setHeaderBytes(getString());
	}
	
	/**
	 * Get the header as a string.
	 * 
	 * @return
	 */
	private String getString(){
		
		return StringUtils.join(requestHeader.getLines());
	}
	
	@Override
	public String toString() {
		return "OptionsRequest ["
				+ ("requestHeader=" + requestHeader + ",")
				+ super.toString()
				+ "]";
	}
}
