/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.effanee.protocol.rtsp.client.message.request.Request;
import net.effanee.protocol.rtsp.client.message.response.Response;

/**
 * <p>A socket connection to a camera.</p>
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class Connection {

	private static final Logger LOGGER = Logger.getLogger(Connection.class.getName());
	
	// Defaults.
	private static final int DEFAULT_RTSP_PORT = 554;
	private static final int SOCKET_TIMEOUT = 3000;
	private static final int BUFFER_INCREMENT = 1024;
	private static final int MAX_HEADER_LENGTH = BUFFER_INCREMENT * 8;
	private static final byte[] HEADER_TERMINATOR = "\r\n\r\n".getBytes();
	
	// Connection socket.
	private Socket sock;
	
	/**
	 * Create a new connection to a camera.
	 * 
	 * Parameters for the connection are extracted from the Request object
	 * passed to the send() method.
	 */
	public Connection (){
	}

	/**
	 * Send a request to a camera.
	 * 
	 * @param request - a RTSP request.
	 * @return a RTSP response.
	 */
	public Response send (Request request){
		
		return readResponse(request.getResponse());
	}
	
	/**
	 * Close the socket.
	 */
	public void closeConnection(){
		
		if(sock != null & !sock.isClosed()){
			try {
				closeSocket();
			}
			catch (IOException e) {
				LOGGER.log(Level.SEVERE, e.getMessage(), e);
			}
		}
		LOGGER.log(Level.INFO, "Closed RTSP socket.");
	}
	
	/**
	 * Open a new socket connection, using the information in the request.
	 * 
	 * @param request - the request.
	 * @return a socket.
	 */
	private Socket openConnection(Request request){
		
		Socket sock = this.sock;
		try {
			if(sock == null){
				sock = openSocket(request.getUri().getHost(), getPort(request));	
				this.sock = sock;
			}
		}
		catch (IOException e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
		}
		LOGGER.log(Level.FINEST, "Opened RTSP socket.");
		return sock;
	}
	
	/**
	 * Read the response from the camera.
	 * 
	 * @param response - the type of response to read.
	 * @return a populated response.
	 */
	private Response readResponse(Response response){
		
		Socket sock = null;
		try {
			sock = openConnection(response.getRequest());

			sendRequest(sock, response.getRequest());
			InputStream in = receiveResponseHeader(sock, response);
			if(response.hasBody()){
				receiveResponseBody(in, response);	
			}
		}
		catch (IOException | RstpProtocolException e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
			closeConnection();
		}
		
		return response;
	}
	
	/**
	 * Get the request port.
	 * 
	 * @param request
	 * @return the request port, or if the port is -1, the default port.
	 */
	private int getPort(Request request){
		
		int port = request.getUri().getPort();
		if(port == -1){
			
			return DEFAULT_RTSP_PORT;
		}
		return port; 
	}
	/**
	 * Read the response header from the socket.
	 * 
	 * @param sock
	 * @param response
	 * @return
	 * @throws IOException
	 * @throws RstpProtocolException
	 */
	private InputStream receiveResponseHeader(Socket sock, Response response) throws IOException, RstpProtocolException{
		
		ByteBuffer buffer = ByteBuffer.allocate(BUFFER_INCREMENT);
		BufferedInputStream in = new BufferedInputStream(sock.getInputStream());
		
		//Collect the latest bytes to compare to the header terminating sequence.
		byte[] lastBytes = new byte[HEADER_TERMINATOR.length];
		
		//Could be - while(true). But, may as well try and catch early
		//termination by server.
		int byteCount = 0;
		byte currentByte = 0;
		while((currentByte = (byte)in.read()) != -1
				&& byteCount++ < MAX_HEADER_LENGTH){
			
			//Check and expand capacity, if necessary.
			if(!buffer.hasRemaining()){
				buffer = expandBuffer(buffer);
			}
			
			//Add the byte to the buffer.
			buffer.put(currentByte);
			
			//Update lastBytes with the latest byte.
			updateLastBytes(lastBytes, currentByte);
			
			//Check if we have reached the end of the input.
			if(Arrays.equals(lastBytes, HEADER_TERMINATOR)){
				
				//Update the response.
				setResponseHeaderBytes(response, buffer);
				break;
			}
		}
		
		return in;
	}
	
	/**
	 * Read the response body from the socket.
	 * 
	 * @param in
	 * @param response
	 * @throws IOException
	 * @throws RstpProtocolException
	 */
	private void receiveResponseBody(InputStream in, Response response) throws IOException, RstpProtocolException{
		
		ByteBuffer buffer = ByteBuffer.allocate(BUFFER_INCREMENT);
		
		int expectedBytes = response.getContentLengthValue();
		int byteCount = 0;
		byte currentByte = 0;
		
		while((currentByte = (byte)in.read()) != -1
				&& ++byteCount < expectedBytes){
			
			//Check and expand capacity, if necessary.
			if(!buffer.hasRemaining()){
				buffer = expandBuffer(buffer);
			}
			
			//Add the byte to the buffer.
			buffer.put(currentByte);
		}
		//Update the response.
		setResponseBodyBytes(response, buffer);
	}
	
	/**
	 * Increase the size of the byte buffer.
	 * 
	 * @param buffer
	 * @return
	 */
	private ByteBuffer expandBuffer(ByteBuffer buffer){
		
		int newLimit = buffer.position() + BUFFER_INCREMENT;
		
		buffer.limit(buffer.position());
		buffer.rewind();
		
		ByteBuffer expandedBuffer = ByteBuffer.allocate(newLimit);
		expandedBuffer.put(buffer);
		return expandedBuffer;
	}
	
	private void setResponseHeaderBytes(Response response, ByteBuffer buffer) throws RstpProtocolException{
		
		buffer.limit(buffer.position());
		buffer.rewind();
		response.setHeaderBytes(buffer);
	}

	private void setResponseBodyBytes(Response response, ByteBuffer buffer) throws RstpProtocolException{
		
		buffer.limit(buffer.position());
		buffer.rewind();
		response.setBodyBytes(buffer);
	}
	
	/**
	 * Used to keep a record of the last four bytes so that we can check
	 * if we have reached the end of the input.
	 * 
	 * @param lastBytes
	 * @param currentByte
	 */
	private void updateLastBytes(byte[] lastBytes, byte currentByte){
		
		for(int i = 0; i < HEADER_TERMINATOR.length - 1; i++){
			lastBytes[i] = lastBytes[i + 1];
		}
		lastBytes[HEADER_TERMINATOR.length - 1] = currentByte;
	}
	
	private void sendRequest(Socket sock, Request request) throws IOException{
		
		BufferedOutputStream out = new BufferedOutputStream(sock.getOutputStream());
		out.write(request.getBytes());
		out.flush();
	}
	
	private Socket openSocket(String host, int port) throws UnknownHostException, IOException{
		
		Socket sock = new Socket(host, port);
		sock.setSoTimeout(SOCKET_TIMEOUT);

		return sock;
	}
	
	/*
	 * Close the socket.
	 */
	private void closeSocket() throws IOException{
		
		sock.close();
	}	
}
