/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.body;

/**
 * <p>MIME content type Definitions.</p>
 * 
 * <h2>RFC2327 - SDP: Session Description Protocol</h2>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public enum BodyKey {

	// MIME content type.
	SDP("application/sdp", "Session Description Protocol");

	private final String key;
	private final String type;

	private BodyKey(String key, String type) {
		this.key = key;
		this.type = type;
	}
	
	public String key() {
		return key;
	}
	
	public String type(){
		return type;
	}
	
	@Override
	public String toString() {
		return "HeaderKey ["
				+ ("enum=" + this.name() + ", ")
				+ ("key=" + key + ", ")
				+ ("type=" + type)
				+ "]";
	}
}
