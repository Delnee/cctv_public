/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.state;

/**
 * <p>See RFC2326 Real Time Streaming Protocol (RTSP) Appendix A.1 Client State
 * Machine <a href="http://www.ietf.org/rfc/rfc2326.txt">RTSP</a>.</p>
 * 
 * <p>Extracts:-</p>
 * 
 * <p>In general, the client changes state on receipt of replies to
 * requests. State is defined on a per object basis. An object is uniquely
 * identified by the stream URL and the RTSP session identifier.</p>
 * 
 * <p>The requests OPTIONS, ANNOUNCE, DESCRIBE, GET_PARAMETER,
 * SET_PARAMETER do not have any effect on client or server state and
 * are therefore not listed in the state tables.</p>
 * 
 * <p>The client can assume the following states:-</p>
 * 
 * <ul>
 * <li>Init - SETUP has been sent, waiting for reply.</li>
 * <li>Ready - SETUP reply received or PAUSE reply received while in Playing
 * state.</li>
 * <li>Playing - PLAY reply received.</li>
 * <li>Recording - RECORD reply received</li>
 * </ul>
 * 
 * <p>In general, the client changes state on receipt of replies to
 * requests. The "next state" column indicates the state assumed after receiving
 * a success response (2xx). If a request yields a status code of 3xx, the
 * state becomes Init, and a status code of 4xx yields no change in
 * state. Messages not listed for each state MUST NOT be issued by the
 * client in that state, with the exception of messages not affecting
 * state, as listed above.</p>
 * 
 * <pre>
 * state        message     next state after response
 * Init         SETUP       Ready
 *              TEARDOWN    Init
 * Ready        PLAY        Playing
 *              RECORD      Recording
 *              TEARDOWN    Init
 *              SETUP       Ready
 * Playing      PAUSE       Ready
 *              TEARDOWN    Init
 *              PLAY        Playing
 *              SETUP       Playing (changed transport)
 * Recording    PAUSE       Ready
 *              TEARDOWN    Init
 *              RECORD      Recording
 *              SETUP       Recording (changed transport)
 * </pre>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public abstract class State {
	// The following methods do not change state...
	
	/**
	 * <p>See RFC2326 Real Time Streaming Protocol (RTSP) Section 10 Method
	 * Definitions <a href="http://www.ietf.org/rfc/rfc2326.txt">RTSP</a>.</p>
	 * 
	 * 10.3 ANNOUNCE
	 * 
	 * <p>The ANNOUNCE method serves two purposes:- When sent from client to
	 * server, ANNOUNCE posts the description of a presentation or media object
	 * identified by the request URL to a server. When sent from server to
	 * client, ANNOUNCE updates the session description in real-time.</p>
	 */	
	public void announce(){};
	
	/**
	 * <p>
	 * See RFC2326 Real Time Streaming Protocol (RTSP) Section 10 Method
	 * Definitions <a href="http://www.ietf.org/rfc/rfc2326.txt">RTSP</a>.
	 * </p>
	 * 
	 * 10.2 DESCRIBE
	 * 
	 * <p>
	 * The DESCRIBE method retrieves the description of a presentation or media
	 * object identified by the request URL from a server. It may use the Accept
	 * header to specify the description formats that the client understands.
	 * The server responds with a description of the requested resource.
	 * </p>
	 * 
	 * <pre>
	 * {@literal
	 * C->S: DESCRIBE rtsp://server.example.com/fizzle/foo RTSP/1.0
	 * CSeq: 312
	 * Accept: application/sdp, application/rtsl, application/mheg
	 * 
	 * S->C: RTSP/1.0 200 OK
	 * CSeq: 312
	 * Date: 23 Jan 1997 15:35:06 GMT
	 * Content-Type: application/sdp
	 * Content-Length: 376
	 * v=0
	 * o=mhandley 2890844526 2890842807 IN IP4 126.16.64.4
	 * s=SDP Seminar
	 * i=A Seminar on the session description protocol
	 * u=http://www.cs.ucl.ac.uk/staff/M.Handley/sdp.03.ps
	 * e=mjh@isi.edu (Mark Handley)
	 * c=IN IP4 224.2.17.12/127
	 * t=2873397496 2873404696
	 * a=recvonly
	 * m=audio 3456 RTP/AVP 0
	 * m=video 2232 RTP/AVP 31
	 * m=whiteboard 32416 UDP WB
	 * a=orient:portrait
	 * }
	 * </pre>
	 * 
	 */		
	public void describe(){};
	
	/**
	 * <p>
	 * See RFC2326 Real Time Streaming Protocol (RTSP) Section 10 Method
	 * Definitions <a href="http://www.ietf.org/rfc/rfc2326.txt">RTSP</a>.
	 * </p>
	 * 
	 * 10.1 OPTIONS
	 * 
	 * <p>
	 * An OPTIONS request may be issued at any time, e.g., if the client is
	 * about to try a nonstandard request. It does not influence server state.
	 * </p>
	 * 
	 * <pre>
	 * {@literal
	 * C->S: OPTIONS * RTSP/1.0
	 * CSeq: 1
	 * Require: implicit-play
	 * Proxy-Require: gzipped-messages
	 * 
	 * S->C: RTSP/1.0 200 OK
	 * CSeq: 1
	 * Public: DESCRIBE, SETUP, TEARDOWN, PLAY, PAUSE
	 * }
	 * </pre>
	 */
	public void options(){};
	
	/**
	 * <p>
	 * See RFC2326 Real Time Streaming Protocol (RTSP) Section 10 Method
	 * Definitions <a href="http://www.ietf.org/rfc/rfc2326.txt">RTSP</a>.
	 * </p>
	 * 
	 * 10.8 GET_PARAMETER
	 * 
	 * <p>
	 * The GET_PARAMETER request retrieves the value of a parameter of a
	 * presentation or stream specified in the URI. The content of the reply and
	 * response is left to the implementation. GET_PARAMETER with no entity body
	 * may be used to test client or server liveness ("ping").
	 * </p>
	 * 
	 * <pre>
	 * {@literal
	 * S->C: GET_PARAMETER rtsp://example.com/fizzle/foo RTSP/1.0
	 * CSeq: 431
	 * Content-Type: text/parameters
	 * Session: 12345678
	 * Content-Length: 15
	 * packets_received
	 * jitter
	 * 
	 * C->S: RTSP/1.0 200 OK
	 * CSeq: 431
	 * Content-Length: 46
	 * Content-Type: text/parameters
	 * packets_received: 10
	 * jitter: 0.3838
	 * }
	 * </pre>
	 * 
	 * <p>
	 * The "text/parameters" section is only an example type for parameter.
	 * This method is intentionally loosely defined with the intention that the
	 * reply content and response content will be defined after further
	 * experimentation.
	 * </p>
	 * @param key - the key of the parameter to get.
	 */	
	public void getParameter(String key){};
	
	/**
	 * <p>
	 * See RFC2326 Real Time Streaming Protocol (RTSP) Section 10 Method
	 * Definitions <a href="http://www.ietf.org/rfc/rfc2326.txt">RTSP</a>.
	 * </p>
	 * 
	 * 10.9 SET_PARAMETER
	 * 
	 * <p>
	 * This method requests to set the value of a parameter for a presentation
	 * or stream specified by the URI. A request SHOULD only contain a single
	 * parameter to allow the client to determine why a particular request
	 * failed.
	 * </p>
	 * @param key - the key of the parameter to set.
	 * @param value - the value of the parameter to set.
	 */		
	public void setParameter(String key, String value){};
	
	// The following methods can change state...
	
	/**
	 * <p>See RFC2326 Real Time Streaming Protocol (RTSP) Section 10 Method
	 * Definitions <a href="http://www.ietf.org/rfc/rfc2326.txt">RTSP</a>.</p>
	 * 
	 * 10.4 SETUP
	 * 
	 * <p>The SETUP request for a URI specifies the transport mechanism to be
	 * used for the streamed media. The Transport header specifies the transport
	 * parameters acceptable to the client for data transmission; the response
	 * will contain the transport parameters selected by the server.</p>
	 * 
	 * <pre>
	 * {@literal
	 * C->S: SETUP rtsp://example.com/foo/bar/baz.rm RTSP/1.0
	 * CSeq: 302
	 * Transport: RTP/AVP;unicast;client_port=4588-4589
	 * 
	 * S->C: RTSP/1.0 200 OK
	 * CSeq: 302
	 * Date: 23 Jan 1997 15:35:06 GMT
	 * Session: 47112344
	 * Transport: RTP/AVP;unicast;
	 * client_port=4588-4589;server_port=6256-6257
	 * }
	 * </pre>
	 * 
	 * <p>The server generates session identifiers in response to SETUP requests.</p>
	 */
	public abstract void setup();
	
	/**
	 * <p>
	 * See RFC2326 Real Time Streaming Protocol (RTSP) Section 10 Method
	 * Definitions <a href="http://www.ietf.org/rfc/rfc2326.txt">RTSP</a>.
	 * </p>
	 * 
	 * 10.5 PLAY
	 * 
	 * <p>
	 * The PLAY method tells the server to start sending data via the mechanism
	 * specified in SETUP. The PLAY request positions the normal play time to
	 * the beginning of the range specified and delivers stream data until the
	 * end of the range is reached. A PLAY request without a Range header is
	 * legal. For a on-demand stream, the server replies with the actual range
	 * that will be played back.
	 * </p>
	 * 
	 * <p>
	 * The live stream from a camera will have a range start of 0 with no end
	 * date.</p>
	 * 
	 * <pre>
	 * {@literal
	 * C->S: PLAY rtsp://192.168.1.15/h264/ch1/main/av_stream/ RTSP/1.0
	 * CSeq: 8
	 * Authorization: Digest username="admin", realm="4419b61f978b", nonce="103db0271a682142449252b1bf96e723", uri="rtsp://192.168.0.42/h264/ch1/main/av_stream/", response="6af31bc9af1dc8310fb00773d393f4e4"
	 * User-Agent: LibVLC/2.1.3 (LIVE555 Streaming Media v2014.01.21)
	 * Session: 1106259704
	 * Range: npt=0.000-
	 * }
	 * </pre>
	 */
	public abstract void play();
	
	/**
	 * <p>
	 * See RFC2326 Real Time Streaming Protocol (RTSP) Section 10 Method
	 * Definitions <a href="http://www.ietf.org/rfc/rfc2326.txt">RTSP</a>.
	 * </p>
	 * 
	 * 10.11 RECORD
	 * 
	 * <p>
	 * This method initiates recording a range of media data according to the
	 * presentation description. The timestamp reflects start and end time
	 * (UTC). If the session has already started, commence recording
	 * immediately. The server decides whether to store the recorded data under
	 * the request-URI or another URI. If the server does not use the request-
	 * URI, the response SHOULD be 201 (Created) and contain an entity which
	 * describes the status of the request and refers to the new resource,and a
	 * Location header. A media server supporting recording of live
	 * presentations MUST support the clock range format; the smpte format does
	 * not make sense.
	 * </p>
	 */
	public abstract void record();
	
	/**
	 * <p>
	 * See RFC2326 Real Time Streaming Protocol (RTSP) Section 10 Method
	 * Definitions <a href="http://www.ietf.org/rfc/rfc2326.txt">RTSP</a>.
	 * </p>
	 * 
	 * 10.6 PAUSE
	 * 
	 * <p>
	 * The PAUSE request causes the stream delivery to be interrupted (halted)
	 * temporarily. Any server resources are kept, though servers MAY close the
	 * session and free resources after being paused for the duration specified
	 * with the timeout parameter of the Session header in the SETUP message. A
	 * PAUSE request discards all queued PLAY requests. However, the pause point
	 * in the media stream MUST be maintained. A subsequent PLAY request without
	 * Range header resumes from the pause point.
	 * </p>
	 * 
	 * <pre>
	 * {@literal
	 * C->S: PAUSE rtsp://example.com/fizzle/foo RTSP/1.0
	 * CSeq: 834
	 * Session: 12345678
	 * 
	 * S->C: RTSP/1.0 200 OK
	 * CSeq: 834
	 * Date: 23 Jan 1997 15:35:06 GMT
	 * }
	 * </pre>
	 */
	public abstract void pause();
	
	/**
	 * <p>
	 * See RFC2326 Real Time Streaming Protocol (RTSP) Section 10 Method
	 * Definitions <a href="http://www.ietf.org/rfc/rfc2326.txt">RTSP</a>.
	 * </p>
	 * 
	 * 10.7 TEARDOWN
	 * 
	 * <p>
	 * The TEARDOWN request stops the stream delivery for the given URI,
	 * freeing the resources associated with it.
	 * </p>
	 */
	public abstract void teardown();
	
	@Override
	public String toString() {
		return "State []";
	}	
}
