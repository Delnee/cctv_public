/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.request;

import net.effanee.protocol.rtsp.client.Context;

/**
 * <p>Factory class with a single method used to create Request objects.</p>
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class RequestFactory {

	private RequestFactory(){
		// Should not be instantiated.
	}
	
	/**
	 * A static factory method used to create Requests.
	 * 
	 * @param method - the type of Request to create.
	 * @param context - the current state of the conversation.
	 * @return a Request.
	 */
	public static Request createRequest(RtspMethod method, Context context){
		
		Request request = null;
		
		switch (method) {
			case OPTIONS:
				request = new OptionsRequest(context);
				break;
			case DESCRIBE:
				request = new DescribeRequest(context);
				break;
			case SETUP:
				request = new SetupRequest(context);
				break;
			case TEARDOWN:
				request = new TeardownRequest(context);
				break;
			case PLAY:
				request = new PlayRequest(context);
				break;
			default:
				break;
		}
		
		return request;
	}
}