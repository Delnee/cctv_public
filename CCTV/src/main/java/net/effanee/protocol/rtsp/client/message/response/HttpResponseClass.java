/**
 * This file is part of CCTV.
 *
 * CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.effanee.protocol.rtsp.client.message.response;

/**
 * <p>RTSP 1.0 status code categories.</p>
 * 
 * <h2>RFC 2326 - Real Time Streaming Protocol 7.1.1</h2>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public enum HttpResponseClass {

	CODE_1(1, "Informational"),
	CODE_2(2, "Success"),
	CODE_3(3, "Redirection"),
	CODE_4(4, "Client Error"),
	CODE_5(5, "Server Error");

	private final int responseClass;
	private final String category;

	private HttpResponseClass(int statusCode, String reasonPhrase) {
		this.responseClass = statusCode;
		this.category = reasonPhrase;
	}
	
	public int statusCode() {
		return responseClass;
	}
	
	public String reasonPhrase(){
		return category;
	}
	
	@Override
	public String toString() {
		return "HttpResponseClass ["
				+ ("enum=" + this.name() + ", ")
				+ ("responseClass=" + responseClass + ", ")
				+ ("category=" + category)
				+ "]";
	}
}
