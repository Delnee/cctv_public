/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.header;

import net.effanee.protocol.rtsp.client.RstpProtocolException;
import net.effanee.protocol.rtsp.client.message.request.Request;

import org.apache.commons.lang3.StringUtils;

/**
 * <p>Concrete Header class.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class Session extends Header{
	
	private static final String  VALUE_SEPARATOR = ";";
	private static final int MIN_LENGTH = 7;
	private static final int MAX_LENGTH = 80;
	
	private String session;
	
	//A String of at least 8 characters.
	//Unique in 8 bytes - probably not.
	Session(String[] lines) throws RstpProtocolException {
		
		super(HeaderKey.SESSION, lines);
		session = readSession();
	}

	Session(Request request){
		
		super(HeaderKey.SESSION, request);
		session = request.getSessionValue();
	}
	
	private String readSession() throws RstpProtocolException{
		
		String[] tokens = StringUtils.split(value, VALUE_SEPARATOR);
		
		//ITS A STRING!
		String sess = "";
		sess = tokens[0];
		
		if(sess.length() < MIN_LENGTH | sess.length() > MAX_LENGTH){
			throw new RstpProtocolException("The RTSP response Header "
					+ HeaderKey.SESSION.key()
					+ " has value '"
					+ value
					+ " 'which cannot be parsed to a string."
					+ " Of minium length " + MIN_LENGTH
					+ " or maximum length " + MAX_LENGTH + "."
					);
		}

		return sess;
	}

	public String getSession(){
		
		return session;
	}
	
	public String getLine(){
		
		return HeaderKey.SESSION.key() + KEY_SEPARATOR
				+ " " + String.valueOf(session);
	}
	
	@Override
	public String toString() {
		return "Session ["
				+ ("session=" + session)
				+ ", "
				+ super.toString()
				+ "]";
	}
}
 