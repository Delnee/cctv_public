/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.header;

import net.effanee.protocol.rtsp.client.RstpProtocolException;

/**
 * <p>Concrete Header class.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class PlayResponseHeader extends ResponseHeader {

	private Sequence sequence;
	private Session session;
	private Range range;
	//private Date date;
	//private Transport transport;
	
	public PlayResponseHeader() {
	}

	public void setHeaders(String header) throws RstpProtocolException{
		
		super.setHeaders(header);
		setSequence(lines);
		setSesssion(lines);
		super.setWwwAuthenticate(lines);
		//setDate(lines);
		//setTransport(lines);
		setRange(lines);

	}

	//CSeq should always be present.
	private void setSequence(String[] lines) throws RstpProtocolException{

		sequence = (Sequence)HeaderFactory.readHeader(HeaderKey.CSEQ, lines);
	}

	//Session should always be present.
	private void setSesssion(String[] lines) throws RstpProtocolException{

		session = (Session)HeaderFactory.readHeader(HeaderKey.SESSION, lines);
	}

	private void setRange(String[] lines) throws RstpProtocolException{
		
		if(hasHeader(HeaderKey.RANGE)){
			range = (Range)HeaderFactory.readHeader(HeaderKey.RANGE, lines);
		}
	}
	
//	private void setDate(String[] lines) throws RstpProtocolException{
//		
//		if(hasHeader(HeaderKey.DATE)){
//			date = (Date)HeaderFactory.readHeader(HeaderKey.DATE, lines);
//		}
//	}
//
//	private void setTransport(String[] lines) throws RstpProtocolException{
//		
//		if(hasHeader(HeaderKey.TRANSPORT)){
//			transport = (Transport)HeaderFactory.readHeader(HeaderKey.TRANSPORT, lines);
//		}
//	}
	
	public Sequence getSequence() {
		return sequence;
	}
	
	public Session getSession(){
		return session;
	}

	public Range getRange() {
		return range;
	}
	
//	public Date getDate() {
//		return date;
//	}
//	
//	public Transport getTransport(){
//		
//		return transport;
//	}
	
	public ContentLength getContentLength(){
		
		return null;
	}
	
	@Override
	public String toString() {
		
		return "PlaydownResponseHeader ["
				+ ("sequence=" + sequence + ",")
				+ ("session=" + session)
				+ ("range=" + range)
				+ super.toString()
				+ "]";
	}
}
