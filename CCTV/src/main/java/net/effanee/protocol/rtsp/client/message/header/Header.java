/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.header;

import net.effanee.protocol.rtsp.client.RstpProtocolException;
import net.effanee.protocol.rtsp.client.message.request.Request;
import net.effanee.protocol.rtsp.client.message.response.Response;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * <p>Abstract Request class. Used to provide common behaviour.</p>
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public abstract class Header{
	
	private final HeaderKey key;
	static final String KEY_SEPARATOR = ":";
	private static final int EXPECTED_TOKENS = 2;
	
	protected final String value;
	
	//Only called by subclasses.
	Header(HeaderKey key, String[] lines) throws RstpProtocolException {
		
		this.key = key;
		value = getValue(lines);
	}

	public Header(HeaderKey key, Response response) {
		
		this.key = key;
		value = null;
	}

	public Header(HeaderKey key, Request request) {
		
		this.key = key;
		value = null;
	}
	
	//If a header key occurs multiple times, unless otherwise stated, a random line is selected from the occurrences.
	private String getValue(String[] lines) throws RstpProtocolException{
		
		for(String line: lines){
			String[] tokens = getTokens(line);
			if(tokens[0].trim().equalsIgnoreCase(key.key())){
				return tokens[1].trim();
			}
		}
		throw new RstpProtocolException("An RTSP response header line could not be parsed."
				+ " The key is - " + key);
	}
	
	private String[] getTokens(String line) throws RstpProtocolException{
		
		String[] tokens = StringUtils.split(line, KEY_SEPARATOR);
		if(tokens.length != EXPECTED_TOKENS){
			if(tokens.length > EXPECTED_TOKENS){
				tokens = joinValue(tokens);
			}
			else{
				throw new RstpProtocolException("An RTSP response header line could not be parsed."
						+ " The line is - " + line);
			}
		}
		return tokens;
	}

	//reassemble the value into one string
	private String[] joinValue(String[] tokens){
		
		String[] newTokens = new String[2];
		newTokens[0] = tokens[0];
		tokens = ArrayUtils.remove(tokens, 0);
		newTokens[1] = StringUtils.join(tokens, ":");
		return newTokens;
	}

	@Override
	public String toString() {
		return "Header ["
				+ ("key=" + key + ", ")
				+ ("value=" + value)
				+ "]";
	}
}
