/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.header;

import net.effanee.protocol.rtsp.client.RstpProtocolException;

/**
 * <p>Concrete Header class.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class ContentBase extends Header{
	
	//Content-Base: rtsp://192.168.1.15:554/h264/ch1/main/av_stream/
	//TODO this should be cast as a URL
	private final String content;
	
	ContentBase(String[] lines) throws RstpProtocolException {
		
		super(HeaderKey.CONTENT_BASE, lines);
		content = content();
	}

	private String content() throws RstpProtocolException{
		return value;
	}
	
	public String getContent() {
		return content;
	}

	@Override
	public String toString() {
		return "ContentBase ["
				+ ("content=" + content)
				+ ", "
				+ super.toString()
				+ "]";
	}
}
