/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.state;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>Ready state.</p>
 * 
 * <p>Valid transitions to:-</p>
 * <ul>
 * 	<li>Playng		via		play()</li>
 * 	<li>Recording	via		record()</li>
 * 	<li>Init		via		teardown()</li>
 * 	<li>Ready		via		setup()</li>
 * </ul>
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class ReadyState extends State {
	
	private static final Logger LOGGER = Logger.getLogger(ReadyState.class.getName());
	
	private final StateMachine machine;

	public ReadyState(StateMachine machine) {
		this.machine = machine;
	}

	@Override
	public void setup() {
		LOGGER.log(Level.FINEST, "State changed from Ready to Ready.");
		machine.setStateToReady();
	}

	@Override
	public void play() {
		LOGGER.log(Level.FINEST, "State changed from Ready to Playing.");
		machine.setStateToPlaying();
	}

	@Override
	public void record() {
		LOGGER.log(Level.FINEST, "State changed from Ready to Recording.");
		machine.setStateToRecording();
	}

	@Override
	public void pause() {
		throw new IllegalStateException("Cannot change state from Ready with pause() message.");
	}

	@Override
	public void teardown() {
		LOGGER.log(Level.FINEST, "State changed from Ready to Init.");
		machine.setStateToInit();
	}
	
	@Override
	public String toString() {
		return "ReadyState ["
				+ super.toString()
				+ "]";
	}
}
