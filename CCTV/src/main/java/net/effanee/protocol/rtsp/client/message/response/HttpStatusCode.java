/**
 * This file is part of CCTV.
 *
 * CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.effanee.protocol.rtsp.client.message.response;

/**
 * <p>RTSP 1.0 header field keys.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public enum HttpStatusCode {

	CODE_100(100, "Continue", HttpResponseClass.CODE_1),
	CODE_101(101, "Switching Protocols", HttpResponseClass.CODE_1),
	CODE_200(200, "OK", HttpResponseClass.CODE_2),
	CODE_201(201, "Created", HttpResponseClass.CODE_2),
	CODE_202(202, "Accepted", HttpResponseClass.CODE_2),
	CODE_203(203, "Non-Authoritative Information", HttpResponseClass.CODE_2),
	CODE_204(204, "No Content", HttpResponseClass.CODE_2),
	CODE_205(205, "Reset Content", HttpResponseClass.CODE_2),
	CODE_206(206, "Partial Content", HttpResponseClass.CODE_2),
	CODE_300(300, "Multiple Choices", HttpResponseClass.CODE_3),
	CODE_301(301, "Moved Permanently", HttpResponseClass.CODE_3),
	CODE_302(302, "Moved Temporarily", HttpResponseClass.CODE_3),
	CODE_303(303, "See Other", HttpResponseClass.CODE_3),
	CODE_304(304, "Not Modified", HttpResponseClass.CODE_3),
	CODE_305(305, "Use Proxy", HttpResponseClass.CODE_3),
	CODE_400(400, "Bad Request", HttpResponseClass.CODE_4),
	CODE_401(401, "Unauthorized", HttpResponseClass.CODE_4),
	CODE_402(402, "Payment Required", HttpResponseClass.CODE_4),
	CODE_403(403, "Forbidden", HttpResponseClass.CODE_4),
	CODE_404(404, "Not Found", HttpResponseClass.CODE_4),
	CODE_405(405, "Method Not Allowed", HttpResponseClass.CODE_4),
	CODE_406(406, "Not Acceptable", HttpResponseClass.CODE_4),
	CODE_407(407, "Proxy Authentication Required", HttpResponseClass.CODE_4),
	CODE_408(408, "Request Time-out", HttpResponseClass.CODE_4),
	CODE_409(409, "Conflict", HttpResponseClass.CODE_4),
	CODE_410(410, "Gone", HttpResponseClass.CODE_4),
	CODE_411(411, "Length Required", HttpResponseClass.CODE_4),
	CODE_412(412, "Precondition Failed", HttpResponseClass.CODE_4),
	CODE_413(413, "Request Entity Too Large", HttpResponseClass.CODE_4),
	CODE_414(414, "Request-URI Too Large", HttpResponseClass.CODE_4),
	CODE_415(415, "Unsupported Media Type", HttpResponseClass.CODE_4),
	CODE_500(500, "Internal Server Error", HttpResponseClass.CODE_5),
	CODE_501(501, "Not Implemented", HttpResponseClass.CODE_5),
	CODE_502(502, "Bad Gateway", HttpResponseClass.CODE_5),
	CODE_503(503, "Service Unavailable", HttpResponseClass.CODE_5),
	CODE_504(504, "Gateway Time-out", HttpResponseClass.CODE_5),
	CODE_505(505, "HTTP Version Not Supported", HttpResponseClass.CODE_5);

	private final int statusCode;
	private final String reasonPhrase;
	private final HttpResponseClass responseClass;

	private HttpStatusCode(int statusCode, String reasonPhrase, HttpResponseClass responseClass) {
		this.statusCode = statusCode;
		this.reasonPhrase = reasonPhrase;
		this.responseClass = responseClass;
	}
	
	public int statusCode() {
		return statusCode;
	}
	
	public String reasonPhrase(){
		return reasonPhrase;
	}
	
	public  HttpResponseClass getResponseClass(){
		return  responseClass;
	}
	
	@Override
	public String toString() {
		return "HttpStatusCode ["
				+ ("enum=" + this.name() + ", ")
				+ ("statusCode=" + statusCode + ", ")
				+ ("reasonPhrase=" + reasonPhrase)
				+ ("responseClass=" + responseClass)
				+ "]";
	}
}
