/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.body;

import net.effanee.protocol.rtsp.client.RstpProtocolException;

/**
 * <p>Concrete Response Body class.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class SdpResponseBody extends ResponseBody {

	private static final BodyKey bodyType = BodyKey.SDP;
	
	private Sdp sdp;
	
	public SdpResponseBody() {
	}

	public void setBody(String body) throws RstpProtocolException{
		
		super.setBody(body);
		setSdp(lines);
	}

	private void setSdp(String[] lines) throws RstpProtocolException{

		sdp = (Sdp)BodyFactory.readBody(BodyKey.SDP, lines);
	}

	public Sdp getSdp() {
		return sdp;
	}

	@Override
	public BodyKey getBodyType() {
		return bodyType;
	}
	
	@Override
	public String toString() {
		return "SdpResponseBody ["
				+ ("sdp=" + sdp + ",")
				+ super.toString()
				+ "]";
	}

}
