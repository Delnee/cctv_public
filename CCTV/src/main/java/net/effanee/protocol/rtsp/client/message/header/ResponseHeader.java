/**
 * This file is part of CCTV.
 *
 * CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.effanee.protocol.rtsp.client.message.header;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.effanee.protocol.rtsp.client.RstpProtocolException;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * <p>Abstract Response class. Used to provide common behaviour.</p>
 * 
 * <p>Parses a RTSP response header, and provides methods to return the contents
 * of the response header.</p>
 * 
 * <h2>RFC2326 - Real Time Streaming Protocol</h2>
 * 
 * <h3>Appendix D: Minimal RTSP implementation - D.1 Client</h3>
 * 
 * <p>Parse and understand the following headers in responses: CSeq,
 * Connection, Session, Transport, Content-Language, Content-
 * Encoding, Content-Length, Content-Type. RTP-compliant
 * implementations should also implement RTP-Info.
 *</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public abstract class ResponseHeader {
	
	private static final String EOL = "\r\n";
	private static final int MIN_HEADER_LINES = 3;
	static final String KEY_SEPARATOR = ":";
	
	String[] lines;
	Set<HeaderKey> returnedHeaders = new HashSet<HeaderKey>();
	private StatusLine statusLine;
	private List<WwwAuthenticate> wwwAuthenticates = new ArrayList<WwwAuthenticate>();
	
	public void setHeaders(String header) throws RstpProtocolException{
		
		lines = getLines(header);
		storeHeaderKeys();
		setStatusLine(lines);
		lines = ArrayUtils.remove(lines, 0);
	}

	public abstract Sequence getSequence();
	
	String[] getLines(String header) throws RstpProtocolException{
		
		if(header == null || header.length() < 1){
			throw new RstpProtocolException("The RTSP response header could not be parsed."
					+ " The token is - " + header);
		}
		
		String[] lines = header.split(EOL);
		if(lines.length < MIN_HEADER_LINES){
			throw new RstpProtocolException("The RTSP response header could not be parsed."
					+ " The token is - " + header);
		}
		return lines;
	}
	
	//So that we can test whether a header exists before instantiating it....
	private void storeHeaderKeys(){
		
		for(String line: lines){
			
			String[] tokens = StringUtils.split(line, KEY_SEPARATOR);
			if(tokens.length > 0){
				
				for(HeaderKey headerKey: HeaderKey.values()){
					if (tokens[0].trim().equalsIgnoreCase(headerKey.key())) {
						returnedHeaders.add(headerKey);
					}					
				}
			}
		}
	}
	
	boolean hasHeader(HeaderKey headerKey){
		
		return returnedHeaders.contains(headerKey);
	}
	
	private void setStatusLine(String[] lines) throws RstpProtocolException{
		
		statusLine = new StatusLine(lines[0]);
	}
	
	public StatusLine getStatusLine(){
		
		return statusLine;
	}

	void setWwwAuthenticate(String[] lines) throws RstpProtocolException{
		
		if(hasHeader(HeaderKey.WWW_AUTHENTICATE)){
			
			List<String> authLines = getMultipleLines(HeaderKey.WWW_AUTHENTICATE);
			for(String authLine: authLines){
				wwwAuthenticates.add((WwwAuthenticate)HeaderFactory.readHeader(HeaderKey.WWW_AUTHENTICATE, new String[] {authLine}));	
			}
		}
	}
	
	public List<WwwAuthenticate> getWwwAuthenticates(){
		
		return wwwAuthenticates;
	}
	
	private List<String> getMultipleLines(HeaderKey headerKey){
		
		List<String> authLines = new ArrayList<String>();
		for (String line : lines) {
			String[] tokens = StringUtils.split(line, KEY_SEPARATOR);
			if (tokens[0].trim().equalsIgnoreCase(headerKey.key())) {
				authLines.add(line);
			}
		}
		return authLines;
	}
	
	@Override
	public String toString() {
		return "ResponseHeader ["
				+ ("statusLine=" + statusLine + ", ")
				+ ("lines=" + lines)
				+ "]";
	}
}
