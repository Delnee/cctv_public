/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.header;

import net.effanee.protocol.rtsp.client.RstpProtocolException;
import net.effanee.protocol.rtsp.client.message.response.HttpStatusCode;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * <p>Concrete Header class.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class StatusLine{
	
	private static final String SEPARATOR = " ";
	private static final int EXPECTED_TOKENS = 3;
	private static final String RTSP_VERSION = "RTSP/1.0";
	
	private final String rtspVersion;
	private final String statusCode;
	private final String reasonPhrase;
	
	public StatusLine(String line) throws RstpProtocolException {
		
		String[] tokens = getTokens(line);
		rtspVersion = getRtspVersion(tokens);
		statusCode = getStatusCode(tokens);
		reasonPhrase = getReasonPhrase(tokens);
	}
	
	//Status-Line = RTSP-Version SP Status-Code SP Reason-Phrase CRLF
	private String getRtspVersion(String[] tokens) throws RstpProtocolException{
		
		String rtspVersion = tokens[0];
		if(!rtspVersion.equals(RTSP_VERSION)){
			throw new RstpProtocolException("The RTSP response Status Line RTSP-Version could not be parsed."
					+ " The token is - " + rtspVersion);
		}
		return rtspVersion;
	}
	
	//Status-Line = RTSP-Version SP Status-Code SP Reason-Phrase CRLF
	private String getStatusCode(String[] tokens) throws RstpProtocolException{
		
		tokens = ArrayUtils.remove(tokens, 0);
		String statusCode = tokens[0];
		if(!isValidStatusCode(statusCode)){
			throw new RstpProtocolException("The RTSP response Status Line Status-Code could not be parsed."
					+ " The token is - " + statusCode);
		}
		return statusCode;
	}
	
	//Status-Line = RTSP-Version SP Status-Code SP Reason-Phrase CRLF
	private String getReasonPhrase(String[] tokens){
		
		tokens = ArrayUtils.removeAll(tokens, 0, 1);
		String reasonPhrase = tokens[0];
		
		return reasonPhrase;
	}		
	
	private String[] getTokens(String line) throws RstpProtocolException{
		
		String[] tokens = StringUtils.split(line, SEPARATOR);
		if(tokens.length < EXPECTED_TOKENS){
			throw new RstpProtocolException("The RTSP response Status Line could not be parsed."
					+ " The line is - " + line);
		}
		return tokens;
	}
	
	private boolean isValidStatusCode(String token){
		
		for(HttpStatusCode code: HttpStatusCode.values()){
			if((Integer.toString(code.statusCode())).equals(token)){
				return true;
			}
		}
		return false;
	}
	
	public HttpStatusCode getStatusCode(){
		
		HttpStatusCode code = null;
		for(HttpStatusCode sCode: HttpStatusCode.values()){
			if((Integer.toString(sCode.statusCode())).equals(statusCode)){
				code = sCode;
				break;
			}
		}
		return code;
	}
	@Override
	public String toString() {
		return "StatusLine ["
				+ ("rtspVersion=" + rtspVersion + ", ")
				+ ("statusCode=" + statusCode + ", ")
				+ ("reasonPhrase=" + reasonPhrase)
				+ "]";
	}
}
