/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.state;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>Init state.</p>
 * 
 * <p>Valid transitions to:-</p>
 * <ul>
 * 	<li>Ready	via		setup()</li>
 * 	<li>Init	via		teardown()</li>
 * </ul>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class InitState extends State {

	private static final Logger LOGGER = Logger.getLogger(InitState.class.getName());
	
	private final StateMachine machine;
	
	public InitState(StateMachine machine) {
		this.machine = machine;
	}

	@Override
	public void setup() {
		LOGGER.log(Level.FINEST, "State changed from Init to Ready.");
		machine.setStateToReady();
	}

	@Override
	public void play() {
		throw new IllegalStateException("Cannot change state from Init with play() message.");
	}

	@Override
	public void record() {
		throw new IllegalStateException("Cannot change state from Init with record() message.");

	}

	@Override
	public void pause() {
		throw new IllegalStateException("Cannot change state from Init with pause message.");
	}

	@Override
	public void teardown() {
		LOGGER.log(Level.FINEST, "State changed from Init to Init.");
		machine.setStateToInit();
	}
	
	@Override
	public String toString() {
		return "InitState ["
				+ super.toString()
				+ "]";
	}
}
