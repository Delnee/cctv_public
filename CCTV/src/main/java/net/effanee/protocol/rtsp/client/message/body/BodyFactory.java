/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.body;

import net.effanee.protocol.rtsp.client.RstpProtocolException;

/**
 * <p>Factory class with methods used to read Body objects.</p>
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class BodyFactory {

	private BodyFactory() {
		// Should not be instantiated.
	}

	/**
	 * A static factory method used to read Bodies.
	 * 
	 * @param key - the type of Body to read.
	 * @param lines - the header lines, one of which should contain the
	 * specified header.
	 * @return a Body.
	 * @throws RstpProtocolException
	 */
	public static Body readBody(BodyKey key, String[] lines) throws RstpProtocolException {

		Body body = null;
		
		switch (key) {
			case SDP:
				body = new Sdp(lines);
				break;
			default:
				break;
		}
		
		return body;
	}
}
