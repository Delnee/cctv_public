/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.header;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import net.effanee.protocol.rtsp.client.RstpProtocolException;

/**
 * <p>Concrete Header class.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class Date extends Header{
	
	private final java.util.Date headerDate;
	private static final String RFC1123_DATE_FORMAT_HIKVISION = "E, MMM dd yyyy HH:mm:ss z";
	private static final String RFC1123_DATE_FORMAT = "E, dd MMM yyyy HH:mm:ss z";
	
	Date(String[] lines) throws RstpProtocolException {
		
		super(HeaderKey.DATE, lines);
		headerDate = date();
	}

	//RFC 1123 [8]-date format.
	//Date: Tue, 15 Nov 1994 08:12:31 GMT
	private java.util.Date date() throws RstpProtocolException{
		
		java.util.Date thisDate = null;
		try{
			//Mon, Aug 11 2014 15:01:09 GMT
			//Thu, 10 Apr 2008 13:30:00 GMT    
			//Sun, 10 Aug 2014 18:02:28 UTC
			SimpleDateFormat formatter = new SimpleDateFormat(RFC1123_DATE_FORMAT);
			thisDate = formatter.parse(value);
		}
		catch(ParseException e1){
			try{
				//Mon, Aug 11 2014 15:01:09 GMT
				//Thu, 10 Apr 2008 13:30:00 GMT    
				//Sun, 10 Aug 2014 18:02:28 UTC
				SimpleDateFormat formatter = new SimpleDateFormat(RFC1123_DATE_FORMAT_HIKVISION);
				thisDate = formatter.parse(value);
			}
			catch(ParseException e2){
				throw new RstpProtocolException("The RTSP response Header "
						+ HeaderKey.DATE.key()
						+ " has value "
						+ value
						+ " which cannot be parsed to a date.");
			}
		}
		return thisDate;
	}
	
	public java.util.Date getDate() {
		return headerDate;
	}
	
	@Override
	public String toString() {
		return "Date ["
				+ ("date=" + headerDate)
				+ ", "
				+ super.toString()
				+ "]";
	}
}
