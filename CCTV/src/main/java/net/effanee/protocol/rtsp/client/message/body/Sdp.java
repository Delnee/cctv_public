/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.body;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import net.effanee.protocol.rtsp.client.RstpProtocolException;

/**
 * <h2>RFC2327 - SDP: Session Description Protocol</h2>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class Sdp extends Body{
	
	public enum SessionKey {

		VERSION("v", "Version"),
		ORIGIN("o", "Origin"),
		SESSION("s", "Session"),
		INFORMATION("i", "Information"),
		URI("u", "URI"),
		EMAIL("e", "Email"),
		PHONE("p", "Phone"),
		CONNECTION("c", "Connection"),
		BANDWIDTH("b", "Bandwidth"),
		ACTIVE_TIME("t", "Active Time"),
		REPEAT_TIME("r", "Repeat Time"),
		TIMEZONE("z", "Timezone"),
		ENCRYPTION_KEY("k", "Encryption Key"),
		ATTRIBUTE("a", "Attribute");

		private final String key;
		private final String description;

		private SessionKey(String key, String description) {
			this.key = key;
			this.description = description;
		}
		
		public String key() {
			return key;
		}

		public String description() {
			return description;
		}
		
		@Override
		public String toString() {
			return "SessionKey ["
					+ ("enum=" + this.name() + ", ")
					+ ("key=" + key + ", ")
					+ ("description=" + description)
					+ "]";
		}
	}
	
	public enum MediaKey {

			NAME_ADDRESS("m", "Name and Address"),
			INFORMATION("i", "Information"),
			CONNECTION("c", "Connection"),
			BANDWIDTH("b", "Bandwidth"),
			ENCRYPTION_KEY("k", "Encryption Key"),
			ATTRIBUTE("a", "Attribute");
	
			private final String key;
			private final String description;
	
			private MediaKey(String key, String description) {
				this.key = key;
				this.description = description;
			}
			
			public String key() {
				return key;
			}
	
			public String description() {
				return description;
			}
				
		@Override
		public String toString() {
			return "SessionKey ["
					+ ("enum=" + this.name() + ", ")
					+ ("key=" + key + ", ")
					+ ("description=" + description)
					+ "]";
		}
	}
	
	private static final String SEPARATOR = "=";
	private static final String ATTRIBUTE_SEPARATOR = ":";
	private static final String ATTRIBUTE_FIELD_SEPARATOR = ";";
	private static final String ATTRIBUTE_KEY_VALUE_SEPARATOR = "=";
	private static final String MEDIA_SECTION_KEY = "m";
	
	private SessionDescription sessionDescription;
	private List<MediaSection> mediaSections = new ArrayList<MediaSection>();;
	
	public Sdp(String[] lines) throws RstpProtocolException {
		
		super(BodyKey.SDP, lines);
		parse(lines);
	}

	private void parse(String[] lines) throws RstpProtocolException{
		
		String[] sessionSection = getSectionLines(lines);
		this.sessionDescription = new SessionDescription(sessionSection);
		List<String[]> mediaSections = getMediaLines(lines);
		for(String[] section: mediaSections){
			MediaSection mediaSection = new MediaSection(section); 
			this.mediaSections.add(mediaSection);
		}
	}
	
	//broken into sections on the m key...
	private String[] getSectionLines(String[] lines){
		
		List<String> sectionLines = new ArrayList<String>();
		for(String line: lines){
			
			String[] tokens = getTokens(line);
			if(tokens[0].equalsIgnoreCase(MEDIA_SECTION_KEY)){
				break;
			}
			else{
				sectionLines.add(StringUtils.join(tokens, SEPARATOR));
			}
		}
		return sectionLines.toArray(new String[sectionLines.size()]);
	}
	
	private List<String[]> getMediaLines(String[] lines){
		
		List<String[]> mediaSections = new ArrayList<String[]>();
		
		for(int i = 0; i < lines.length; i++){
			
			String[] tokens = getTokens(lines[i]);
			if(tokens[0].equalsIgnoreCase(MEDIA_SECTION_KEY)){
				//Get the remaining lines from the media section.
				String[] remainingLines = ArrayUtils.subarray(lines, i + 1, lines.length);
				String[] sectionLines = getSectionLines(remainingLines);
				//Add the m line plus any following lines in the section.
				sectionLines = ArrayUtils.addAll(new String[] {lines[i]}, sectionLines);
				mediaSections.add(sectionLines);
			}
		}
		return mediaSections;
	}
	
	public SessionDescription getSessionDescription(){
	
		return sessionDescription;
	}

	public List<MediaSection> getMediaSections(){
	
		return mediaSections;
	}
	
	public String getVideoStreamUri(){
		
		String control = "";
		for(MediaSection section: mediaSections){
			if(section.isVideoStream()){
				control = section.getControl();
			}
		}
		return control;
	}
	
	private String[] getTokens(String line){
		
		String[] tokens = StringUtils.split(line, SEPARATOR);
		if(tokens.length > 1){
			
			tokens[1] = StringUtils.join(ArrayUtils.remove(tokens, 0), SEPARATOR);
		}
		//TODO tidy...
		String[] newTokens = {tokens[0], tokens[1]};
		return newTokens;
	}
	
	@Override
	public String toString() {
		return "Sdp ["
				+ ("sessionDescription=" + sessionDescription + ", ")
				+ ("mediaSections=" + mediaSections)
				+ "]";
	}
	
	/*

v=0
o=- 1407352941213957 1407352941213957 IN IP4 192.168.1.15
s=Media Presentation
e=NONE
b=AS:5050
t=0 0
a=control:rtsp://192.168.1.15:554/h264/ch1/main/av_stream/
m=video 0 RTP/AVP 96
b=AS:5000
a=control:rtsp://192.168.1.15:554/h264/ch1/main/av_stream/trackID=1
a=rtpmap:96 H264/90000
a=fmtp:96 profile-level-id=420029; packetization-mode=1; sprop-parameter-sets=Z00AKZpmA8ARPyzUBAQFAAADA+gAAMNQBA==,aO48gA==
a=Media_header:MEDIAINFO=494D4B48010100000400010000000000000000000000000000000000000000000000000000000000;
a=appversion:1.0


v=0
o=- 381783596 381783596 IN IP4 184.72.239.149
s=BigBuckBunny_175k.mov
c=IN IP4 184.72.239.149
t=0 0
a=sdplang:en
a=range:npt=0- 596.458
a=control:*
m=audio 0 RTP/AVP 96
a=rtpmap:96 mpeg4-generic/48000/2
a=fmtp:96 profile-level-id=1;mode=AAC-hbr;sizelength=13;indexlength=3;indexdeltalength=3;config=1190
a=control:trackID=1
m=video 0 RTP/AVP 97
a=rtpmap:97 H264/90000
a=fmtp:97 packetization-mode=1;profile-level-id=42C01E;sprop-parameter-sets=Z0LAHtkDxWhAAAADAEAAAAwDxYuS,aMuMsg==
a=cliprect:0,0,160,240
a=framesize:97 240-160
a=framerate:24.0
a=control:trackID=2

	 */
	
	//[v=0, o=- 1407779694328232 1407779694328232 IN IP4 192.168.1.15, s=Media Presentation, e=NONE, b=AS:5050, t=0 0, a=control:rtsp://192.168.1.15/h264/ch1/main/av_stream/]
	public class SessionDescription{

		
		//Once.
			//Required.
			private String version;
			private String origin;
			private String session;
			//Optional.
			private String information;
			private String uri;
			private String email;
			private String phone;
			private String connection;
			private String bandwidth;
		//One or more.
			//Required.
			private String activeTime;
			//Optional.
			private String repeatTime;
		//Once.
			//Optional.
			private String timeZone;
			private String encryptionKey;
		//Zero or more.
			//Optional.
			private Map<String, String> attributes =  new HashMap<String, String>();
		
		SessionDescription(String[] lines){
			
			parse(lines);
		}
	
		private void parse(String[] lines){
			
			setVersion(lines);
			setOrigin(lines);
			setSession(lines);
			if(lines.length > 3){
				for(int i = 3; i < lines.length; i++){
					setOptional(lines[i]);	
				}
			}
		}
		
		private void setVersion(String[] lines){
			
			if(checkKey(SessionKey.VERSION, lines[0])){
				this.version = getTokens(lines[0])[1];
			}
		}

		private void setOrigin(String[] lines){
			
			if(checkKey(SessionKey.ORIGIN, lines[1])){
				this.origin = getTokens(lines[1])[1];
			}
		}		

		private void setSession(String[] lines){
			
			if(checkKey(SessionKey.SESSION, lines[2])){
				this.session = getTokens(lines[2])[1];
			}
		}	
		
		private void setOptional(String line){
			
			String[] tokens = getTokens(line);
			String key = tokens[0];
			SessionKey sessionKey = null;
			for(SessionKey sKey: SessionKey.values()){
				if(sKey.key().equalsIgnoreCase(key)){
					sessionKey = sKey;
					break;
				}
			}
			
			switch (sessionKey) {
				case INFORMATION:
					this.information = tokens[1];
					break;
				case URI:
					this.uri = tokens[1];
					break;
				case EMAIL:
					this.email = tokens[1];
					break;
				case PHONE:
					this.phone = tokens[1];
					break;
				case CONNECTION:
					this.connection = tokens[1];
					break;
				case BANDWIDTH:
					this.bandwidth = tokens[1];
					break;
				case ACTIVE_TIME:
					this.activeTime = tokens[1];
					break;
				case REPEAT_TIME:
					this.repeatTime = tokens[1];
					break;
				case TIMEZONE:
					this.timeZone = tokens[1];
					break;	
				case ENCRYPTION_KEY:
					this.encryptionKey = tokens[1];
					break;
				case ATTRIBUTE:
					attributes.putAll(getAttribute(line));
					break;	
				default:
					break;
			}			
		}

		private Map<String, String> getAttribute(String line){
		
			String[] tokens = getTokens(line);
			String[] attributetokens = getAttributeTokens(tokens[1]);
			Map<String, String> keyValue = new HashMap<String, String>();
			if(attributetokens.length > 1){
				keyValue.put(attributetokens[0], attributetokens[1]);
			}
			return keyValue;
		}

		private boolean checkKey(SessionKey key, String line){
			
			String[] tokens = getTokens(line);
			if(tokens.length > 0){
				return tokens[0].equalsIgnoreCase(key.key());
			}
			return false;
		}
		
		private String[] getAttributeTokens(String line){
		
			String[] tokens = StringUtils.split(line, ATTRIBUTE_SEPARATOR);
			if(tokens.length > 1){
				tokens[1] = StringUtils.join(ArrayUtils.remove(tokens, 0), ATTRIBUTE_SEPARATOR);
			}
			//TODO tidy...
			String[] newTokens = {tokens[0], tokens[1]};
			return newTokens;
		}
	
		@Override
		public String toString() {
			return "SessionDescription ["
					+ ("version=" + version + ", ")
					+ ("origin=" + origin + ", ")
					+ ("session=" + session + ", ")
					+ ("information=" + information + ", ")
					+ ("uri=" + uri + ", ")
					+ ("email=" + email + ", ")
					+ ("phone=" + phone + ", ")
					+ ("connection=" + connection + ", ")
					+ ("bandwidth=" + bandwidth + ", ")
					+ ("activeTime=" + activeTime + ", ")
					+ ("repeatTime=" + repeatTime + ", ")
					+ ("timezone=" + timeZone + ", ")
					+ ("encryptionKey=" + encryptionKey + ", ")
					+ ("attributes=" + attributes)
					+ "]";
		}
	}

	public class MediaSection{
		
		//Once.
			//Required.
			private String nameAddress;
			//Optional.
			private String connection;
			private String bandwidth;
			private String encryptionKey;
		//Zero or more.
			//Optional.
			private Map<String, String> attributes =  new HashMap<String, String>();
		
		MediaSection(String[] lines){
			
			parse(lines);
		}
		
		private void parse(String[] lines){
			
			setNameAddress(lines);
			if(lines.length > 1){
				for(int i = 1; i < lines.length; i++){
					setOptional(lines[i]);	
				}
			}
		}

		private void setNameAddress(String[] lines){
			
			if(checkKey(MediaKey.NAME_ADDRESS, lines[0])){
				this.nameAddress = getTokens(lines[0])[1];
			}
		}
				
		private void setOptional(String line){
			
			String[] tokens = getTokens(line);
			String key = tokens[0];
			MediaKey mediaKey = null;
			for(MediaKey mKey: MediaKey.values()){
				if(mKey.key().equalsIgnoreCase(key)){
					mediaKey = mKey;
					break;
				}
			}
			
			switch (mediaKey) {
				case NAME_ADDRESS:
					this.nameAddress = tokens[1];
					break;
				case CONNECTION:
					this.connection = tokens[1];
					break;
				case BANDWIDTH:
					this.bandwidth = tokens[1];
					break;
				case ENCRYPTION_KEY:
					this.encryptionKey = tokens[1];
					break;
				case ATTRIBUTE:
					attributes.putAll(getAttribute(line));
					break;	
				default:
					break;
			}			
		}

		private Map<String, String> getAttribute(String line){
		
			String[] tokens = getTokens(line);
			String[] attributetokens = getAttributeTokens(tokens[1]);
			Map<String, String> keyValue = new HashMap<String, String>();
			if(attributetokens.length > 1){
				keyValue.put(attributetokens[0], attributetokens[1]);
			}
			return keyValue;
		}
		
		private boolean checkKey(MediaKey key, String line){
			
			String[] tokens = getTokens(line);
			if(tokens.length > 0){
				return tokens[0].equalsIgnoreCase(key.key());
			}
			return false;
		}
		
		private String[] getAttributeTokens(String line){
		
			String[] tokens = StringUtils.split(line, ATTRIBUTE_SEPARATOR);
			if(tokens.length > 1){
				tokens[1] = StringUtils.join(ArrayUtils.remove(tokens, 0), ATTRIBUTE_SEPARATOR);
			}
			//TODO tidy...
			String[] newTokens = {tokens[0], tokens[1]};
			return newTokens;
		}
		
		public boolean isVideoStream(){
			
			return nameAddress.startsWith("video");
		}
		
		public String getControl(){
			
			String control = null;
			for(String key: attributes.keySet()){
				if(key.equalsIgnoreCase("control")){
					control = attributes.get(key);
					break;
				}
			}
			return control;
		}

		/**
		 * Get the packetization mode, from the format parameters, in the media
		 * section. For the first media section entry.
		 * 
		 * @return the packetization mode, (one of 0, 1, or 2), or -1 if not
		 * found.
		 */
		public int getPacketizationMode(){
			
			final String PACKET_MODE_KEY = "packetization-mode";
			
			int mode = -1;
			String fmtp = getFmtp();
			
			// TODO No checking for safety.
			if (fmtp != null){
				
				// Remove format payload type at start of line, which is a
				// number, followed by a space.
				// from:- 96 profile-level-id=420029; packetization-mode=1; sprop-parameter-sets=Z00AKZpmA8ARPyzUBAQFAAADA+gAAMNQBA=,aO48gA
				// To:- profile-level-id=420029; packetization-mode=1; sprop-parameter-sets=Z00AKZpmA8ARPyzUBAQFAAADA+gAAMNQBA=,aO48gA
				fmtp = fmtp.substring(fmtp.indexOf(" "));
				
				// Split the string into a list of key/value pairs.
				// Divide the list into a map of key/value pairs.
				// from:- profile-level-id=420029; packetization-mode=1; sprop-parameter-sets=Z00AKZpmA8ARPyzUBAQFAAADA+gAAMNQBA=,aO48gA
				// To:- {profile-level-id=420029, packetization-mode=1, sprop-parameter-sets=Z00AKZpmA8ARPyzUBAQFAAADA+gAAMNQBA=,aO48gA}
				Map<String, String> keysValues =
				Arrays.stream(StringUtils.split(fmtp, ATTRIBUTE_FIELD_SEPARATOR))
						.map(element -> element.split(ATTRIBUTE_KEY_VALUE_SEPARATOR))
						.collect(Collectors.toMap(k -> k[0].toLowerCase().trim(), v -> v[1]));
				
				// Get the value as an integer.
				if(keysValues.containsKey(PACKET_MODE_KEY)){
					try{
						mode = Integer.parseInt(keysValues.get(PACKET_MODE_KEY));
					}
					catch(NumberFormatException e){
						// Ignore.				
					}
				}
			}
			return mode;
		}
		
		/**
		 * <p>Get the format parameters from the first entry in the media section.</p>
		 * 
		 * <h2>RFC 4566 - SDP: Session Description Protocol</h2>
		 * 
		 * <h3>6. SDP Attributes</h3>
		 * <code>
		 * a=fmtp:{@literal<format> <format specific parameters>}
		 * </code>
		 * This attribute allows parameters that are specific to a particular
		 * format to be conveyed in a way that SDP does not have to understand
		 * them. The format must be one of the formats specified for the media.
		 * Format-specific parameters may be any set of parameters required to
		 * be conveyed by SDP and given unchanged to the media tool that will
		 * use this format. At most one instance of this attribute is allowed
		 * for each format.
		 * 
		 * Example:-
		 * <code>
		 * fmtp=96 profile-level-id=420029; packetization-mode=1; sprop-parameter-sets=Z00AKZpmA8ARPyzUBAQFAAADA+gAAMNQBA=,aO48gA 
		 * </code>
		 * 
		 * @return the fmtp attribute, or an empty string.
		 */
		public String getFmtp(){
			
			String fmtp = "";
			for(String key: attributes.keySet()){
				if(key.equalsIgnoreCase("fmtp")){
					fmtp = attributes.get(key);
					break;
				}
			}
			return fmtp;
		}
		
		@Override
		public String toString() {
			return "MediaSection ["
					+ ("nameAddress=" + nameAddress + ", ")
					+ ("connection=" + connection + ", ")
					+ ("bandwidth=" + bandwidth + ", ")
					+ ("encryptionKey=" + encryptionKey + ", ")
					+ ("attributes=" + attributes)
					+ "]";
		}
	}
}
