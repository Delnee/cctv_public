/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.body;

import net.effanee.protocol.rtsp.client.RstpProtocolException;

/**
 * <p>Abstract Body class. Used to provide common behaviour.</p>
 * 
 * <p>Parses a RTSP response body, and provides methods to return the contents
 * of the response body.</p>
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public abstract class ResponseBody {
	
	private static final String EOL = "\r\n";
	
	String[] lines;
	
	public void setBody(String body) throws RstpProtocolException{
		
		lines = getLines(body);
	}

	public abstract BodyKey getBodyType();
	
	String[] getLines(String body) throws RstpProtocolException{
		
		if(body == null || body.length() < 1){
			throw new RstpProtocolException("The RTSP response body could not be parsed."
					+ " The token is - " + body);
		}
		
		String[] lines = body.split(EOL);

		return lines;
	}

	@Override
	public String toString() {
		return "ResponseBody ["
				+ ("lines=" + lines)
				+ "]";
	}
}
