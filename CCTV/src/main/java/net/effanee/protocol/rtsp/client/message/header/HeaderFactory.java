/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.header;

import net.effanee.protocol.rtsp.client.RstpProtocolException;
import net.effanee.protocol.rtsp.client.message.request.Request;

/**
 * <p>Factory class with a methods used to read and create Header objects.</p>
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class HeaderFactory {

	private HeaderFactory() {
		// Should not be instantiated.
	}

	/**
	 * A static factory method used to read Headers.
	 * 
	 * @param key - the type of Header to read.
	 * @param lines - the header lines, one of which should contain the
	 * specified header.
	 * @return a header.
	 * @throws RstpProtocolException
	 */
	public static Header readHeader(HeaderKey key, String[] lines) throws RstpProtocolException {

		Header header = null;
		
		switch (key) {
			case CSEQ:
				header = new Sequence(lines);
				break;
			case USER_AGENT:
				header = new UserAgent(lines);
				break;
			case DATE:
				header = new Date(lines);
				break;
			case PUBLIC:
				header = new Public(lines);
				break;
			case WWW_AUTHENTICATE:
				header = new WwwAuthenticate(lines);
				break;
			case CONTENT_LENGTH:
				header = new ContentLength(lines);
				break;
			case CONTENT_TYPE:
				header = new ContentType(lines);
				break;
			case CONTENT_BASE:
				header = new ContentBase(lines);
				break;
			case SESSION:
				header = new Session(lines);
				break;
			case TRANSPORT:
				header = new Transport(lines);
				break;
			case RANGE:
				header = new Range(lines);
				break;
			default:
				break;
		}
		
		return header;
		
	}

	/**
	 * A static factory method used to create Headers.
	 * 
	 * @param key - the type of Header to create.
	 * @param request - the request containing the details for the header.
	 * @return a header.
	 */
	public static Header createHeader(HeaderKey key, Request request) {

		Header header = null;
		
		switch (key) {
			case CSEQ:
				header = new Sequence(request);
				break;
			case USER_AGENT:
				header = new UserAgent(request);
				break;
			case ACCEPT:
				header = new Accept(request);
				break;
			case AUTHORIZATION:
				header = new Authorization(request);
				break;
			case TRANSPORT:
				header = new Transport(request);
				break;
			case SESSION:
				header = new Session(request);
				break;	
			case RANGE:
				header = new Range(request);
				break;	
			default:
				break;
		}
		
		return header;
	}
}
