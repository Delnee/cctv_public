/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.response;

import net.effanee.protocol.rtsp.client.message.request.Request;
import net.effanee.protocol.rtsp.client.message.request.RtspMethod;

/**
 * <p>Factory class with a single method used to create Response objects.</p>
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class ResponseFactory {

	private ResponseFactory() {
		// Should not be instantiated.
	}

	/**
	 * A static factory method used to create Responses.
	 * 
	 * @param method - the type of Response to create.
	 * @param request - the Request that will generate the Response.
	 * @return a Response.
	 */
	public static Response createResponse(RtspMethod method, Request request) {

		Response response = null;
		
		switch (method) {
			case OPTIONS:
				response = new OptionsResponse(request);
				break;
			case DESCRIBE:
				response = new DescribeResponse(request);
				break;
			case SETUP:
				response = new SetupResponse(request);
				break;
			case TEARDOWN:
				response = new TeardownResponse(request);
				break;
			case PLAY:
				response = new PlayResponse(request);
				break;
			default:
				break;
		}
		
		return response;
	}		
}