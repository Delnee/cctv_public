/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.body;

import net.effanee.protocol.rtsp.client.RstpProtocolException;

/**
 * <p>Abstract Body class. Used to provide common behaviour.</p>
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public abstract class Body{
	
	private final BodyKey key;
	static final String KEY_SEPARATOR = ":";
	
	protected final String[] value;
	
	//Only called by subclasses.
	protected Body(BodyKey key, String[] lines) throws RstpProtocolException {
		
		this.key = key;
		value = lines;
	}

	@Override
	public String toString() {
		return "Body ["
				+ ("key=" + key + ", ")
				+ ("value=" + value)
				+ "]";
	}
}
