/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.header;

import net.effanee.protocol.rtsp.client.RstpProtocolException;

/**
 * <p>Concrete Header class.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class DescribeResponseHeader extends ResponseHeader {

	private Sequence sequence;
	private Date date;
	private ContentLength contentLength;
	private ContentType contentType;
	private ContentBase contentBase;
	
	public DescribeResponseHeader() {
	}

	public void setHeaders(String header) throws RstpProtocolException{
		
		super.setHeaders(header);
		setSequence(lines);
		super.setWwwAuthenticate(lines);
		setDate(lines);
		setContentLength(lines);
		setContentType(lines);
		setContentBase(lines);
	}

	//CSeq should always be present.
	private void setSequence(String[] lines) throws RstpProtocolException{

		sequence = (Sequence)HeaderFactory.readHeader(HeaderKey.CSEQ, lines);
	}

	private void setDate(String[] lines) throws RstpProtocolException{
		
		if(hasHeader(HeaderKey.DATE)){
			date = (Date)HeaderFactory.readHeader(HeaderKey.DATE, lines);
		}
	}

	private void setContentLength(String[] lines) throws RstpProtocolException{
		
		if(hasHeader(HeaderKey.CONTENT_LENGTH)){
			contentLength = (ContentLength)HeaderFactory.readHeader(HeaderKey.CONTENT_LENGTH, lines);
		}
	}

	private void setContentType(String[] lines) throws RstpProtocolException{
		
		if(hasHeader(HeaderKey.CONTENT_TYPE)){
			contentType = (ContentType)HeaderFactory.readHeader(HeaderKey.CONTENT_TYPE, lines);
		}
	}
	
	private void setContentBase(String[] lines) throws RstpProtocolException{
		
		if(hasHeader(HeaderKey.CONTENT_BASE)){
			contentBase = (ContentBase)HeaderFactory.readHeader(HeaderKey.CONTENT_BASE, lines);
		}
	}
	
	public Sequence getSequence() {
		return sequence;
	}

	public Date getDate() {
		return date;
	}
	
	public ContentLength getContentLength(){
		
		return contentLength;
	}

	public ContentType getContentType(){
		
		return contentType;
	}
	
	public ContentBase getContentBase(){
		
		return contentBase;
	}
	
	@Override
	public String toString() {
		
		return "DescribeResponseHeader ["
				+ ("sequence=" + sequence + ",")
				+ ("date=" + date + ",")
				+ super.toString()
				+ "]";
	}
}
