/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.header;

/**
 * <p>HTTP 1.0 Status Code Definitions.</p>
 * 
 * <h2>RFC2068 - HTTP/1.1</h2>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public enum HeaderKey {

	CONTENT_BASE("Content-Base", "Entity"),
	CONTENT_LANGUAGE("Content-Language", "Entity"),
	CONTENT_ENCODING("Content-Encoding", "Entity"),
	CONTENT_LENGTH("Content-Length", "Entity"),
	CONTENT_TYPE("Content-Type", "Entity"),
	
	CSEQ("CSeq", "General"),
	CONNECTION("Connection", "General"),
	DATE("Date", "General"),
	
	SESSION("Session", "Request and Response"),
	TRANSPORT("Transport", "Request and Response"),
	
	ACCEPT("Accept", "Request"),
	AUTHORIZATION("Authorization", "Request"),
	USER_AGENT("User-Agent", "Request"),
	
	PUBLIC("Public", "Response"),
	RANGE("Range", "Response"),
	RTP_INFO("RTP-Info", "Response"),
	WWW_AUTHENTICATE("WWW-Authenticate", "Response");

	private final String key;
	private final String type;

	private HeaderKey(String key, String type) {
		this.key = key;
		this.type = type;
	}
	
	public String key() {
		return key;
	}
	
	public String type(){
		return type;
	}
	
	@Override
	public String toString() {
		return "HeaderKey ["
				+ ("enum=" + this.name() + ", ")
				+ ("key=" + key + ", ")
				+ ("type=" + type)
				+ "]";
	}
}
