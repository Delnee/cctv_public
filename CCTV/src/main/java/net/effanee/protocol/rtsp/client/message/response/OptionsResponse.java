/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.response;

import java.nio.ByteBuffer;
import java.util.List;

import net.effanee.protocol.rtsp.client.RstpProtocolException;
import net.effanee.protocol.rtsp.client.message.body.ResponseBody;
import net.effanee.protocol.rtsp.client.message.header.Date;
import net.effanee.protocol.rtsp.client.message.header.OptionsResponseHeader;
import net.effanee.protocol.rtsp.client.message.header.Public;
import net.effanee.protocol.rtsp.client.message.header.Sequence;
import net.effanee.protocol.rtsp.client.message.header.StatusLine;
import net.effanee.protocol.rtsp.client.message.header.Transport;
import net.effanee.protocol.rtsp.client.message.header.WwwAuthenticate;
import net.effanee.protocol.rtsp.client.message.request.Request;

/**
 * <p>Concrete Response class.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class OptionsResponse extends Response{

	private OptionsResponseHeader responseHeader; 

	public OptionsResponse(Request request) {
		
		this.request = request;
	}
	
	public void setHeaderBytes(ByteBuffer buffer) throws RstpProtocolException {
		
		super.setHeaderBytes(buffer);
		responseHeader = new OptionsResponseHeader();
		responseHeader.setHeaders(responseLines);
	}
	
	public String getResponseLines() {
		return responseLines;
	}

	public Date getDate() {
		return responseHeader.getDate();
	}

	public Public getPublicMethods() {
		return responseHeader.getPublicMethods();
	}

	public List<WwwAuthenticate> getWwwAuthenticates() {
		return responseHeader.getWwwAuthenticates();
	}

	@Override
	public StatusLine getStatusLine() {
		return responseHeader.getStatusLine();
	}
	
	@Override
	public Sequence getSequence() {
		return responseHeader.getSequence();
	}
	
	@Override
	public boolean hasBody() {
		return false;
	}

	@Override
	public ResponseBody getBody() {
		
		return null;
	}
	
	@Override
	public
	int getContentLengthValue() {
		return 0;
	}
	
	@Override
	public String toString() {
		return "OptionsResponse ["
				+ ("responseHeader=" + responseHeader + ",")
				+ super.toString()
				+ "]";
	}

	@Override
	public String getSession() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public Transport getTransport() {
		// TODO Auto-generated method stub
		return null;
	}

}
