/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client;

import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.effanee.protocol.rtsp.client.message.request.Request;
import net.effanee.protocol.rtsp.client.message.request.RequestFactory;
import net.effanee.protocol.rtsp.client.message.request.RtspMethod;
import net.effanee.protocol.rtsp.client.message.response.DescribeResponse;
import net.effanee.protocol.rtsp.client.message.response.HttpStatusCode;
import net.effanee.protocol.rtsp.client.message.response.OptionsResponse;
import net.effanee.protocol.rtsp.client.message.response.PlayResponse;
import net.effanee.protocol.rtsp.client.message.response.Response;
import net.effanee.protocol.rtsp.client.message.response.ResponseFactory;
import net.effanee.protocol.rtsp.client.message.response.SetupResponse;
import net.effanee.protocol.rtsp.client.message.response.TeardownResponse;
import net.effanee.protocol.rtsp.client.state.StateMachine;

/**
 * <p>Manage the RTSP communication between this application and the camera.</p>
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class RtspManager {

	private static final Logger LOGGER = Logger.getLogger(RtspManager.class.getName());
	
	private final Context context;
	private Connection connection;
	private StateMachine stateMachine;
	
	/**
	 * Create a new RTSP manager.
	 * 
	 * A manager consists of:-
	 * <ul>
	 * <li>a Request, with an embedded Response</li>
	 * <li>a Connection, to send Requests and receive Responses</li>
	 * <li>a Context, to store the conversation state</li>
	 * <li>a StateMachine, to constrain state transitions</li>
	 * </ul>
	 * @param uri - the URI of the camera.
	 * @param clientRtpPort - the local RTP port to use.
	 * @param clientRtcpPort - the local RTCP port to use.
	 */
	public RtspManager(URI uri, int clientRtpPort, int clientRtcpPort) {
		
		this.connection = new Connection();
		this.context = new Context(uri, clientRtpPort, clientRtcpPort);
		this.stateMachine = new StateMachine();
	}

	// Move this into the response object...
	private boolean isValid(Response response){
		
		// response.isValid()
		return true;
	}
	
	/**
	 * Send RTSP Options method.
	 * 
	 * <p>An OPTIONS request may be issued at any time, e.g., if the client is
	 * about to try a nonstandard request. It does not influence server state.</p>
	 * 
	 * @return true, if the request was successful.
	 */
	public boolean sendOptions(){
		
		OptionsResponse response = (OptionsResponse)sendRequest(RtspMethod.OPTIONS);
		stateMachine.options();
		return isValid(response);
	}

	/**
	 * Send RTSP Describe method.
	 * 
	 * <p>The DESCRIBE method retrieves the description of a presentation or media
	 * object identified by the request URL from a server. It may use the Accept
	 * header to specify the description formats that the client understands.
	 * The server responds with a description of the requested resource.</p>
	 * 
	 * @return true, if the request was successful.
	 */
	public boolean sendDescribe(){
		
		DescribeResponse response = (DescribeResponse)sendRequest(RtspMethod.DESCRIBE);
		stateMachine.describe();
		return isValid(response);
	}

	/**
	 * Send RTSP Setup method.
	 * 
	 * <p>The SETUP request for a URI specifies the transport mechanism to be
	 * used for the streamed media. The Transport header specifies the transport
	 * parameters acceptable to the client for data transmission; the response
	 * will contain the transport parameters selected by the server.</p>
	 * 
	 * @return true, if the request was successful.
	 */
	public boolean sendSetup(){
		
		SetupResponse response = (SetupResponse)sendRequest(RtspMethod.SETUP);
		stateMachine.setup();
		return isValid(response);
	}

	/**
	 * Send RTSP Play method.
	 * 
	 * <p>The PLAY method tells the server to start sending data via the mechanism
	 * specified in SETUP. The PLAY request positions the normal play time to
	 * the beginning of the range specified and delivers stream data until the
	 * end of the range is reached. A PLAY request without a Range header is
	 * legal. For a on-demand stream, the server replies with the actual range
	 * that will be played back.</p>
	 * 
	 * <p>The live stream from a camera will have a range start of 0 with no end
	 * date.</p>
	 * 
	 * @return true, if the request was successful.
	 */
	public boolean sendPlay(){
		
		PlayResponse response = (PlayResponse)sendRequest(RtspMethod.PLAY);
		stateMachine.play();
		return isValid(response);
	}

	/**
	 * Send RTSP Teardown method.
	 * 
	 * <p>The TEARDOWN request stops the stream delivery for the given URI,
	 * freeing the resources associated with it.</p>
	 * 
	 * @return true, if the request was successful.
	 */
	public boolean sendTeardown(){
		
		TeardownResponse response = (TeardownResponse)sendRequest(RtspMethod.TEARDOWN);
		stateMachine.teardown();
		
		connection.closeConnection();
		
		return isValid(response);
	}

	// Server ports get allocated after the SETUP method is called.
	public int getServerRtpPort() {

		return context.getServerRtpPort();
	}

	public int getServerRtcpPort() {

		return context.getServerRtcpPort();
	}

	public int getPacketizationMode() {

		return context.getPacketizationMode();
	}
	
	/**
	 * Send a request of the required RTSP method type.
	 * 
	 * @param rtspMethod - the type of the request.
	 * @return the response.
	 */
	private Response sendRequest(RtspMethod rtspMethod){
		
		Response response = send(rtspMethod);
		LOGGER.log(Level.INFO, "RTSP request:- \r\n"
									+ response.getRequest().getRequestLines());
		LOGGER.log(Level.INFO, "RTSP response:- \r\n"
									+response.getResponseLines());
		
		// If authorization fails, retry once. Updating the context with the 401
		// status code should trigger a re-authorization attempt when
		// constructing the next request.
		if(response.getStatusLine().getStatusCode() == HttpStatusCode.CODE_401){
			
			response = send(rtspMethod);
			LOGGER.log(Level.FINEST, "RTSP request:- \r\n"
					+ response.getRequest().getRequestLines());
			LOGGER.log(Level.FINEST, "RTSP response:- \r\n"
					+response.getResponseLines());
		}
		
		return response;
	}
	
	/**
	 * Send a Request, get the Response and update the context.
	 * 
	 * @param rtspMethod - the RTSP method to send.
	 * @return a RTSP Response.
	 */
	private Response send(RtspMethod rtspMethod){
		
		Request request = getRequest(rtspMethod);
		Response response = connection.send(request);
		context.update(request);		
		
		return response;
	}
	
	/**
	 * Generate a Request, with embedded Response, from the supplied RTSP
	 * method.
	 * 
	 * @param method - the RTSP method.
	 * @return a RTSP Request.
	 */
	private Request getRequest(RtspMethod method){
		
		Request request = RequestFactory.createRequest(method, this.context);
		request.setResponse(ResponseFactory.createResponse(method, request));
		
		return request;
	}
}