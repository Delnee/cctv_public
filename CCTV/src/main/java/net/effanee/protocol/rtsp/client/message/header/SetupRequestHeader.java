/**
 * This file is part of CCTV.
 *
 * CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.effanee.protocol.rtsp.client.message.header;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import net.effanee.protocol.rtsp.client.message.request.Request;
import net.effanee.protocol.rtsp.client.message.request.RtspMethod;

/**
 * <p>Concrete Header class.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class SetupRequestHeader extends RequestHeader {

	private Sequence sequence;
	private Authorization authorization;
	private UserAgent userAgent;
	private Transport transport;
	
	public void setHeaders(Request request){

		try {
			super.setHeaders(RtspMethod.SETUP, new URI(request.getControlUri()));
		} catch (URISyntaxException e) {
			//TODO tidy
			e.printStackTrace();
		}
		setSequence(request);
		if(request.requiresAuthentication()){
			setAuthorization(request);
		}
		setUserAgent(request);
		setTransport(request);
	}

	//CSeq should always be present.
	private void setSequence(Request request){

		sequence = (Sequence)HeaderFactory.createHeader(HeaderKey.CSEQ, request);
	}

	private void setAuthorization(Request request){

		authorization = (Authorization)HeaderFactory.createHeader(HeaderKey.AUTHORIZATION, request);
	}
	
	private void setUserAgent(Request request){

		userAgent = (UserAgent)HeaderFactory.createHeader(HeaderKey.USER_AGENT, request);
	}

	private void setTransport(Request request){

		transport = (Transport)HeaderFactory.createHeader(HeaderKey.TRANSPORT, request);
	}
	
	public Authorization getAuthorization() {
		
		return authorization;
	}
	
	public Sequence getSequence() {
		
		return sequence;
	}

	public UserAgent getUserAgent() {
		
		return userAgent;
	}

	public Transport getTransport() {
		
		return transport;
	}
	
	@Override
	public String[] getLines() {
		
		List<String> lines = new ArrayList<String>();
		lines.add(super.getRequestLine().getLine() + EOL);
		lines.add(getSequence().getLine() + EOL);
		if(authorization != null){
			lines.add(getAuthorization().getLine() + EOL);
		}
		lines.add(getUserAgent().getLine() + EOL);
		lines.add(getTransport().getLine() + EOL);
		lines.add(EOL);
		
		return lines.toArray(new String[lines.size()]);
	}
	
	@Override
	public String toString() {
		
		return "SetupRequestHeader ["
				+ ("sequence=" + sequence + ",")
				+ ("authorization=" + authorization + ",")
				+ ("userAgent=" + userAgent + ",")
				+ ("transport=" + transport + ",")
				+ super.toString()
				+ "]";
	}
}
