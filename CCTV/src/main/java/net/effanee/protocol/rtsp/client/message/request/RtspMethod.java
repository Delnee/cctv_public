/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.request;

/**
 * <p>RTSP methods.</p>
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public enum RtspMethod {

	// Used.
	OPTIONS("OPTIONS"),
	DESCRIBE("DESCRIBE"),
	SETUP("SETUP"),
	PLAY("PLAY"),
	TEARDOWN("TEARDOWN"),
	
	// Not used...
	ANNOUNCE("ANNOUNCE"),
	GET_PARAMETER("	GET_PARAMETER"),
	SET_PARAMETER("SET_PARAMETER"),
	RECORD("RECORD"),
	PAUSE("PAUSE");

	private final String token;

	private RtspMethod(String token) {
		this.token = token;
	}
	
	public String token() {
		return token;
	}
	
	@Override
	public String toString() {
		return "RtspMethod ["
				+ ("enum=" + this.name() + ", ")
				+ ("token=" + token)
				+ "]";
	}
}
