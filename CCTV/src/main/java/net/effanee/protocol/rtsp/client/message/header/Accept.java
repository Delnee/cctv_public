/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.header;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import net.effanee.protocol.rtsp.client.message.request.Request;

import org.apache.commons.lang3.StringUtils;

/**
 * <p>Concrete Header class.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class Accept extends Header{
	
	private static final List<String> ACCEPT = Arrays.asList("application/sdp");
	
	private final List<String> mediaRanges = new ArrayList<String>();

	//Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
	//media-range;accept-params, ...
	Accept(Request request) {

		super(HeaderKey.ACCEPT, request);
		mediaRanges.addAll(createProducts());
	}
	
	private List<String> createProducts(){
		
		return ACCEPT;
	}
	
	public List<String> getMediaRanges() {
		return Collections.unmodifiableList(mediaRanges);
	}
	
	public String getLine(){
		
		return HeaderKey.ACCEPT.key() + KEY_SEPARATOR
				+ " " + StringUtils.join(mediaRanges, " ");
	}
	
	@Override
	public String toString() {
		return "Accept ["
				+ ("mediaRanges=" + mediaRanges)
				+ ", "
				+ super.toString()
				+ "]";
	}
}
