/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.header;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import net.effanee.protocol.rtsp.client.RstpProtocolException;
import net.effanee.protocol.rtsp.client.message.request.Request;

import org.apache.commons.lang3.StringUtils;

/**
 * <p>Concrete Header class.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class Transport extends Header{
	
	private static final String PARAM_SEPARATOR = ";";
	private static final String KEY_VALUE_SEPARATOR = "=";
	private static final String PORT_SEPARATOR = "-";
	private static final List<String> TRANSPORT = Arrays.asList("RTP/AVP", "unicast", "client_port=");
	
	private static final String PROTOCOL_KEY = "rtp/avp";
	private static final String UNICAST_DELIVERY_KEY = "unicast";
	private static final String MULICATS_DELIVERY_KEY = "multicast";
	private static final String CLIENT_PORT_KEY = "client_port";
	private static final String SERVER_PORT_KEY = "server_port";
	//Synchronization source 32 bits
	private static final String SSCR_KEY = "ssrc";
	private static final String MODE_KEY = "mode";
	
	private final List<String> transport = new ArrayList<String>();
	
	private String protocol;
	private String delivery;
	private int clientRtpPort;
	private int clientRtcpPort;
	private int serverRtpPort;
	private int serverRtcpPort;
	private String ssrc;
	private String mode;
	
	Transport(String[] lines) throws RstpProtocolException {
		
		super(HeaderKey.TRANSPORT, lines);
		transport.addAll(readTransport());
	}

	Transport(Request request) {

		super(HeaderKey.TRANSPORT, request);
		transport.addAll(createTransport());
		
		clientRtpPort = request.getClientRtpPort();
		clientRtcpPort = request.getClientRtcpPort();
	}
	
	//Transport: RTP/AVP;unicast;client_port=58844-58845;server_port=8336-8337;ssrc=290e8147;mode="play"
	private List<String> readTransport() throws RstpProtocolException{
		
		String[] tokens = getTokens(value);
		return getTransport(tokens);
	}
	
	private String[] getTokens(String line){
		
		String[] tokens = StringUtils.split(line, PARAM_SEPARATOR);
		return tokens;
	}
	
	//[RTP/AVP, unicast, client_port=58844-58845, server_port=8342-8343, ssrc=789e9016, mode="play"]
	private List<String> getTransport(String[] tokens) throws RstpProtocolException{
		
		List<String> transport = new ArrayList<String>();
		for(String token: tokens){
			String key = getKey(token);
			assignValue(key, token);
			//TODO Probably need a better structure here to store transport details.
			transport.add(token);
		}
		return transport;
	}
	
	private String getKey(String token){
		
		if(token.contains(KEY_VALUE_SEPARATOR)){
			return token.substring(0, token.indexOf(KEY_VALUE_SEPARATOR));
		}
		else{
			return token;
		}
	}
	
	private String getValue(String token){
		
		if(token.contains(KEY_VALUE_SEPARATOR)){
			return token.substring(token.indexOf(KEY_VALUE_SEPARATOR) + 1);
		}
		else{
			return token;
		}
	}
	
	//8356-8357
	private int getRtpPort(String token) throws RstpProtocolException{
		
		String port = "";
		if(token.contains(PORT_SEPARATOR)){
			port = token.substring(0, token.indexOf(PORT_SEPARATOR));
		}
		
		int portNumber = 0;
		try{
			portNumber = Integer.parseInt(port);
		}
		catch(NumberFormatException e){
			throw new RstpProtocolException("The RTSP response Header "
					+ HeaderKey.TRANSPORT.key()
					+ " has value "
					+ port
					+ " which cannot be parsed to an integer.");
		}
		return portNumber;
		
	}
	
	//8356-8357
	private int getRtcpPort(String token) throws RstpProtocolException{
		
		String port = "";
		if(token.contains(PORT_SEPARATOR)){
			port = token.substring(token.indexOf(PORT_SEPARATOR) + 1);
		}
		
		int portNumber = 0;
		try{
			portNumber = Integer.parseInt(port);
		}
		catch(NumberFormatException e){
			throw new RstpProtocolException("The RTSP response Header "
					+ HeaderKey.TRANSPORT.key()
					+ " has value "
					+ port
					+ " which cannot be parsed to an integer.");
		}
		return portNumber;
		
	}
	
	private void assignValue(String key, String token) throws RstpProtocolException{
		
		switch (key.toLowerCase()) {
			case PROTOCOL_KEY:
				protocol = token;
				break;
			case UNICAST_DELIVERY_KEY:
				delivery = token;
			break;
			case MULICATS_DELIVERY_KEY:
				delivery = token;
			break;
			case CLIENT_PORT_KEY:
//				clientRtpPort = getRtpPort(getValue(token));
//				clientRtcpPort = getRtcpPort(getValue(token));
			break;
			case SERVER_PORT_KEY:
				serverRtpPort = getRtpPort(getValue(token));
				serverRtcpPort = getRtcpPort(getValue(token));
			break;
			case SSCR_KEY:
				ssrc = getValue(token);
			break;
			case MODE_KEY:
				mode = getValue(token);
			break;
			
			default:
				//TODO Ignore or throw exception?
				break;
		}
	}
	
	private List<String> createTransport(){
		
		return TRANSPORT;
	}
	
	public List<String> getTransport() {
		return Collections.unmodifiableList(transport);
	}
	
	public int getServerRtpPort() {

		return serverRtpPort;
	}

	public int getServerRtcpPort() {

		return serverRtcpPort;
	}
	
	public String getLine(){
		
		//"RTP/AVP", "unicast", "client_port=58844-58845"
		return HeaderKey.TRANSPORT.key() + KEY_SEPARATOR
				+ " " + StringUtils.join(transport, PARAM_SEPARATOR)
				+ clientRtpPort + PORT_SEPARATOR + clientRtcpPort;
	}

	@Override
	public String toString() {
		return "Transport ["
				+ ("protocol=" + protocol + ", ")
				+ ("delivery=" + delivery + ", ")
				+ ("clientLowerPort=" + clientRtpPort + ", ")
				+ ("clientUpperPort=" + clientRtcpPort + ", ")
				+ ("serverLowerPort=" + serverRtpPort + ", ")
				+ ("serverUpperPort=" + serverRtcpPort + ", ")
				+ ("ssrc=" + ssrc + ", ")
				+ ("mode=" + mode + ", ")
				+ super.toString()
				+ "]";
	}
}
