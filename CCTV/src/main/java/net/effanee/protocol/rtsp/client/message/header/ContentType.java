/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.header;

import net.effanee.protocol.rtsp.client.RstpProtocolException;
import net.effanee.protocol.rtsp.client.message.body.BodyKey;

import org.apache.commons.lang3.StringUtils;

/**
 * <p>Concrete Header class.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class ContentType extends Header{
	
	//content-type:application/json; charset=UTF-8
	//Content-Type:text/html
	//Content-Type: application/sdp
	private final String content;
	private final boolean hasSdpBody;
	
	ContentType(String[] lines) throws RstpProtocolException {
		
		super(HeaderKey.CONTENT_TYPE, lines);
		content = content();
		hasSdpBody = setHasSdp();
	}

	private String content() throws RstpProtocolException{
		
		return value;
	}
	
	private boolean setHasSdp() throws RstpProtocolException{
		
		if(StringUtils.contains(value.toLowerCase(), BodyKey.SDP.key())){
			return true;
		}
		else{
			return false;
		}
	}
	
	public String getContent() {
		return content;
	}
	
	public boolean hasSdpBody(){
		return hasSdpBody;
	}
	
	@Override
	public String toString() {
		return "ContentType ["
				+ ("content=" + content)
				+ ", "
				+ super.toString()
				+ "]";
	}
}
