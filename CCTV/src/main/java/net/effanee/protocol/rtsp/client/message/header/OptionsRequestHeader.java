/**
 * This file is part of CCTV.
 *
 * CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.effanee.protocol.rtsp.client.message.header;

import net.effanee.protocol.rtsp.client.message.request.Request;
import net.effanee.protocol.rtsp.client.message.request.RtspMethod;

/**
 * <p>Concrete Header class.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class OptionsRequestHeader extends RequestHeader {

	private Sequence sequence;
	private UserAgent userAgent;
	
	public void setHeaders(Request request){

		super.setHeaders(RtspMethod.OPTIONS, request.getUri());
		setSequence(request);
		setUserAgent(request);
	}

	//CSeq should always be present.
	private void setSequence(Request request){

		sequence = (Sequence)HeaderFactory.createHeader(HeaderKey.CSEQ, request);
	}

	private void setUserAgent(Request request){

		userAgent = (UserAgent)HeaderFactory.createHeader(HeaderKey.USER_AGENT, request);
	}

	public Sequence getSequence() {
		
		return sequence;
	}

	public UserAgent getUserAgent() {
		
		return userAgent;
	}

	@Override
	public String[] getLines() {

		String[] lines = {super.getRequestLine().getLine() + EOL,
							getSequence().getLine() + EOL,
							getUserAgent().getLine() + EOL,
							EOL};
		
		return lines;
	}
	
	@Override
	public String toString() {
		return "OptionsRequestHeader ["
				+ ("sequence=" + sequence + ",")
				+ ("userAgent=" + userAgent + ",")
				+ super.toString()
				+ "]";
	}
}
