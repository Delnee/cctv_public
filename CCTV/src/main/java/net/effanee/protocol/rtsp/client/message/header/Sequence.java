/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.header;

import net.effanee.protocol.rtsp.client.RstpProtocolException;
import net.effanee.protocol.rtsp.client.message.request.Request;

/**
 * <p>Concrete Header class.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class Sequence extends Header{
	
	private int sequence;
	
	Sequence(String[] lines) throws RstpProtocolException {
		
		super(HeaderKey.CSEQ, lines);
		sequence = readSequence();
	}

	Sequence(Request request){
		
		super(HeaderKey.CSEQ, request);
		sequence = request.getSequenceValue();
	}
	
	private int readSequence() throws RstpProtocolException{
		
		int seq = 0;
		try{
			seq = Integer.parseInt(value);
		}
		catch(NumberFormatException e){
			throw new RstpProtocolException("The RTSP response Header "
					+ HeaderKey.CSEQ.key()
					+ " has value "
					+ value
					+ " which cannot be parsed to an integer.");
		}
		return seq;
	}

	public int getSequence(){
		
		return sequence;
	}
	
	public String getLine(){
		
		return HeaderKey.CSEQ.key() + KEY_SEPARATOR
				+ " " + String.valueOf(sequence);
	}
	
	@Override
	public String toString() {
		return "Sequence ["
				+ ("sequence=" + sequence)
				+ ", "
				+ super.toString()
				+ "]";
	}
}
