/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.response;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.List;

import net.effanee.protocol.rtsp.client.RstpProtocolException;
import net.effanee.protocol.rtsp.client.message.body.ResponseBody;
import net.effanee.protocol.rtsp.client.message.header.Sequence;
import net.effanee.protocol.rtsp.client.message.header.StatusLine;
import net.effanee.protocol.rtsp.client.message.header.Transport;
import net.effanee.protocol.rtsp.client.message.header.WwwAuthenticate;
import net.effanee.protocol.rtsp.client.message.request.Request;

/**
 * <p>Abstract Request class. Used to provide common behaviour.</p>
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public abstract class Response {
	
	protected Request request;
	protected String responseHeaderLines;
	protected String responseBodyLines;
	protected String responseLines;
		
	//Should always be present.
	public abstract StatusLine getStatusLine();
	public abstract Sequence getSequence();
	public abstract List<WwwAuthenticate> getWwwAuthenticates();
	public abstract ResponseBody getBody();
	public abstract boolean hasBody();
	public abstract String getSession();
	public abstract Transport getTransport();
	public abstract int getContentLengthValue();	
	
	public void setHeaderBytes(ByteBuffer buffer) throws RstpProtocolException {
		
		responseHeaderLines = StandardCharsets.UTF_8.decode(buffer).toString();
		responseLines = responseHeaderLines;
	}

	public void setBodyBytes(ByteBuffer buffer) throws RstpProtocolException {
		
		responseBodyLines = StandardCharsets.UTF_8.decode(buffer).toString();
		responseLines += responseBodyLines;
	}
	
	public Request getRequest(){

		return request;
	}
	
	public String getResponseLines() {
		
		return responseLines;
	}
	
	@Override
	public String toString() {
		return "Response ["
				+ ("response=" + responseLines)
				+ "]";
	}
}
