/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.header;

import net.effanee.protocol.rtsp.client.RstpProtocolException;

/**
 * <p>Concrete Header class.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class OptionsResponseHeader extends ResponseHeader {

	private Sequence sequence;
	private Date date;
	private Public publicMethods;
	
	public OptionsResponseHeader() {
		// TODO Auto-generated constructor stub
	}

	public void setHeaders(String header) throws RstpProtocolException{
		
		super.setHeaders(header);
		setSequence(lines);
		setDate(lines);
		setPublic(lines);
	}

	//CSeq should always be present.
	private void setSequence(String[] lines) throws RstpProtocolException{

		sequence = (Sequence)HeaderFactory.readHeader(HeaderKey.CSEQ, lines);
	}

	private void setDate(String[] lines) throws RstpProtocolException{
		
		if(hasHeader(HeaderKey.DATE)){
			date = (Date)HeaderFactory.readHeader(HeaderKey.DATE, lines);
		}
	}
	
	private void setPublic(String[] lines) throws RstpProtocolException{
		
		publicMethods = (Public)HeaderFactory.readHeader(HeaderKey.PUBLIC, lines);
	}

	public Sequence getSequence() {
		return sequence;
	}

	public Date getDate() {
		return date;
	}

	public Public getPublicMethods() {
		return publicMethods;
	}
	
	@Override
	public String toString() {
		return "OptionsResponseHeader ["
				+ ("sequence=" + sequence + ",")
				+ ("date=" + date + ",")
				+ ("publicMethods=" + publicMethods + ",")
				+ super.toString()
				+ "]";
	}
}
