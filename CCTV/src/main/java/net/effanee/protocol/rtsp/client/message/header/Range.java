/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.header;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import net.effanee.protocol.rtsp.client.RstpProtocolException;
import net.effanee.protocol.rtsp.client.message.request.Request;

import org.apache.commons.lang3.StringUtils;

/**
 * <p>Concrete Header class.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class Range extends Header{
	
	private static final List<String> RANGE = Arrays.asList("npt=0.000-");
	
	private final List<String> mediaRanges = new ArrayList<String>();

	Range(String[] lines) throws RstpProtocolException {
		
		super(HeaderKey.USER_AGENT, lines);
		//TODO this is wrong should not be constant??
		RANGE.add(readRanges());
	}
	
	//Range: npt=0.000-596.458
	Range(Request request) {

		super(HeaderKey.RANGE, request);
		mediaRanges.addAll(createProducts());
	}
	
	private String readRanges() throws RstpProtocolException{
		
		return value;
	}
	
	private List<String> createProducts(){
		
		return RANGE;
	}
	
	public List<String> getRanges() {
		return Collections.unmodifiableList(mediaRanges);
	}
	
	public String getLine(){
		
		return HeaderKey.RANGE.key() + KEY_SEPARATOR
				+ " " + StringUtils.join(mediaRanges, " ");
	}
	
	@Override
	public String toString() {
		return "Range ["
				+ ("mediaRanges=" + mediaRanges)
				+ ", "
				+ super.toString()
				+ "]";
	}
}
