/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.header;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import net.effanee.protocol.rtsp.client.RstpProtocolException;
import net.effanee.protocol.rtsp.client.message.request.Request;

import org.apache.commons.lang3.StringUtils;

/**
 * <p>Concrete Header class.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class UserAgent extends Header{
	
	private static final String SEPARATOR = " ";
	private static final String COMMENT_START = "(";
	private static final List<String> USER_AGENT = Arrays.asList("Mozilla/5.0 (Windows NT 6.1; WOW64)", "Rtsp/0.1 (effanee.net)");
	//User-Agent: LibVLC/2.1.3 (LIVE555 Streaming Media v2014.01.21)
	//private static final List<String> USER_AGENT = Arrays.asList("LibVLC/2.1.3 (LIVE555 Streaming Media v2014.01.21)");

	
	private final List<String> products = new ArrayList<String>();
	
	UserAgent(String[] lines) throws RstpProtocolException {
		
		super(HeaderKey.USER_AGENT, lines);
		products.addAll(readProducts());
	}

	UserAgent(Request request) {

		super(HeaderKey.USER_AGENT, request);
		products.addAll(createProducts());
	}
	
	//User-Agent:Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36
	private List<String> readProducts() throws RstpProtocolException{
		
		String[] tokens = getTokens(value);
		return getProducts(tokens);
	}
	
	private String[] getTokens(String line){
		
		String[] tokens = StringUtils.split(line, SEPARATOR);
		return tokens;
	}
	
	private List<String> getProducts(String[] tokens){
		
		List<String> product = new ArrayList<String>();
		for(String token: tokens){
			if(token.length() > 0 && !token.startsWith(COMMENT_START)){
				product.add(token);
			}
		}
		return product;
	}
	
	private List<String> createProducts(){
		
		return USER_AGENT;
	}
	
	public List<String> getProducts() {
		return Collections.unmodifiableList(products);
	}
	
	public String getLine(){
		
		return HeaderKey.USER_AGENT.key() + KEY_SEPARATOR
				+ " " + StringUtils.join(products, " ");
	}
	
	@Override
	public String toString() {
		return "UserAgent ["
				+ ("products=" + products)
				+ ", "
				+ super.toString()
				+ "]";
	}
}
