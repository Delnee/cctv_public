/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.header;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.effanee.protocol.rtsp.client.RstpProtocolException;

import org.apache.commons.lang3.StringUtils;

/**
 * <p>Concrete Header class.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class Public extends Header{
	
	private static final String SEPARATOR = ",";
	
	private final List<String> methods = new ArrayList<String>();
	
	Public(String[] lines) throws RstpProtocolException {
		
		super(HeaderKey.PUBLIC, lines);
		methods.addAll(methods());
	}

	//Public: OPTIONS, DESCRIBE, PLAY, PAUSE, SETUP, TEARDOWN, SET_PARAMETER, GET_PARAMETER
	private List<String> methods() throws RstpProtocolException{
		
		String[] tokens = getTokens(value);
		return getMethods(tokens);
	}
	
	private String[] getTokens(String line){
		
		String[] tokens = StringUtils.split(line, SEPARATOR);
		return tokens;
	}
	
	private List<String> getMethods(String[] tokens){
		
		List<String> method = new ArrayList<String>();
		for(String token: tokens){
			if(token.length() > 0){
				method.add(token.trim());
			}
		}
		return method;
	}
	
	public List<String> getProducts() {
		return Collections.unmodifiableList(methods);
	}
	
	@Override
	public String toString() {
		return "Public ["
				+ ("methods=" + methods)
				+ ", "
				+ super.toString()
				+ "]";
	}
}
