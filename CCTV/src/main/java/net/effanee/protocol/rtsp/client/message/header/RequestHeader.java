/**
 * This file is part of CCTV.
 *
 * CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.effanee.protocol.rtsp.client.message.header;

import java.net.URI;

import net.effanee.protocol.rtsp.client.message.request.RtspMethod;

/**
 * <p>Abstract Request class. Used to provide common behaviour.</p>
 *
 * <p>Parses a RTSP response header, and provides methods to return the contents
 * of the response header.</p>
 * 
 * <h2>RFC2326 - Real Time Streaming Protocol</h2>
 * 
 * <h3>Appendix D: Minimal RTSP implementation - D.1 Client</h3>
 * 
 * <p>Parse and understand the following headers in responses: CSeq,
 * Connection, Session, Transport, Content-Language, Content-
 * Encoding, Content-Length, Content-Type. RTP-compliant
 * implementations should also implement RTP-Info.
 *</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public abstract class RequestHeader {
	
	static final String EOL = "\r\n";
	
	private RequestLine requestLine;
	
	public abstract String[] getLines();
	
	void setHeaders(RtspMethod method, URI uri){
		
		setRequestLine(method, uri);
	}
	
	private void setRequestLine(RtspMethod method, URI uri){
			
		requestLine = new RequestLine(method, uri);
	}

	public RequestLine getRequestLine() {
		
		return requestLine;
	}
	
	@Override
	public String toString() {
		return "RequestHeader ["
				+ ("requestLine=" + requestLine + ", ")
				+ "]";
	}
}
