/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.header;

import java.net.URI;

import net.effanee.protocol.rtsp.client.message.request.RtspMethod;

/**
 * <p>Request header lines are preceded by a single request line.</p> 
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class RequestLine{
	
	private static final String SEPARATOR = " ";
	private static final String RTSP_VERSION = "RTSP/1.0";
	
	private final String method; 
	private final String uri;
	private final String rtspVersion;
	
	//Request-Line = Method SP Request-URI SP RTSP-Version CRLF
	public RequestLine(RtspMethod method, URI uri){
		
		this.method = method.token();
		this.uri = getUri(uri);
		rtspVersion = getRtspVersion();
	}

	private String getUri(URI uri){
		
		return uri.toString();
	}
	
	private String getRtspVersion(){
		
		return RTSP_VERSION;
	}

	String getLine(){
		
		return method + SEPARATOR
				+ uri + SEPARATOR
				+ rtspVersion;
	}
	
	@Override
	public String toString() {
		return "RequestLine ["
				+ ("method=" + method + ", ")
				+ ("uri=" + uri + ", ")
				+ ("rtspVersion=" + rtspVersion)
				+ "]";
	}
}
