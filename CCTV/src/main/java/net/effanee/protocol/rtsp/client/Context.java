/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client;

import java.net.URI;
import java.util.List;
import java.util.logging.Logger;

import net.effanee.protocol.rtsp.client.message.authorization.Authorization;
import net.effanee.protocol.rtsp.client.message.authorization.BasicAuthorization;
import net.effanee.protocol.rtsp.client.message.body.BodyKey;
import net.effanee.protocol.rtsp.client.message.body.ResponseBody;
import net.effanee.protocol.rtsp.client.message.body.Sdp;
import net.effanee.protocol.rtsp.client.message.body.SdpResponseBody;
import net.effanee.protocol.rtsp.client.message.header.WwwAuthenticate;
import net.effanee.protocol.rtsp.client.message.header.WwwAuthenticate.AuthScheme;
import net.effanee.protocol.rtsp.client.message.header.WwwAuthenticate.Scheme;
import net.effanee.protocol.rtsp.client.message.request.Request;

/**
 * <p>Stores the state of variables during a RTSP exchange.</p>
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
//The only RTSP methods that need to be implemented are:-
/*
 OPTIONS("OPTIONS"),
 DESCRIBE("DESCRIBE"),
 SETUP("SETUP"),
 PLAY("PLAY"),
 TEARDOWN("TEARDOWN");
 */
public class Context {

	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(Context.class.getName());

	private static final String USERNAME_PASSWORD_SEPARATOR = ":";
	
	private final AuthScheme FAVOURED = AuthScheme.BASIC;
	private final AuthScheme UNFAVOURED = AuthScheme.DIGEST;
	private final URI uri;
	private final String userId;
	private final String password;	
	private final int clientRtpPort;
	private final int clientRtcpPort;
	
	private int sequence = 0;
	private String session = "";
	private Authorization authorization;
	private boolean requiresAuthentication = false;
	private Sdp sdp;
	private int packetizationMode;
	private int serverRtpPort;
	private int serverRtcpPort;

	public Context(URI uri, int clientRtpPort, int clientRtcpPort) {

		this.uri = uri;
		userId = getUserNamePassword()[0];
		password = getUserNamePassword()[1];
		
		this.clientRtpPort = clientRtpPort;
		this.clientRtcpPort = clientRtcpPort;
	}

	public void update(Request request) {

		// Increment sequence.
		updateSequence(request);
		updateAuthenticate(request);
		updateSdp(request);
		updateSession(request);
		updateServerRtpPorts(request);
	}

	/**Get the CCTV user/password setting from the URI.
	 * 
	 * @return the user/password pair, or null if it is not set.
	 */
	private String[] getUserNamePassword(){
		
		String[] namePassword = uri.getUserInfo().split(USERNAME_PASSWORD_SEPARATOR);
		return namePassword;
	}	

	private void updateSession(Request request) {

		setSession(request.getResponse().getSession());
	}

	private void updateSdp(Request request) {

		if (request.getResponse().hasBody()) {

			ResponseBody body = request.getResponse().getBody();
			if (body.getBodyType() == BodyKey.SDP) {
				sdp = ((SdpResponseBody) body).getSdp();
				packetizationMode = sdp.getMediaSections().get(0).getPacketizationMode();
			}
		}
		
		// Get the Content-Base and check...
		// It can be structured as:-
		// Content-Base: rtsp://184.72.239.149/vod/mp4:BigBuckBunny_175k.mov/
		// a=control:trackID=2
		// where the URI is constructed from the control attribute appended
		// to the Content-Base.
		// Per Wowza Streaming Engine.
	}

	private void updateAuthenticate(Request request) {

		// public String getCredentials

		List<WwwAuthenticate> wwwAuthenticates = request.getResponse()
				.getWwwAuthenticates();
		Scheme scheme = pickScheme(wwwAuthenticates);
		if (scheme == null) {
			// Authentication not required.
			return;
		}
		if (scheme.getAuthScheme() == AuthScheme.BASIC) {
			// Update BASIC credentials.
			updateBasicCredentials(scheme);
		}
		else if (scheme.getAuthScheme() == AuthScheme.DIGEST) {
			// Update DIGEST credentials.
			// TODO
		}

		requiresAuthentication = true;
	}

	private Scheme pickScheme(List<WwwAuthenticate> wwwAuthenticates) {

		Scheme favoured = null;
		Scheme unfavoured = null;

		Scheme scheme = null;
		for (WwwAuthenticate wwwAuthenticate : wwwAuthenticates) {
			scheme = wwwAuthenticate.getScheme();
			if (scheme.getAuthScheme() == FAVOURED) {
				favoured = scheme;
			}
			else if (scheme.getAuthScheme() == UNFAVOURED) {
				unfavoured = scheme;
			}
		}

		if (favoured != null) {
			return favoured;
		}
		else if (unfavoured != null) {
			return unfavoured;
		}
		else {
			return null;
		}
	}

	private void updateBasicCredentials(Scheme scheme) {

		if (authorization == null) {
			authorization = new BasicAuthorization(scheme, getUserId(),
					getPassword());
		}
		else {
			authorization.updateCredentials(scheme);
		}
	}

	private String getUserId() {

		return userId;
	}

	private String getPassword() {

		return password;
	}

	public boolean requiresAuthentication() {

		return requiresAuthentication;
	}

	public String getAuthorisationValue() {

		return authorization.getCredentialsValue();
	}

	public boolean hasSession() {

		return session.length() > 0;
	}

	public String getSessionValue() {

		return session;
	}

	public int getSequenceValue() {

		return sequence;
	}

	private void setSequence(int sequence) {

		this.sequence = sequence;
	}

	private void setSession(String session) {

		this.session = session;
	}

	private void setServerRtpPort(int port) {

		this.serverRtpPort = port;
	}

	private void setServerRtcpPort(int port) {

		this.serverRtcpPort = port;
	}

	public int getServerRtpPort() {

		return serverRtpPort;
	}

	public int getServerRtcpPort() {

		return serverRtcpPort;
	}

	public int getClientRtpPort() {

		return clientRtpPort;
	}

	public int getClientRtcpPort() {

		return clientRtcpPort;
	}
	
	public int getPacketizationMode() {

		return packetizationMode;
	}

	private void updateSequence(Request request) {

		setSequence(request.getSequenceValue() + 1);
	}

	private void updateServerRtpPorts(Request request) {

		if (request.getResponse().getTransport() != null){
			setServerRtpPort(request.getResponse().getTransport()
					.getServerRtpPort());
			setServerRtcpPort(request.getResponse().getTransport()
					.getServerRtcpPort());
		}
	}
	
	public URI getUri() {

		return uri;
	}

	public String getControlUri() {

		return sdp.getVideoStreamUri();
	}

	@Override
	public String toString() {
		return "Context [" + ("uri=" + uri + ", ")
				+ ("sequence=" + sequence + ", ") + ("session=" + session)
				+ ("requiresAuthentication=" + requiresAuthentication)
				+ ("authorization=" + authorization) + "]";
	}
}