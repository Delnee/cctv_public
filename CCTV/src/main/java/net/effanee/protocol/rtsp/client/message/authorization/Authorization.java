/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.authorization;

import net.effanee.protocol.rtsp.client.message.header.WwwAuthenticate.Scheme;

/**
 * <h2>RFC2617 - HTTP Authentication: Basic and Digest Access Authentication</h2>
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public abstract class Authorization {
	
	static final String KEY_VALUE_SEPARATOR = ":";
	
	private Scheme scheme;
	private final String userId;
	private final String password;
	
	public Authorization(String userId, String password) {

		this.userId = userId;
		this.password = password;
	}

	public abstract void updateCredentials(Scheme scheme);
	public abstract String getCredentialsValue();
	
	public Scheme getScheme() {
		return scheme;
	}

	public void setScheme(Scheme scheme) {
		this.scheme = scheme;
	}

	public String getUserId() {
		return userId;
	}

	public String getPassword() {
		return password;
	}
	
	public String toString() {
		return "Authorization ["
				+ ("scheme=" + scheme + ", ")
				+ ("userId=" + userId + ", ")
				+ ("password=" + password)
				+ "]";
	}
}
