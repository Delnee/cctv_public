/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtp.client;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.effanee.packetize.rtp.RtpPacket;

/**
 * <p>Read the RTP bytes from the file.</p>
 * 
 * <p>The file format:-</p>
 * <ul>
 * 	<li>4 byte integer - giving the length of the following RTP packet data</li>
 * </ul>
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class RtpFileReader {

	private static final Logger LOGGER = Logger.getLogger(RtpFileReader.class.getName());
	
	private DataInputStream dataInputStream;
	
	/**
	 * Create a new RTP reader.
	 */
	public RtpFileReader() {
	}

	/**
	 * Read the supplied file and parse the RTP packets contained within.
	 * 
	 * @param path - the file to parse.
	 * @return a list of RTP packets.
	 */
	public List<RtpPacket> getRtpPackets(Path path) {

			// Open the file.
			try {
				openReader(path);
			}
			catch (IOException e) {
				return null;
			}
	
			// Read the file..
			List<RtpPacket> rtpPackets = readPackets(path);
			
			// Close the file.
			closeReader(path);
			
			return rtpPackets;
	}
	
	/**
	 * Open a blocking, read only, file.
	 * 
	 * @throws IOException 
	 */
	private void openReader(Path path) throws IOException{
		
		try {
			dataInputStream =
					new DataInputStream(
					new BufferedInputStream(
					Files.newInputStream(path, StandardOpenOption.READ)));
			
			LOGGER.log(Level.FINEST, "Opened the file " + path);
		}
		catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Could not open the file " + path + ".", e);
			throw e;
		}
	}	
	
	/**
	 * Read the file and create a list of RTP packets.
	 * 
	 * @param path - the file to parse.
	 * @return a list RTP packlets.
	 */
	private List<RtpPacket> readPackets(Path path) {

		List<RtpPacket> rtpPackets = new ArrayList<>(450);
		
		try {
			while (dataInputStream.available() > 0) {

				// Get packet length.
				byte[] packet = new byte[dataInputStream.readInt()];

				// Read the packet data.
				dataInputStream.readFully(packet);
				
				// Create an RTP packet, with an encapsulated H.264 Network
				// Abstraction Layer, (NAL), payload.
				rtpPackets.add(new RtpPacket(packet));
			}
		}
		catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Could not read the file " + path + ".", e);
		}
		
		return rtpPackets;
	}

	/**
	 * Close the file.
	 */
	private void closeReader(Path path){
		
		try {
			dataInputStream.close();
			LOGGER.log(Level.FINEST, "Closed the file " + path);
		}
		catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Could not close the file " + path + ".", e);
		}
	}	
	
}
