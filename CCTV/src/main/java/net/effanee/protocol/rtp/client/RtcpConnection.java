/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtp.client;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.StandardProtocolFamily;
import java.net.StandardSocketOptions;
import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.DatatypeConverter;

import net.effanee.protocol.rtp.client.message.Bye;
import net.effanee.protocol.rtp.client.message.ReceiverReport;
import net.effanee.protocol.rtp.client.message.RtcpMessage;
import net.effanee.protocol.rtp.client.message.RtcpPacket;
import net.effanee.protocol.rtp.client.message.SenderReport;
import net.effanee.protocol.rtp.client.message.SourceDescription;

/**
 * <p>An RTCP connection to a remote host, via UDP, using the supplied URI and
 * ports.</p>
 * 
 * <p>Instances of this class are run in their own thread, and must be
 * terminated by calling the stop() method, which interrupts the thread and
 * closes the connection.</p>
 *
 * <h2>RFC 3550 - RTP: A Transport Protocol for Real-Time Applications</h2>
 * 
 * <p>The RTP control protocol (RTCP) is based on the periodic transmission of
 * control packets to all participants in a session. The RTP control protocol
 * (RTCP) is based on the periodic transmission of control packets to all
 * participants in the session.</p>
 * 
 * <p>When a participant wishes to leave a session, a BYE packet is transmitted to
 * inform the other participants of the event. However, a participant which
 * never sent an RTP or RTCP packet must not send a BYE packet when they leave
 * the group.</p>
 * 
 * <p>12.1 RTCP Packet Types</p>
 * <pre>
 *      abbrev. name                value
 *      SR      sender report       200
 *      RR      receiver report     201
 *      SDES    source description  202
 *      BYE     goodbye             203
 *      APP     application-defined 204
 * </pre>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class RtcpConnection implements Runnable{

	private static final Logger LOGGER = Logger.getLogger(RtcpConnection.class.getName());
	
	// Defaults.
	private static final int BUFFER_INCREMENT = 65535;
	private static final int MIN_PACKET_BYTES = 4 * 3;	// Arbitrary.
	
	private final URI uri;
	private final int clientPort;
	private final int serverPort;
	private final int payloadSize;
	
	private Thread thisThread;
	private SelectionKey key = null;
	private byte[] lastMessage = null;
	
	/**
	 * Create a new connection.
	 * 
	 * @param uri - the remote host URI - only the IP address is used.
	 * @param clientPort - the local IP port.
	 * @param serverPort - the remote IP port.
	 * @param payloadSize - the RTP payload bytes size. 
	 */
	public RtcpConnection(URI uri, int clientPort, int serverPort, int payloadSize){
		
		this.uri = uri;
		this.clientPort = clientPort;
		this.serverPort = serverPort;
		this.payloadSize = payloadSize;
		LOGGER.log(Level.FINEST, this.toString());
	}

	/**
	 * Connect to the remote host and read packets until the stop() method is
	 * called.
	 */
	@Override
	public void run() {

		// At this point we can get a reference to the executing thread,
		// which can be used later to stop the thread.
		thisThread = Thread.currentThread();
		
		// Open a connection to the server.
		try {
			openConnection();
		}
		catch (IOException e) {
			return;
		}

		// Read UDP packets until the stop() method is called. Or, the buffer
		// is closed, and has caused the read method to exit.
		read();
		
		// Close the connection.
		closeConnection();
	}

	/**
	 * Open a non-blocking, read only, connection to the remote host.
	 * 
	 * @throws IOException 
	 */
	private void openConnection() throws IOException{
		
		try {
			// Create a UDP socket and bind to the local address.
			DatagramChannel channel = DatagramChannel.open(StandardProtocolFamily.INET)
					.setOption(StandardSocketOptions.SO_REUSEADDR, true)
					.setOption(StandardSocketOptions.SO_RCVBUF, BUFFER_INCREMENT)
					.bind(new InetSocketAddress(getIpv4InterfaceAddress(), clientPort));
			
			// Non-blocking.
			channel.configureBlocking(false);
			
			// Bind to one remote address.
			SocketAddress remoteSocket = new InetSocketAddress(InetAddress.getByName(uri.getHost()), serverPort);
			channel.connect(remoteSocket);
			
			// Store the key and use it to access this connection.
			key = channel.register(Selector.open(), SelectionKey.OP_READ);
		}
		catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Could not open the DatagramChannel", e);
			throw e;
		}
		
		LOGGER.log(Level.FINEST, "Opened the DatagramChannel.");
	}
	
	/**
	 * Read the datagram packets, and write them to the console, until close()
	 * is invoked.
	 */
	private void read(){

		ByteBuffer byteBuffer = ByteBuffer.allocate(payloadSize);
		
		while (!Thread.currentThread().isInterrupted()) {

			try {
				// Only one key is associated with the channel, so we can skip
				// iterating over the selected keys set, returned from:-
				// 		key.selector().selectedKeys().iterator();
                key.selector().select();
                byteBuffer.clear();
				((DatagramChannel) key.channel()).read(byteBuffer);
				byteBuffer.flip();
				
				// Display on console.
				lastMessage = new byte[byteBuffer.remaining()];
				byteBuffer.get(lastMessage);
				processMessage(lastMessage);
			}
			catch (ClosedByInterruptException e) {
				// Assume the exception is caused by the stop() method being
				// invoked and exit.
				LOGGER.log(Level.INFO, "The thread was expectedly interrupted.");
				break;
			}
			catch (IOException e) {
				LOGGER.log(Level.SEVERE, "The thread experienced an unexpected exception.", e);
				break;
			}
		}
		
		// TODO Send BYE packet.
		//a participant which never sent an RTP or RTCP packet must not send a BYE
		//packet when they leave the group.
		
	}

	private void processMessage(byte[] lastMessage){
		
		// Check length.
		if (lastMessage.length < MIN_PACKET_BYTES){
			LOGGER.log(Level.SEVERE, "The RTCP packet "
					+ DatatypeConverter.printHexBinary(lastMessage)
					+ " was too short.");
		}		
		
		// Get packet type.
		RtcpPacket rtcpPacket;
		try {
			rtcpPacket = getPacketType(lastMessage);
		}
		catch (IllegalArgumentException e) {
			return;
		}
		
		//TODO Break out into factory method...
		// Create the packet object.
		RtcpMessage rtcpMessage = null;
		switch (rtcpPacket) {
			case SENDER_REPORT:
				rtcpMessage = new SenderReport();
				rtcpMessage.parse(lastMessage);		
				break;

			case RECEIVER_REPORT:
				rtcpMessage = new ReceiverReport();
				rtcpMessage.parse(lastMessage);		
				break;

			case SOURCE_DESCRIPTION:
				rtcpMessage = new SourceDescription();
				rtcpMessage.parse(lastMessage);		
				break;
				
			case BYE:
				rtcpMessage = new Bye();
				rtcpMessage.parse(lastMessage);		
				break;
				
			default:
				LOGGER.log(Level.SEVERE, "Received an unimplemented RTCP packet "
						+ rtcpPacket
						+ " - this functionality needs to be implemented.");				
				break;
		}
		
		LOGGER.log(Level.FINEST, "Received RTCP packet - " + rtcpMessage);
	}
	
	/**
	 * Get the RTCP packet type.
	 * 
	 * @param lastMessage - the packet bytes.
	 * @return an RtcpPacket enum, or throw IllegalArgumentException if the
	 * packet type cannot be determined.
	 * @throws IllegalArgumentException 
	 */
	private RtcpPacket getPacketType(byte[] lastMessage) throws IllegalArgumentException{
		
		RtcpPacket rtcpPacket = RtcpPacket.getRtcpPacket(lastMessage[1]);
		if (rtcpPacket == null){
			LOGGER.log(Level.SEVERE, "The RTCP packet type for "
					+ DatatypeConverter.printHexBinary(lastMessage)
					+ "was not recognised.");
			throw new IllegalArgumentException();			
		}
		
		return rtcpPacket;
	}
	
	/**
	 * Close this connection.
	 */
	private void closeConnection(){
		
		try {
			key.selector().close();
			key.channel().close();
			LOGGER.log(Level.FINEST, "Closed the DatagramChannel.");
		}
		catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Could not close the DatagramChannel.", e);
		}
	}
	
	/**
	 * Close this connection, by setting the interrupt flag. This method
	 * must be called to close and tidy a thread running an instance of this
	 * class.
	 */
	public void stop(){
		
		thisThread.interrupt();
	}
	
	protected byte[] getLastMessage(){
	
		return lastMessage;
	}
	
	/**
	 * Get the IPv4 interface address for the first active, non-loop back, NIC.
	 * 
	 * @return the address, or null.
	 */
	protected InetAddress getIpv4InterfaceAddress(){

		InetAddress addr = null;
		try {
			// Loop the interface cards.
			Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
			while (interfaces.hasMoreElements()) {
				
				NetworkInterface networkInterface = interfaces.nextElement();
				if (!networkInterface.isLoopback() && networkInterface.isUp()) {
					
					// Loop an interfaces addresses.
					for (InterfaceAddress address : networkInterface.getInterfaceAddresses()) {
						addr = address.getAddress();
						if (addr != null && addr instanceof Inet4Address)
							LOGGER.log(Level.INFO, "Obtained the following local NIC IPv4 address "
									+ addr.getHostAddress()
									+ " with MTU of "
									+ networkInterface.getMTU());
							return addr;
					}
				}
			}
		}
		catch (SocketException e) {
			LOGGER.log(Level.SEVERE, "A network interface address could not be obtained for this device.", e);
		}
		return addr;
	}
	
	@Override
	public String toString() {
		return "RtcpConnection ["
				+ ("uri=" + uri + ",")
				+ ("clientPort=" + clientPort + ",")
				+ ("serverPort=" + serverPort)
				+ "]";
	}
}
