/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtp.client.message;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.DatatypeConverter;

/**
 * <p>RTCP Sender Report packet for transmission and reception statistics from
 * participants that are active senders.</p>
 * 
 * <h2>FC 3550 - RTP: A Transport Protocol for Real-Time Applications</h2>
 *
 * <h3>6.4.2 RR: Receiver Report RTCP Packet</h3>
 * <pre>
 *         0                   1                   2                   3
 *         0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * header |V=2|P|   RC   |   PT=RR=201   |             length             |
 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *        |                     SSRC of packet sender                     |
 *        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
 * report |                 SSRC_1 (SSRC of first source)                 |
 * block  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *   1    | fraction lost|       cumulative number of packets lost        |
 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *        |            extended highest sequence number received          |
 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *        |                     inter-arrival jitter                      |
 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *        |                        last SR (LSR)                          |
 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *        |                 delay since last SR (DLSR)                    |
 *        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
 * report |               SSRC_2 (SSRC of second source)                  |
 * block  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *   2    :                            ...                                :
 *        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
 *        |                profile-specific extensions                    |
 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * </pre>
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class ReceiverReport extends RtcpMessage {

	private static final Logger LOGGER = Logger.getLogger(ReceiverReport.class.getName());
	
	protected long ssrcId = 0l;
	protected int fractionLost = 0;
	protected long packetsLost = 0;
	protected long highestReceived = 0l;
	protected long jitter = 0l;
	protected long lastSenderReport = 0l;
	protected long delayLastSenderReport = 0l;

	/**
	 * A temporary implementation...
	 * 
	 * Parse the body.
	 * 
	 * <pre>
	 * 		   0                   1                   2                   3
	 * 		   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
	 *        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
	 * report |                 SSRC_1 (SSRC of first source)                 |
	 * block  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	 *        | fraction lost|       cumulative number of packets lost        |
	 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	 *        |            extended highest sequence number received          |
	 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	 *        |                     inter-arrival jitter                      |
	 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	 *        |                        last SR (LSR)                          |
	 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	 *        |                 delay since last SR (DLSR)                    |
	 *        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
	 *</pre>
	 * 
	 * @param received - the bytes received in the RTCP packet.
	 * @throws IllegalArgumentException - if the length of the report is less
	 * than 24 bytes.
	 */
	@Override
	public void parse(byte[] received) throws IllegalArgumentException {
	
		// Parse header.
		super.parse(received);

		// Ensure body is at least an additional 20 bytes, to the headers 8
		// bytes.
		if(!(received.length > (7 + 24))){
			LOGGER.log(Level.SEVERE, "The RTCP packet body was only " + received.length + " bytes long.");
			throw new IllegalArgumentException();
		}
		
		// Get SSRC Id.
		ssrcId = bytesToInt(Arrays.copyOfRange(received, 8, 12));
		
		//TODO
		// Just extract and store as a long -  without any interpretation.
		fractionLost = received[12];
		packetsLost = bytesToInt(Arrays.copyOfRange(received, 13, 16));
		highestReceived = bytesToInt(Arrays.copyOfRange(received, 16, 20));
		jitter = bytesToInt(Arrays.copyOfRange(received, 20, 24));
		lastSenderReport = bytesToInt(Arrays.copyOfRange(received, 24, 28));
		delayLastSenderReport = bytesToInt(Arrays.copyOfRange(received, 28, 32));
		LOGGER.log(Level.SEVERE, "Received a Receiver Report RTCP packet "
				+ DatatypeConverter.printHexBinary(Arrays.copyOfRange(received, 20, 32))
				+ " - this functionality needs to be implemented.");
	}

	protected long getSsrcId() {
		return ssrcId;
	}

	protected int getFractionLost() {
		return fractionLost;
	}

	protected long getPacketsLost() {
		return packetsLost;
	}

	protected long getHighestReceived() {
		return highestReceived;
	}

	protected long getJitter() {
		return jitter;
	}

	protected long getLastSenderReport() {
		return lastSenderReport;
	}

	protected long getDelayLastSenderReport() {
		return delayLastSenderReport;
	}
	
	@Override
	public String toString() {
		return "ReceiverReport ["
				+ ("Not Implemented")
				+ "]";
	}	
}
