/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtp.client.message;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.DatatypeConverter;

/**
 * <p>RTCP Source Description packet for describing the source.</p> 
 * 
 * <h2>RFC 3550 - RTP: A Transport Protocol for Real-Time Applications</h2>
 *
 * <h3>6.5 SDES: Source Description RTCP Packet</h3>
 * 
 * <p>Each chunk consists of an SSRC/CSRC identifier followed by a list of zero or
 * more items, which carry information about the SSRC/CSRC. Each chunk starts
 * on a 32-bit boundary.</p>
 * 
 * <p>Valid items are:-</p>
 * <pre>
 *      name                                                value
 *      CNAME: Canonical End-Point Identifier SDES Item     1
 *      NAME: User Name SDES Item                           2
 *      EMAIL: Electronic Mail Address SDES Item            3
 *      PHONE: Phone Number SDES Item                       4
 *      LOC: Geographic User Location SDES Item             5
 *      TOOL: Application or Tool Name SDES Item            6
 *      NOTE: Notice/Status SDES Item                       7
 *      PRIV: Private Extensions SDES Item                  8
 * </pre>
 * <p>The general format for items is:-</p>
 * <pre>
 *         0                   1                   2                   3
 *         0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *        |  Item value  |    length     |      Item properties...
 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * </pre>
 * 
 * <pre>
 *         0                   1                   2                   3
 *         0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * header |V=2|P|   RC   |  PT=SDES=202  |             length             |
 *        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
 * chunk  |                        SSRC of sender                         |
 *   1    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *        |                          SDES items                           |
 *        |                              ...                              |
 *        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
 *                               Additional chunks...
 * </pre>
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class SourceDescription extends RtcpMessage {

	private static final Logger LOGGER = Logger.getLogger(SourceDescription.class.getName());
	
	protected byte[] items = null;

	/**
	 * A temporary implementation...
	 * 
	 * Parse the body.
	 * 
	 * <pre>
	 *         0                   1                   2                   3
	 *         0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
	 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	 * header |V=2|P|   RC   |  PT=SDES=202  |             length             |
	 *        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
	 * chunk  |                        SSRC of sender                         |
	 *   1    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	 *        |                          SDES items                           |
	 *        |                              ...                              |
	 *        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
	 *                               Additional chunks...
	 * </pre>
	 * 
	 * @param received - the bytes received in the RTCP packet.
	 * @throws IllegalArgumentException - if the length of the report is less
	 * than 2 bytes.
	 * 
	 */
	@Override
	public void parse(byte[] received) throws IllegalArgumentException {

		// Parse header.
		super.parse(received);

		// Ensure body is at least an additional 2 bytes, to the headers 8
		// bytes.
		if (!(received.length > (7 + 2))) {
			LOGGER.log(Level.SEVERE, "The RTCP packet body was only "
					+ received.length + " bytes long.");
			throw new IllegalArgumentException();
		}
		
		//TODO
		//Just store the bytes for now...
		items = Arrays.copyOfRange(received, 12, received.length);
		LOGGER.log(Level.SEVERE, "Received a Source Description RTCP packet "
				+ DatatypeConverter.printHexBinary(items)
				+ " - this functionality needs to be implemented.");
	}
	
	@Override
	public String toString() {
		return "SourceDescription ["
				+ ("Not Implemented")
				+ "]";
	}	
}
