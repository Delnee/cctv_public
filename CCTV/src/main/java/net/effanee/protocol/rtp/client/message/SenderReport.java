/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtp.client.message;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>RTCP Sender Report packet for transmission and reception statistics from
 * participants that are active senders.</p>
 * 
 * <h2>RFC 3550 - RTP: A Transport Protocol for Real-Time Applications</h2>
 *
 * <h3>6.4.1 SR: Sender Report RTCP Packet</h3>
 * <pre>
 *        0                   1                   2                   3
 *        0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * header |V=2|P|   RC   |   PT=SR=200   |             length             |
 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *        |                         SSRC of sender                        |
 *        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
 * sender |             NTP timestamp, most significant word              |
 * info   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *        |             NTP timestamp, least significant word             |
 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *        |                        RTP timestamp                          |
 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *        |                    sender's packet count                      |
 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *        |                    sender's octet count                       |
 *        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
 *                    1 - 32 optional Reception Report blocks...
 * </pre>
 *
 * <p>Example:-</p>
 * 
 * <pre>
 * Real-time Transport Control Protocol (Sender Report)
 *    [Stream setup by RTSP (frame 158)]
 *    10.. .... = Version: RFC 1889 Version (2)
 *    ..0. .... = Padding: False
 *    ...0 0000 = Reception report count: 0
 *    Packet type: Sender Report (200)
 *    Length: 6 (28 bytes)
 *    Sender SSRC: 0x3d0f9727 (1024431911)
 *    Timestamp, MSW: 3664742736 (0xda6f8950)
 *    Timestamp, LSW: 3080982948 (0xb7a411a4)
 *    [MSW and LSW as NTP timestamp: Feb 18, 2016 00:05:36.717347000 UTC]
 *    RTP timestamp: 1698505754
 *    Sender's packet count: 4294964650
 *    Sender's octet count: 88807320
 * 
 *    80 c8 00 06 3d 0f 97 27 da 6f 89 50 b7 a4 11 a4  ....=..'.o.P....
 *    65 3d 24 1a ff ff f5 aa 05 4b 17 98              e=$......K..
 * </pre>
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class SenderReport extends RtcpMessage {

	private static final Logger LOGGER = Logger.getLogger(SenderReport.class.getName());

	// Body fields.
	protected long ntpSeconds = 0l;
	protected long ntpFractional = 0l;
	protected long rtpTimestamp = 0l;
	protected long sendersPacketCount = 0l;
	protected long sendersOctetCount = 0l;
	protected List<ReceiverReport> receptionReports = new ArrayList<>();

	/**
	 * Parse the body.
	 * 
	 * <pre>
	 * 		   0                   1                   2                   3
	 * 		   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
	 *        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
	 * sender |             NTP timestamp, most significant word              |
	 * info   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	 *        |             NTP timestamp, least significant word             |
	 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	 *        |                        RTP timestamp                          |
	 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	 *        |                    sender's packet count                      |
	 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	 *        |                    sender's octet count                       |
	 *        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
	 *                    1 - 32 optional Reception Report blocks...
	 *</pre>
	 * 
	 * @param received - the bytes received in the RTCP packet.
	 * @throws IllegalArgumentException - if the length of the report is less
	 * than 20 bytes.
	 * 
	 */
	@Override
	public void parse(byte[] received) throws IllegalArgumentException {

		// Parse header.
		super.parse(received);

		// Ensure body is at least an additional 20 bytes, to the headers 8
		// bytes.
		if (!(received.length > (7 + 20))) {
			LOGGER.log(Level.SEVERE, "The RTCP packet body was only "
					+ received.length + " bytes long.");
			throw new IllegalArgumentException();
		}

		// Get NTP seconds part.
		ntpSeconds = bytesToInt(Arrays.copyOfRange(received, 8, 12));

		// Get NTP fractions of a second part.
		ntpFractional = bytesToInt(Arrays.copyOfRange(received, 12, 16));

		// Get RTP timestamp.
		rtpTimestamp = bytesToInt(Arrays.copyOfRange(received, 16, 20));

		// Get senders packet count.
		sendersPacketCount = bytesToInt(Arrays.copyOfRange(received, 20, 24));

		// Get senders octet count.
		sendersOctetCount = bytesToInt(Arrays.copyOfRange(received, 24, 28));
		
		//TODO ...
		// Store reception reports.
//		if (getCount() > 0){
//			parseReceiverReports(received);
//		}
	}
	
	//this needs careful checking and testing!!!!!!!!!!
	@SuppressWarnings("unused")
	private void parseReceiverReports(byte[] received){
		
		int offset = 28;
		int startPosition = 0;
		byte[] header = Arrays.copyOfRange(received, 0, 8);
		byte[] body = null;
		for (int i = 0; i < getCount(); i++){
			
			// Get array bytes combining header and body.
			startPosition = offset + (24 * i);
			body = Arrays.copyOfRange(received, startPosition, startPosition + 24);
			byte[] reportBytes = Arrays.copyOf(header, header.length + body.length);
			System.arraycopy(body, 0, reportBytes, header.length, body.length);
			
			
			// Create and store receiver report.
			ReceiverReport report = new ReceiverReport();
			report.parse(reportBytes);
			receptionReports.add(report);
		}
	}
	
	protected long getNtpSeconds() {
		return ntpSeconds;
	}

	/**
	 * Get NTP seconds as a date.
	 * 
	 * @return the NTP seconds since 1 Jan. 1900 as a date.
	 */
	protected LocalDateTime getNtpSecondsAsDate() {

		// NTP Epoch start January 1, 1900. Seconds before Java Epoch start
		// January 1, 1970.
		return LocalDateTime.ofEpochSecond(ntpSeconds - NTP_EPOCH_ADJUSTMENT,
				0, ZoneOffset.UTC);
	}

	protected long getNtpFractional() {
		return ntpFractional;
	}

	/**
	 * Get the NTP fractional part as milliseconds.
	 * 
	 * @return the truncated milliseconds.
	 */
	protected long getNtpFractionalAsMilliSeconds() {

		// Get unsigned integer maximum, this is the range over which the
		// the fractional part is spread.
		long unsignedIntMax = (Integer.MAX_VALUE * 2l) + 1;
		// Convert to thousandths of a second.
		return (ntpFractional * 1000) / unsignedIntMax;
	}

	protected long getRtpTimestamp() {
		return rtpTimestamp;
	}

	protected long getSendersPacketCount() {
		return sendersPacketCount;
	}

	protected long getSendersOctetCount() {
		return sendersOctetCount;
	}

	@Override
	public String toString() {
		return "SenderReport ["
				+ ("super()=" + super.toString() + ",")
				+ ("ntpSeconds=" + ntpSeconds + " (" + getNtpSecondsAsDate() + "),")
				+ ("ntpFractional=" + ntpFractional + ",")
				+ ("rtpTimestamp=" + rtpTimestamp + ",")
				+ ("sendersPacketCount=" + sendersPacketCount + ",")
				+ ("sendersOctetCount=" + sendersOctetCount)
				+ "]";
	}	
}
