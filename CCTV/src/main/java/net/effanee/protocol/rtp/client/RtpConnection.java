/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtp.client;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.StandardProtocolFamily;
import java.net.StandardSocketOptions;
import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.effanee.buffer.RingBuffer;

/**
 * <p>An RTP connection to a remote host, via UDP, using the supplied URI and
 * ports.</p>
 * 
 * <p>Instances of this class are run in their own thread, and must be
 * terminated by calling the stop() method, which interrupts the thread and
 * closes the connection.</p>
 *
 * <p>RTP UDP packets contain a 16 bit unsigned sequence number, which increases
 * monotonically, in bytes 2 and 3 of the RTP header, (where the first byte is
 * numbered 0).</p>
 * 
 * <p>The sequence wraps to 0, when the maximum value - 65,535 - is reached.</p>
 * 
 * <p>Packets may be lost, and delivered out of order. Duplicate or out-dated
 * packets should be discarded.</p>
 * 
 * <p>The RTP specification does not include the ability for clients to request
 * that lost packets be re-sent.</p>
 * 
 * <p>On a LAN packet loss is around 0.02%, (socket buffer size is important here).
 * But, packets travelling across the Internet, would likely have a much higher
 * level of disorder.</p>
 * 
 * <p>The RTP packet H.264 payload is designed to be resilient to lost bytes.</p>
 * 
 * <p>The expected use case for this application is locally across a LAN, so packet
 * loss should be low. This, together with the resilience features of the H.264
 * packet structure, means that the anticipated packet loss should be low.</p>
 * 
 * <p>Samples from a HikVision DS-2CD2032-I, running H.264 at 25 FPS, across a
 * gigabit LAN:-</p>
 * <ul>
 * 		<li>450 packets a second.</li>
 * 		<li>25 NAL units a second.</li>
 * 		<li>600 KB a second.</li>
 * </ul>
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class RtpConnection  implements Runnable{

	private static final Logger LOGGER = Logger.getLogger(RtpConnection.class.getName());
	
	// Defaults.
	private static final int BUFFER_INCREMENT = 65535;
	private static final int PPS_INTERVAL = 5;
	
	private final URI uri;
	private final int clientPort;
	private final int serverPort;
	private final RingBuffer<?> buffer;
	private final boolean logPacketsPerSecond;
	private final AtomicLong packetCount = new AtomicLong();
	private final AtomicInteger packetsPerSecond = new AtomicInteger();
	private Timer timer = null; 
	
	private Thread thisThread;
	private SelectionKey key = null;
	
	/**
	 * Create a new connection.
	 * 
	 * @param uri - the remote host URI - only the IP address is used.
	 * @param clientPort - the local IP port.
	 * @param serverPort - the remote IP port.
	 * @param buffer - the buffer to contain the RTP payload bytes. 
	 */
	public RtpConnection(URI uri, int clientPort, int serverPort, RingBuffer<?> buffer, boolean logPacketsPerSecond){
		
		this.uri = uri;
		this.clientPort = clientPort;
		this.serverPort = serverPort;
		this.buffer = buffer;
		this.logPacketsPerSecond = logPacketsPerSecond;
		startTask();
		LOGGER.log(Level.FINEST, this.toString());
	}

	/**
	 * Start the periodic task that calculates the packets per second value.
	 */
	private void startTask(){
		
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.SECOND, PPS_INTERVAL);
		
		timer = new Timer(true);
        timer.schedule(new PacketsPerSecond(PPS_INTERVAL),
		        		calendar.getTime(),
		        		TimeUnit.SECONDS.toMillis(5));
	}
	
	/**
	 * Get the packets per second value, for the default interval of 5 seconds.
	 * 
	 * @return the packets per second value.
	 */
	int getPacketsPerSecond(){
		
		return packetsPerSecond.get();
	}
	
	/**
	 * Set the packets per second value, for the default interval of 5 seconds.
	 * Log the value, if configured to do so. 
	 * 
	 * @param latestMeasure - the latest value.
	 */
	private void setPacketsPerSecond(int latestMeasure){
		
		packetsPerSecond.set(latestMeasure);

		if(logPacketsPerSecond){
			LOGGER.log(Level.INFO, "Received " + getPacketsPerSecond()
					+ " packets per second, during the last " + PPS_INTERVAL
					+ " seconds.");
		}
	}
	
	/**
	 * Connect to the remote host and read packets until the stop() method is
	 * called.
	 */
	@Override
	public void run() {

		// At this point we can get a reference to the executing thread,
		// which can be used later to stop the thread.
		thisThread = Thread.currentThread();
		
		// Open a connection to the server.
		try {
			openConnection();
		}
		catch (IOException e) {
			return;
		}

		// Read UDP packets until the stop() method is called. Or, the buffer
		// is closed, and has caused the read method to exit.
		read();
		
		// Close the connection.
		closeConnection();
	}

	/**
	 * Open a non-blocking, read only, connection to the remote host.
	 * 
	 * @throws IOException 
	 */
	private void openConnection() throws IOException{
		
		try {
			// Create a UDP socket and bind to the local address.
			DatagramChannel channel = DatagramChannel.open(StandardProtocolFamily.INET)
					.setOption(StandardSocketOptions.SO_REUSEADDR, true)
					.setOption(StandardSocketOptions.SO_RCVBUF, BUFFER_INCREMENT * 10)
					.bind(new InetSocketAddress(getIpv4InterfaceAddress(), clientPort));
			
			// Non-blocking.
			channel.configureBlocking(false);
			
			// Bind to one remote address.
			SocketAddress remoteSocket = new InetSocketAddress(InetAddress.getByName(uri.getHost()), serverPort);
			channel.connect(remoteSocket);
			
			// Store the key and use it to access this connection.
			key = channel.register(Selector.open(), SelectionKey.OP_READ);
		}
		catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Could not open the DatagramChannel", e);
			throw e;
		}
		
		LOGGER.log(Level.FINEST, "Opened the DatagramChannel.");
	}
	
	/**
	 * Read the datagram packets, and write them to the buffer, until close()
	 * is invoked.
	 */
	private void read(){

		ByteBuffer byteBuffer = ByteBuffer.allocate(10);
		
		while (!Thread.currentThread().isInterrupted()
				&& byteBuffer != null) {

			try {
				// Only one key is associated with the channel, so we can skip
				// iterating over the selected keys set returned from:-
				// 		key.selector().selectedKeys().iterator();
                key.selector().select();
				byteBuffer = (ByteBuffer) buffer.takeNextProducerItem();
				if (byteBuffer == null){
					LOGGER.log(Level.INFO, "The thread expectedly stopped upon buffer closure.");
					continue;
				}
				((DatagramChannel) key.channel()).read(byteBuffer);
				byteBuffer.flip();
				buffer.returnLastProducerItem();
				packetCount.getAndIncrement();
			}
			catch (InterruptedException | ClosedByInterruptException e) {
				// Assume the exception is caused by the stop() method being
				// invoked and exit.
				LOGGER.log(Level.INFO, "The thread was expectedly interrupted.");
				break;
			}
			catch (IOException e) {
				LOGGER.log(Level.SEVERE, "The thread experienced an unexpected exception.", e);
				break;
			}
		}
	}

	/**
	 * Close this connection.
	 */
	private void closeConnection(){
		
		try {
			key.selector().close();
			key.channel().close();
			LOGGER.log(Level.FINEST, "Closed the DatagramChannel.");
		}
		catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Could not close the DatagramChannel.", e);
		}
	}
	
	/**
	 * Close this connection, by interrupting the NIO select() method. This method
	 * must be called to close and tidy a thread running an instance of this
	 * class. If the buffer has been previously closed, this thread may already
	 * be terminated. In which case, this method will have no effect. 
	 */
	public void stop(){
		// Will not exit while loop, without condition.
		//key.selector().wakeup();
		timer.cancel();
		thisThread.interrupt();
	}
	
	/**
	 * Get the IPv4 interface address for the first active, non-loop back, NIC.
	 * 
	 * @return the address, or null.
	 */
	protected InetAddress getIpv4InterfaceAddress(){

		InetAddress addr = null;
		try {
			// Loop the interface cards.
			Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
			while (interfaces.hasMoreElements()) {
				
				NetworkInterface networkInterface = interfaces.nextElement();
				if (!networkInterface.isLoopback() && networkInterface.isUp()) {
					
					// Loop an interfaces addresses.
					for (InterfaceAddress address : networkInterface.getInterfaceAddresses()) {
						addr = address.getAddress();
						if (addr != null && addr instanceof Inet4Address)
							LOGGER.log(Level.INFO, "Obtained the following local NIC IPv4 address "
									+ addr.getHostAddress()
									+ " with MTU of "
									+ networkInterface.getMTU());
							return addr;
					}
				}
			}
		}
		catch (SocketException e) {
			LOGGER.log(Level.SEVERE, "A network interface address could not be obtained for this device.", e);
		}
		return addr;
	}
	
	@Override
	public String toString() {
		return "RtpConnection ["
				+ ("uri=" + uri + ",")
				+ ("clientPort=" + clientPort + ",")
				+ ("serverPort=" + serverPort + ",")
				+ ("buffer=" + buffer)
				+ "]";
	}
	
	/**
	 * Run periodically to calculate the packets per second being received. The
	 * value is calculated by:-
	 * 
	 * 	(currentCount - lastCount) / interval
	 *
	 * The task calls the enclosing classes setPacketsPerSecond() method to set
	 * the result at each interval.
	 *
	 * @author Francis J. Hammell [hammell.francis@gmail.com]
	 * <p><em>Copyright</em><br />
	 * \u00A9 Francis J. Hammell 2016</p>
	 */
	private class PacketsPerSecond extends TimerTask{

		private long lastCount = 0;
		private final int interval;
		
		/**
		 * @param interval - the frequency the task should be run.
		 */
		private PacketsPerSecond(int interval){
			this.interval = interval;
		}
		
		/**
		 * Run periodically, calculate the packets per second and set the value
		 * in the enclosing class.
		 */
		public void run() {
			
			// Set the new Packets per seconds value for the specified interval.
			long currentCount = packetCount.get();
			setPacketsPerSecond((int)((currentCount - lastCount) / interval));
			lastCount = currentCount;
			
		}
	}
	// End PacketPerSecond.
}
