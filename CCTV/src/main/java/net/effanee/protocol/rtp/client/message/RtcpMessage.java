/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtp.client.message;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>An RTCP message. This class interprets the common header, while sub-classes
 * interpret the individual bodies.</p>
 * 
 * <h2>RFC 3550 - RTP: A Transport Protocol for Real-Time Applications</h2>
 * 
 * <h3>6. RTP Control Protocol | RTCP</h3>
 * 
 * <p>The RTP control protocol (RTCP) is based on the periodic transmission of
 * control packets to all participants in the session, using the same
 * distribution mechanism as the data packets. The underlying protocol must
 * provide multiplexing of the data and control packets, for example using
 * separate port numbers with UDP.</p>
 * 
 * <h3>12.1 RTCP Packet Types</h3>
 * <pre> 
 *      abbrev. name                value
 *      SR      sender report       200
 *      RR      receiver report     201
 *      SDES    source description  202
 *      BYE     goodbye             203
 *      APP     application-defined 204
 * </pre>
 * 
 * <p>Message header:-</p>
 * 
 * <pre>
 *         0                   1                   2                   3
 *         0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * header |V=2|P| Count  |  PT=200-204   |             length             |
 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *        |                     SSRC of packet sender                     |
 *        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
 * 
 * version (V), padding (P), length:
 * 		As described for the SR packet (see Section 6.4.1).
 * 
 * Padding (P): 1 bit
 * 		If the padding bit is set, this individual RTCP packet contains some
 * 		additional padding octets at the end which are not part of the control
 * 		information but are included in the length field.
 * 
 * source count (SC): 5 bits
 * 		The number of SSRC/CSRC identifiers included in this BYE packet.A count
 * 		value of zero is valid, but useless.
 * 
 * packet type (PT): 8 bits
 * 		Contains the constant 203 to identify this as an RTCP BYE packet.
 * 
 * length: 16 bits
 * 		The length of this RTCP packet in 32-bit words minus one, including the
 * 		header and any padding. (The offset of one makes zero a valid length and
 * 		avoids a possible infinite loop in scanning a compound RTCP packet,
 * 		while counting 32-bit words avoids a validity check for a multiple of 4.)
 * 
 * SSRC: 32 bits
 * 		The synchronization source identifier for the originator of this packet.
 * </pre>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public abstract class RtcpMessage {

	private static final Logger LOGGER = Logger.getLogger(RtcpMessage.class.getName());
	
	public static final long NTP_EPOCH_ADJUSTMENT = 2208988800l;
	
	protected static final byte VERSION = (byte) 0b00000010;

	// Header fields.
	protected boolean isPadded = false;
	protected int count = 0;
	protected RtcpPacket packetType;
	protected int length = 0;
	protected long ssrcId = 0l;

	/**
	 * Parse the header.
	 * 
	 * <pre>
	 *         0                   1                   2                   3
	 *         0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
	 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	 * header |V=2|P| Count  |  PT=200-204   |             length             |
	 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	 *        |                     SSRC of packet sender                     |
	 *        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
	 * </pre>
	 * 
	 * @param received - the bytes received in the RTCP packet.
	 * @throws IllegalArgumentException - if the length of the header is less
	 * than 7 bytes or the version number is not 2.
	 */
	public void parse(byte[] received) throws IllegalArgumentException{
		
		// Ensure header is at least 8 bytes.
		if(!(received.length > 7)){
			LOGGER.log(Level.SEVERE, "The RTCP packet header was only " + received.length + " bytes long.");
			throw new IllegalArgumentException();
		}
		
		// Get bits 0-1, version.
		if(getBits(received[0], 0, 1) != VERSION){
			LOGGER.log(Level.SEVERE, "The RTCP version was " + getBits(received[0], 0, 1)
					+ " and it should be " + VERSION + ".");
			throw new IllegalArgumentException();
		}
		
		// Get padding.
		isPadded = getBits(received[0], 2, 2) != 0;
		
		// Get count.
		count = getBits(received[0], 3, 7);
		
		// Get packet type.
		packetType = RtcpPacket.getRtcpPacket(received[1]);
		
		// Get length.
		length = ((received[2] & 0xFF) << 8) | ((received[3] & 0xFF) << 0);

		// Get SSRC Id.
		ssrcId = bytesToInt(Arrays.copyOfRange(received, 4, 8));
	}

	protected boolean isPadded(){
		
		return isPadded; 
	}
	
	protected int getCount(){
		
		return count;
	}

	protected RtcpPacket getRtcpPacket(){
		
		return packetType;
	}
	
	protected int getLength(){
		
		return length;
	}

	protected long getSsrcId(){
		
		return ssrcId;
	}
	
	/**
	 * Extract a contiguous range of bits from a byte, and return the result as
	 * a byte where the requested bits are right aligned in the new byte.
	 *   
	 * @param b - the byte containing the bits.
	 * @param startBit - the start position of the first contiguous bit, the left
	 * most bit is numbered 0, positions are inclusive.
	 * @param endBit - the start position of the last contiguous bit, the right
	 * most bit is numbered 7, positions are inclusive.
	 * @return the bits, right aligned in the new byte.
	 */
	protected byte getBits(byte b, int startBit, int endBit){
		
		int bits = 0;
		int mask = getBitMask((endBit - startBit) + 1);
		
		// Right align the bits we want.
		bits = (byte)(b >>> (7 - endBit));
		// Remove ones to the left of the bits we want.
		bits = (bits & mask);
		
		return (byte)bits;
	}
	
	/**
	 * Create a bit mask consisting of right aligned ones. This mask is intended
	 * to be used with 8 bit bytes, so the valid parameter values are 1 - 8. 
	 * 
	 * @param noOfBits - a value in the range 1 to 8.
	 * @return a bit mask with the requested number of contiguous ones in the
	 * right most positions.
	 */
	protected int getBitMask(int noOfBits){
		
		int mask = 0;
		for(int i = 0; i < noOfBits; i++){
			mask = (int) (mask + Math.pow(2, i));
		}
		
		return mask;
	}
	
	/**
	 * Convert a 4 byte array into an unsigned integer representation. 
	 * 
	 * @param bytes - the four bytes of the integer, with the most significant
	 * byte first. 
	 * @return the new integer.
	 */
	protected long bytesToInt(byte[] bytes){
		
		// Need to promote to long to get full range of 32 bits.
		long intValue = ((0xFFl & bytes[0]) << 24)
				| ((0xFFl & bytes[1]) << 16)
				| ((0xFFl & bytes[2]) << 8)
				| (0xFFl & bytes[3]);
		
		return intValue;
	}
	
	@Override
	public String toString() {
		return "RtcpMessage ["
				+ ("VERSION=" + VERSION + ",")
				+ ("isPadded=" + isPadded + ",")
				+ ("count=" + count + ",")
				+ ("packetType=" + packetType + ",")
				+ ("length=" + length + ",")
				+ ("ssrcId=" + ssrcId)
				+ "]";
	}	
}