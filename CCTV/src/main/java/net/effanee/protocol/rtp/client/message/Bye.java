/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtp.client.message;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.DatatypeConverter;

/**
 * <p>RTCP BYE packet indicating that one or more sources are no longer active.</p>
 * 
 * <h2>RFC 3550 - RTP: A Transport Protocol for Real-Time Applications</h2>
 *
 * <h3>6.6 BYE: Goodbye RTCP Packet</h3>
 * <pre>
 *         0                   1                   2                   3
 *         0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * header |V=2|P|   RC   |   PT=BYE=203   |            length             |
 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *        |                             SSRC                              |
 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *        |                              ...                              |
 *        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
 *  (opt) |    length    |           reason for leaving                 ...
 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * </pre>
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class Bye extends RtcpMessage {

	private static final Logger LOGGER = Logger.getLogger(Bye.class.getName());

	// Body fields.
	protected long ssrcId = 0l;
	protected long length = 0l;
	protected String reason = "";

	/**
	 * Parse the body.
	 * 
	 * <pre>
	 *         0                   1                   2                   3
	 *         0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
	 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	 * header |V=2|P|   RC   |   PT=BYE=203   |            length             |
	 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	 *        |                             SSRC                              |
	 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	 *        |                              ...                              |
	 *        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
	 *  (opt) |    length    |           reason for leaving                 ...
	 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	 * </pre>
	 * 
	 * @param received - the bytes received in the RTCP packet.
	 * @throws IllegalArgumentException - if the length of the report is less
	 * than 2 bytes.
	 * 
	 */
	@Override
	public void parse(byte[] received) throws IllegalArgumentException {

		// Parse header.
		super.parse(received);

		// Ensure body is at least an additional 2 bytes, to the headers 8
		// bytes.
		if (!(received.length > (7 + 2))) {
			LOGGER.log(Level.SEVERE, "The RTCP packet body was only "
					+ received.length + " bytes long.");
			throw new IllegalArgumentException();
		}

		// Get SSRC Id.
		ssrcId = bytesToInt(Arrays.copyOfRange(received, 8, 12));
		
		//TODO
		// Just extract and store - without any interpretation.
		length = received[12];
		
		reason = new String(Arrays.copyOfRange(received, 13, (int)(13 + length)), StandardCharsets.UTF_8);
		
		LOGGER.log(Level.SEVERE, "Received a Bye RTCP packet "
				+ DatatypeConverter.printHexBinary(Arrays.copyOfRange(received, 20, 32))
				+ " - this functionality needs to be implemented.");
	}

	@Override
	public String toString() {
		return "Bye ["
				+ ("Not Implemented")
				+ "]";
	}
}
