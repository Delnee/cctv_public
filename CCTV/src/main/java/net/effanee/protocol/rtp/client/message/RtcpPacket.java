/**
 * This file is part of CCTV.
 *
 * CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.effanee.protocol.rtp.client.message;

/**
 * <p>RTCP header field keys.</p>
 * 
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public enum RtcpPacket {
	
	/* 		abbrev. name 				value	Java byte value
	 * 		SR 		sender report 		200		-56
	 * 		RR 		receiver report 	201		-55
	 * 		SDES 	source description 	202		-54
	 * 		BYE 	goodbye 			203		-53
	 *  	APP 	application-defined	204		-52
	 */
	 
	SENDER_REPORT((byte) 0b11001000, "Sender Report"),
	RECEIVER_REPORT((byte) 0b11001001, "Receiver Report"),
	SOURCE_DESCRIPTION((byte) 0b11001010, "Source Description"),
	BYE((byte) 0b11001011, "Goodbye"),
	APP((byte) 0b11001100, "Application Specific");

	private final byte packetType;
	private final String description;

	private RtcpPacket( byte packetType, String description) {
		this.packetType = packetType;
		this.description = description;
	}
	
	public byte packetType() {
		return packetType;
	}
	
	public String description(){
		return description;
	}
	
	/**
	 * Get a RtcpPacket when given its byte value.
	 * 
	 * Note:- this is the byte value as it is read from the RTCP packet, since
	 * Java bytes wrap, the decimal value of the packet as described in the
	 * RTCP specification will not equal this Java byte value. i.e. the RTCP
	 * Sender Report has a packet value of 200, but Java will represent this as
	 * -56 when stored in a byte. It is the bit representation of this byte
	 * that is used. 
	 * 
	 * @param b - the packet byte value.
	 * @return the corresponding RtcpPacket, or null if the supplied byte
	 * cannot be matched.
	 */
	public static RtcpPacket getRtcpPacket(byte b){
		
		for (RtcpPacket packet: RtcpPacket.values()){
			
			if (packet.packetType == b){
				return packet;
			}
		}
		return null;
	}
	
	// Display the packet type as per the specification.
	@Override
	public String toString() {
		return "RtcpPacket ["
				+ ("enum=" + this.name() + ", ")
				+ ("packetType=" + (0xFF & packetType) + ", ")
				+ ("description=" + description)
				+ "]";
	}
}
