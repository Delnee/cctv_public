/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtp.client;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.effanee.buffer.RingBuffer;

/**
 * <p>Read the RTP bytes from the buffer. Write a minutes worth of data to a series
 * of files. Notify any listeners when a new file has been written. The files
 * are stored in the root of the output directory. A minutes worth of data is
 * calculated by the formula:- packets per second * 60.</p>
 * 
 * <p>The file format:-</p>
 * <ul>
 * 	<li>4 byte integer - giving the length of the following RTP packet data</li>
 * </ul>
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class RtpFileWriter implements Runnable{

	private static final Logger LOGGER = Logger.getLogger(RtpFileWriter.class.getName());
	
	private static final String FILE_EXTENSION = ".rtp";
	
	private final RingBuffer<?> buffer;
	private final Path path;
	private final int packetsPerSecond;
	private final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH-mm-ss");
	
	private FileChannel fileChannel;
	private boolean isExiting = false;
	private List<RtpFileWriterListener> listeners = Collections.synchronizedList(new LinkedList<>());
	
	/**
	 * Create a new file writer.
	 * 
	 * @param buffer - the buffer containing the RTP payload bytes. 
	 * @param path - the directory where the bytes will be written.
	 * @param packetsPerSecond - used to calculate a minutes worth of data.
	 */
	public RtpFileWriter(RingBuffer<?> buffer, Path path, int packetsPerSecond){
		
		this.buffer = buffer;
		this.path = path;
		this.packetsPerSecond = packetsPerSecond;
	}

	/**
	 * Register to be notified when a new RTP file has been written.
	 * 
	 * @param listener - the listener to be notified.
	 */
	public void register(RtpFileWriterListener listener) {
		
		listeners.add(listener);
	}
	
	/**
	 * Unregister to be notified when a new RTP file has been written.
	 * 
	 * @param listener - the listener to unregister.
	 */
	public void unregister(RtpFileWriterListener listener) {
		
		listeners.remove(listener);
	}
	
	/**
	 * Notify listeners.
	 * 
	 * @param path - the path of the new file.
	 */
	private void notify(Path path){
		
		for(RtpFileWriterListener listener: listeners){
			
			listener.newFile(path);
		}
	}
	
	/**
	 * Read bytes from the buffer until the buffer is closed.
	 */
	@Override
	public void run() {

		while (!isExiting){
			
			Path filePath = FileSystems.getDefault().getPath(path.toString(), nextFileName());
			
			// Open the file.
			try {
				openWriter(filePath);
			}
			catch (IOException e) {
				LOGGER.log(Level.SEVERE, "Could not open a temporary file for the RTP bytes.", e);
				return;
			}
	
			// Read the buffer until it returns null.
			write();
			
			// Close the file.
			closeWriter();
			
			// Notify listeners.
			notify(filePath);
		}
		LOGGER.log(Level.INFO, "Stopped writing RTP packets.");
	}

	/**
	 * Get a new file name based on the current time, i.e.
	 * 
	 * 	2016-02-15T23-54-12.h264
	 * 
	 * @return the file name.
	 */
	private String nextFileName(){
	
		return dateFormatter.format(new Date()) + FILE_EXTENSION;
	}
	
	/**
	 * Open a non-blocking, write only, file.
	 * 
	 * @throws IOException 
	 */
	private void openWriter(Path filePath) throws IOException{

		try {
			fileChannel = FileChannel.open(filePath,
											EnumSet.of(StandardOpenOption.CREATE,
											StandardOpenOption.WRITE));
			LOGGER.log(Level.FINEST, "Opened the File Channel.");
		}
		catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Could not open a temporary file for the RTP bytes.", e);
			throw e;
		}
	}

	/**
	 * Read the buffer items, and write them to the file, until the buffer is
	 * closed. Uses the rtp.client.stream1.packetsPerSecond configuration
	 * setting to calculate a minutes worth of data for each file:- 
	 * 
	 * 	(packets per second * 60) == Each individual file 
	 */
	private void write(){

		ByteBuffer byteBuffer = ByteBuffer.allocate(0);
		ByteBuffer byteBufferLength = ByteBuffer.allocate(4);
		int packetCount = 0;

		// Loop until the buffer is closed or a minute has pasted.
		while ((byteBuffer != null) & (packetCount < (packetsPerSecond * 60))) {

			try {
				// Get buffer reference.
				byteBuffer = (ByteBuffer) buffer.takeNextConsumerItem();
				
				// Exit if the buffer has been closed.
				if (byteBuffer == null){
					LOGGER.log(Level.INFO, "The thread expectedly stopped upon buffer closure.");
					isExiting = true;
					continue;
				}
				
				// Write buffer length.
				byteBufferLength.putInt(byteBuffer.remaining());
				byteBufferLength.flip();
				fileChannel.write(byteBufferLength);
				byteBufferLength.clear();
				
				// Write buffer.
				fileChannel.write(byteBuffer);
				byteBuffer.clear();
				
				// Increment the count used to split the buffer contents
				// between files.
				packetCount++;
			}
			catch (InterruptedException | IOException e) {
				LOGGER.log(Level.SEVERE, "Could not write the temporary file for the RTP bytes.", e);
				break;
			}			
		}		
	}

	/**
	 * Close the file.
	 */
	private void closeWriter(){
		
		try {
			fileChannel.force(false);
			fileChannel.close();
			LOGGER.log(Level.FINEST, "Closed the File Channel.");
		}
		catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Could not close the temporary file for the RTP bytes.", e);
		}
	}
	
	@Override
	public String toString() {
		return "RtpWriter ["
				+ ("buffer=" + buffer + ",")
				+ ("path=" + path)
				+ "]";
	}	
}
