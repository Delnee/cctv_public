/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * ("at your option") any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtp.client.message;

/**
 * <p>RTCP packet types.</p> 
 * 
 * <h2>RFC 3550 - RTP: A Transport Protocol for Real-Time Applications</h2>
 *
 * <h3>6.1 RTCP Packet Format</h3>
 *
 * <p>This specification defines several RTCP packet types to carry a variety of
 * control information:</p>
 * <ul>
 *	<li>SR: Sender report, for transmission and reception statistics from
 *	participants that are active senders.</li>
 *
 *	<li>RR: Receiver report, for reception statistics from participants that are not
 *	active senders and in combination with SR for active senders reporting on
 *	more than 31 sources.</li>
 *
 *	<li>SDES: Source description items, including CNAME.</li>
 *
 *	<li>BYE: Indicates end of participation.</li>
 *
 *	<li>APP: Application-specific functions.</li>
 * </ul>
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public enum PacketType {
	
	SR ("Sender Report"),
	RR ("Receiver Report"),
	SDES ("Source Description"),
	BYE ("End"),
	APP ("Application Specific");

	private final String fieldName;
	
	PacketType(String fieldName) {
        this.fieldName = fieldName;
    }
	
	public String fieldName(){
		return fieldName;
	}
	
	@Override
	public String toString() {
		return PacketType.class.getSimpleName()
				+ "["
				+ ("fieldName=" + fieldName)
				+ "]";
	}
}
