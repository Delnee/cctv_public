/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.buffer;

import static org.junit.Assert.*;

import java.nio.ByteBuffer;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

import org.junit.Test;

/**
 * Test buffer.
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class RingBufferTest {

	/**
	 * Test modulus for wrapping the index. 
	 */
	@Test
	public void testModulus() {
		
		int index = 0;
		int size = 100;
		
		index = 0;
		assertEquals(0, ((index % size) + size) % size);
		
		index = 100;
		assertEquals(0, ((index % size) + size) % size);
		
		index = 1;
		assertEquals(1, ((index % size) + size) % size);

		// Does not wrap.
		index = Integer.MAX_VALUE;
		assertEquals(47, ((index % size) + size) % size);		
		
		index = Integer.MAX_VALUE + 1;
		assertEquals(52, ((index % size) + size) % size);

		index = Integer.MIN_VALUE;
		assertEquals(52, ((index % size) + size) % size);
	
		index = Integer.MIN_VALUE - 1;
		assertEquals(47, ((index % size) + size) % size);
	}
	
	/**
	 * Test constructor/generator.
	 * 
	 * Test method for {@link net.effanee.buffer.RingBuffer#RingBuffer(int, Supplier)}.
	 */
	@Test
	public void testConstructor() {
		
		Supplier<ByteBuffer> generator  = () -> ByteBuffer.allocate(20);
		RingBuffer<ByteBuffer> buffer = new RingBuffer<>(100, generator);

		assertEquals(100, buffer.getSize());
		
		Object[] internalBuffer =  buffer.getBuffer();
		for(int i = 0; i < internalBuffer.length; i++){
			
			assertTrue(internalBuffer[i] instanceof ByteBuffer);
		}
	}

	/**
	 * Fill the buffer.
	 * 
	 * Test method for {@link net.effanee.buffer.RingBuffer#takeNextProducerItem()}.
	 * @throws InterruptedException 
	 */
	@Test
	public void testNextProducerItem1() throws InterruptedException {
		
		final int bufferSize = 100;
		
		Supplier<Integer> generator  = () -> new Integer(1);
		RingBuffer<Integer> buffer = new RingBuffer<>(bufferSize, generator);

		assertEquals(bufferSize, buffer.getSize());
		
		// 1 - 99.
		for(int i = 1; i < bufferSize; i++){
			buffer.takeNextProducerItem();
			buffer.returnLastProducerItem();
		}
		
		assertEquals(bufferSize - 1, buffer.getTail());
	}	

	/**
	 * Fill the buffer, remove 20, and add 20.
	 * 
	 * Test method for {@link net.effanee.buffer.RingBuffer#takeNextProducerItem()}.
	 * @throws InterruptedException 
	 */
	@Test
	public void testNextProducerItem2() throws InterruptedException {
		
		final int bufferSize = 100;
		
		Supplier<Integer> generator  = () -> new Integer(1);
		RingBuffer<Integer> buffer = new RingBuffer<>(bufferSize, generator);

		assertEquals(bufferSize, buffer.getSize());

		// Fill 1 - 99.
		for(int i = 1; i < bufferSize; i++){
			buffer.takeNextProducerItem();
			buffer.returnLastProducerItem();
		}	
	
		// Remove 1 - 20.
		for(int i = 0; i < 20; i++){
			buffer.takeNextConsumerItem();
		}

		// Fill 0 - 19
		for(int i = 0; i < 20; i++){
			buffer.takeNextProducerItem();
			buffer.returnLastProducerItem();
		}		
		
		assertEquals(20, buffer.getHead());
		assertEquals(19, buffer.getTail());
	}

	/**
	 * Fill the buffer, causing a blocking wait.
	 * 
	 * Test method for {@link net.effanee.buffer.RingBuffer#takeNextProducerItem()}.
	 * @throws InterruptedException 
	 */
	@Test
	public void testNextProducerItem3() throws InterruptedException{
		
		final int bufferSize = 100;
		final int interations = 100;
		
		Supplier<ByteBuffer> generator  = () -> ByteBuffer.allocate(20);
		RingBuffer<ByteBuffer> buffer = new RingBuffer<>(bufferSize, generator);

		assertEquals(bufferSize, buffer.getSize());
		
		// The number of iterations should cause the buffer to block in a
		// wait.
        Thread producerThread = new Thread(new Producer(buffer, interations));
        producerThread.start();		
		producerThread.join(100);
		
		assertEquals(Thread.State.WAITING, producerThread.getState());
		
		// Wake up and close the thread, by raising an exception, which is
		// consumed by the thread.
		producerThread.interrupt();
	}	

	/**
	 * Ensure Consumer fully reads buffer.
	 * 
	 * Test method for {@link net.effanee.buffer.RingBuffer#flush()}.
	 * @throws InterruptedException 
	 */
	@Test
	public void testFlush1() throws InterruptedException {

		final int bufferSize = 1000;
		final int interations = 999;
		
		Supplier<ByteBuffer> generator  = () -> ByteBuffer.allocate(10);
		RingBuffer<ByteBuffer> buffer = new RingBuffer<>(bufferSize, generator);

		assertEquals(bufferSize, buffer.getSize());
		
		// Start the producer thread and wait for the buffer to fill.
        Thread producerThread = new Thread(new Producer(buffer, interations));
        producerThread.start();
        producerThread.join(100);
        
        // Flush the buffer.
        buffer.flush();
        
        // Start the consumer thread and wait for the buffer to empty.
        Thread consumerThread = new Thread(new Consumer(buffer, interations));
        consumerThread.start();
        consumerThread.join(100);
        
        // Expected.
		assertEquals(interations, buffer.getHead());
		assertEquals(interations, buffer.getTail());
		assertEquals(Thread.State.TERMINATED, producerThread.getState());
		assertEquals(Thread.State.TERMINATED, consumerThread.getState());
	}

	/**
	 * Test that the buffer flushes on every 10th item.
	 * 
	 * Test method for {@link net.effanee.buffer.RingBuffer#flush()}.
	 * @throws InterruptedException 
	 */
	@Test
	public void testFlush2() throws InterruptedException {
		
		final int bufferSize = 100;
		final int interations = 5000;
		
		final CountDownLatch doneSignal = new CountDownLatch(2);
		
		Supplier<AtomicInteger> generator  = () -> new AtomicInteger();
		RingBuffer<AtomicInteger> buffer = new RingBuffer<>(bufferSize, generator);

		assertEquals(bufferSize, buffer.getSize());

		// Consumer thread. Check each item has been incremented by one.
		new Thread(() -> {

			int next = 0;
			
			for(int i = 0; i < interations; i++){
				try {
					// Lock the buffer so that the enclosed assert will return
					// the correct values.
					synchronized (buffer) {
						AtomicInteger read = (AtomicInteger)buffer.takeNextConsumerItem();
						++next;
						if (next != read.intValue()){
							throw new IllegalArgumentException("Integers do not increment correctly. Expected " + next + " got " + read);
						}
						// The producer flushes on every 10th item, so the
						// consumer should be synchronised on each 10th item.
						if ((buffer.getHead() % 10) == 0){
							assertEquals(buffer.getHead(), buffer.getTail());
						}
					}
				}
				catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			doneSignal.countDown();
	    }).start();
		
		// Producer thread. Increment each item by one.
		new Thread(() -> {

			int next = 0;
			
			for(int i = 0; i < interations / 10; i++){
				for(int j = 0; j < 10; j++){
					try {
						AtomicInteger read = (AtomicInteger)buffer.takeNextProducerItem();
						read.set(++next);
						buffer.returnLastProducerItem();
					}
					catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				// Flush on every 10th item.
				buffer.flush();
			}
			doneSignal.countDown();
	    }).start();
		
		// Wait for threads to complete.
		doneSignal.await();
		
        // Producer and consumer are multiples of buffer size.
		assertEquals(0, buffer.getHead());
		assertEquals(0, buffer.getTail());
	}

	/**
	 * Test buffer exits on close, by returning null.
	 * 
	 * Test method for {@link net.effanee.buffer.RingBuffer#close()}.
	 * @throws InterruptedException 
	 */
	@Test
	public void testClose() throws InterruptedException {
		
		final int bufferSize = 100;
		final int interations = 5000;
		
		final CountDownLatch doneSignal = new CountDownLatch(2);
		
		Supplier<AtomicInteger> generator  = () -> new AtomicInteger();
		RingBuffer<AtomicInteger> buffer = new RingBuffer<>(bufferSize, generator);

		assertEquals(bufferSize, buffer.getSize());

		// Consumer thread. Consume items until null is returned.
		new Thread(() -> {

			AtomicInteger read = new AtomicInteger();
			
			while (read != null){
				try {
						read = (AtomicInteger)buffer.takeNextConsumerItem();
						//if (read != null){
						//	System.out.println("Consumer " + read.get());
						//}
				}
				catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			doneSignal.countDown();
	    }).start();
		
		// Producer thread. Produce items, close buffer and read final null.
		new Thread(() -> {

			AtomicInteger read = new AtomicInteger();
			int next = 0;
			
			while (read != null){
				for(int i = 0; i < interations; i++){
					try {
						read = (AtomicInteger)buffer.takeNextProducerItem();
						if (read != null){
							read.set(++next);
							//System.out.println("Producer " + next);
						}
						buffer.returnLastProducerItem();
					}
					catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				buffer.close();
				try {
					read = (AtomicInteger)buffer.takeNextProducerItem();
					//buffer.returnLastProducerItem();
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
			doneSignal.countDown();
	    }).start();
		
		// Wait for threads to complete.
		doneSignal.await();

		assertEquals(0, buffer.getTail());
		assertEquals(0, buffer.getHead());
	}	
	
	/**
	 * Read an empty buffer, causing a blocking wait.
	 * 
	 * Test method for {@link net.effanee.buffer.RingBuffer#takeNextConsumerItem()}.
	 * @throws InterruptedException 
	 */
	@Test
	public void testNextConsumerItem1() throws InterruptedException{
		
		final int bufferSize = 100;
		final int interations = 1;
		
		Supplier<ByteBuffer> generator  = () -> ByteBuffer.allocate(200);
		RingBuffer<ByteBuffer> buffer = new RingBuffer<>(bufferSize, generator);

		assertEquals(bufferSize, buffer.getSize());
		
		// The number of iterations should cause the buffer to block in a
		// wait.
        Thread consumerThread = new Thread(new Consumer(buffer, interations));
        consumerThread.start();		
		consumerThread.join(100);
		
		assertEquals(Thread.State.WAITING, consumerThread.getState());
		
		// Wake up and close the thread, by raising an exception, which is
		// consumed by the thread.
		consumerThread.interrupt();
	}		
	
	/**
	 * Test concurrent access. Just read and write items.
	 * 
	 * Test method for {@link net.effanee.buffer.RingBuffer}.
	 * @throws InterruptedException 
	 */
	@Test
	public void testConcurrentAccess1() throws InterruptedException {
		
		final int bufferSize = 100;
		final int interations = 50000;
		
		Supplier<ByteBuffer> generator  = () -> ByteBuffer.allocate(200);
		RingBuffer<ByteBuffer> buffer = new RingBuffer<>(bufferSize, generator);

		assertEquals(bufferSize, buffer.getSize());
		
        Thread consumerThread = new Thread(new Consumer(buffer, interations));
        consumerThread.start();
        Thread producerThread = new Thread(new Producer(buffer, interations));
        producerThread.start();		
		
        consumerThread.join();
        producerThread.join();
		
        // Producer and consumer are multiples of buffer size.
		assertEquals(0, buffer.getHead());
		assertEquals(0, buffer.getTail());
	}	

	/**
	 * Test buffer contents. Create AtomicIntegers and read/compare them.
	 * 
	 * Test method for {@link net.effanee.buffer.RingBuffer}.
	 * @throws InterruptedException 
	 */
	@Test
	public void testConcurrentAccess2() throws InterruptedException {
		
		final int bufferSize = 100;
		final int interations = 500;
		
		final CountDownLatch doneSignal = new CountDownLatch(2);
		
		Supplier<AtomicInteger> generator  = () -> new AtomicInteger();
		RingBuffer<AtomicInteger> buffer = new RingBuffer<>(bufferSize, generator);

		assertEquals(bufferSize, buffer.getSize());

		// Consumer thread. Check each item has been incremented by one.
		new Thread(() -> {

			int next = 0;
			
			for(int i = 0; i < interations; i++){
				try {
						AtomicInteger read = (AtomicInteger)buffer.takeNextConsumerItem();
						++next;
						if (next != read.intValue()){
							throw new IllegalArgumentException("Integers do not increment correctly. Expected " + next + " got " + read);
						}
				}
				catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			doneSignal.countDown();
	    }).start();
		
		// Producer thread. Increment each item by one.
		new Thread(() -> {

			int next = 0;
			
			for(int i = 0; i < interations; i++){
				try {
						AtomicInteger read = (AtomicInteger)buffer.takeNextProducerItem();
						read.set(++next);
						buffer.returnLastProducerItem();
				}
				catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			doneSignal.countDown();
	    }).start();
		
		// Wait for threads to complete.
		doneSignal.await();
		
        // Producer and consumer are multiples of buffer size.
		assertEquals(0, buffer.getHead());
		assertEquals(0, buffer.getTail());
	}

	/**
	 * Test concurrent access. Write an integer to the buffer and read/compare.
	 * 
	 * Test method for {@link net.effanee.buffer.RingBuffer}.
	 * @throws InterruptedException 
	 */
	@Test
	public void testConcurrentAccess3() throws InterruptedException {
		
		final int bufferSize = 1000;
		final int interations = 5000000;
		
		Supplier<ByteBuffer> generator  = () -> ByteBuffer.allocate(10);
		RingBuffer<ByteBuffer> buffer = new RingBuffer<>(bufferSize, generator);

		assertEquals(bufferSize, buffer.getSize());
		
		//long startTime = System.currentTimeMillis();
		
        Thread consumerThread = new Thread(new ConsumerInOrder(buffer, interations));
        consumerThread.start();
        Thread producerThread = new Thread(new ProducerInOrder(buffer, interations));
        producerThread.start();		
		
        consumerThread.join();
        producerThread.join();
		
        //long endTime = System.currentTimeMillis();
        
        // Producer and consumer are multiples of buffer size.
		assertEquals(0, buffer.getHead());
		assertEquals(0, buffer.getTail());
		
		//System.out.println("Took " + (endTime - startTime) + " milliseconds.");
	}
	
	/**
	 * Consumes items.
	 *
	 * @author Francis J. Hammell [hammell.francis@gmail.com]
	 * <p><em>Copyright</em>
	 * \u00A9 Francis J. Hammell 2016</p>
	 */
	private final class Consumer implements Runnable {

		private final RingBuffer<?> buffer;
		private final int consume;
		
		public Consumer(RingBuffer<?> buffer, int consume) {
			this.buffer = buffer;
			this.consume = consume;
		}

		/**
		 */
		@Override
		public void run() {

			for(int i = 0; i < consume; i++){
				try {
					buffer.takeNextConsumerItem();
				}
				catch (InterruptedException e) {
					//e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Produces items.
	 *
	 * @author Francis J. Hammell [hammell.francis@gmail.com]
	 * <p><em>Copyright</em>
	 * \u00A9 Francis J. Hammell 2016</p>
	 */	
	private final class Producer implements Runnable {

		private final RingBuffer<?> buffer;
		private final int produce;
		
		public Producer(RingBuffer<?> buffer, int produce) {
			this.buffer = buffer;
			this.produce = produce;
		}

		/**
		 */
		@Override
		public void run() {

			for(int i = 0; i < produce; i++){
				try {
					buffer.takeNextProducerItem();
					buffer.returnLastProducerItem();
				}
				catch (InterruptedException e) {
					//e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Consumes items. Reads an integer from the start of the buffer and
	 * checks it has the correct value. 
	 *
	 * @author Francis J. Hammell [hammell.francis@gmail.com]
	 * <p><em>Copyright</em>
	 * \u00A9 Francis J. Hammell 2016</p>
	 */
	private final class ConsumerInOrder implements Runnable {

		private final RingBuffer<?> buffer;
		private final int consume;
		
		public ConsumerInOrder(RingBuffer<?> buffer, int consume) {
			this.buffer = buffer;
			this.consume = consume;
		}

		/**
		 */
		@Override
		public void run() {
			
			int nextInt = 0;
			int readInt = 0;
			ByteBuffer byteBuffer = null;
			
			for(int i = 0; i < consume; i++){
				try {
					byteBuffer = (ByteBuffer)buffer.takeNextConsumerItem();
					readInt = byteBuffer.getInt();
					//System.out.println("Read " + readInt);
					byteBuffer.clear();
					if (++nextInt != readInt){
						throw new IllegalArgumentException("Integers do not increment correctly. Expected " + nextInt + " got " + readInt);
					}
				}
				catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			//System.out.println(readInt);
		}
	}
	
	/**
	 * Produces items. Puts an integer at the start of the buffer. 
	 *
	 * @author Francis J. Hammell [hammell.francis@gmail.com]
	 * <p><em>Copyright</em>
	 * \u00A9 Francis J. Hammell 2016</p>
	 */	
	private final class ProducerInOrder implements Runnable {

		private final RingBuffer<?> buffer;
		private final int produce;
		
		public ProducerInOrder(RingBuffer<?> buffer, int produce) {
			this.buffer = buffer;
			this.produce = produce;
		}

		/**
		 */
		@Override
		public void run() {
			
			int nextInt = 1;
			ByteBuffer byteBuffer = null;

			for(int i = 0; i < produce; i++){
				try {
					byteBuffer = (ByteBuffer)buffer.takeNextProducerItem();
					//System.out.println("Put " + nextInt);
					byteBuffer.putInt(nextInt++);
					byteBuffer.flip();
					buffer.returnLastProducerItem();
				}
				catch (InterruptedException e) {
					e.printStackTrace();
				}
				finally {
					
				}
			}
			//System.out.println(nextInt);
		}
	}
}
