/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee;

import static org.junit.Assert.assertEquals;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

import org.junit.Test;

/**
 * Tests.
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class ConfigurationTest {

	/**
	 * Test method for {@link net.effanee.Configuration#getFriendlyName()}.
	 */
	@Test
	public void testGetFriendlyName() {
		
		Configuration.setFileLocation("src\\test\\resources\\cctv.properties");
		assertEquals("Friendly", Configuration.getFriendlyName());
	}
	
	/**
	 * Test method for {@link net.effanee.Configuration#getRtpClientPort()}.
	 */
	@Test
	public void testGetRtpClientPort() {
		
		Configuration.setFileLocation("src\\test\\resources\\cctv.properties");
		assertEquals(58844, Configuration.getRtpClientPort());
	}	

	/**
	 * Test method for {@link net.effanee.Configuration#getLogPacketsPerSecond()}.
	 */
	@Test
	public void testGetLogPacketsPerSecond() {
		
		Configuration.setFileLocation("src\\test\\resources\\cctv.properties");
		assertEquals(true, Configuration.getLogPacketsPerSecond());
	}

	/**
	 * Test method for {@link net.effanee.Configuration#getRtpClientPacketsPerSecond()}.
	 */
	@Test
	public void testGetRtpClientPacketsPerSecond() {
		
		Configuration.setFileLocation("src\\test\\resources\\cctv.properties");
		assertEquals(450, Configuration.getRtpClientPacketsPerSecond());
	}

	/**
	 * Test method for {@link net.effanee.Configuration#getRtpClientPayloadSize()}.
	 */
	@Test
	public void testGetRtpClientPayloadSize() {
		
		Configuration.setFileLocation("src\\test\\resources\\cctv.properties");
		assertEquals(1460, Configuration.getRtpClientPayloadSize());
	}

	/**
	 * Test method for {@link net.effanee.Configuration#getMaximumDiscSpace()}.
	 */
	@Test
	public void testGetMaximumDiscSpace() {
		
		Configuration.setFileLocation("src\\test\\resources\\cctv.properties");
		assertEquals(1000000000, Configuration.getMaximumDiscSpace());
	}	
	
	/**
	 * Test method for {@link net.effanee.Configuration#getRtcpClientPort()}.
	 */
	@Test
	public void testGetRtcpClientPort() {
		
		Configuration.setFileLocation("src\\test\\resources\\cctv.properties");
		assertEquals(58845, Configuration.getRtcpClientPort());
	}	

	/**
	 * Test method for {@link net.effanee.Configuration#getUserName()}.
	 */
	@Test
	public void testGetUsername() {
		
		Configuration.setFileLocation("src\\test\\resources\\cctv.properties");
		assertEquals("admin", Configuration.getUserName());
	}

	/**
	 * Test method for {@link net.effanee.Configuration#getPassword()}.
	 */
	@Test
	public void testGetPassword() {
		
		Configuration.setFileLocation("src\\test\\resources\\cctv.properties");
		assertEquals("12345", Configuration.getPassword());
	}

	/**
	 * Test method for {@link net.effanee.Configuration#getOutputDirectory()}.
	 * @throws URISyntaxException 
	 */
	@Test
	public void testGetOutputDirectory() throws URISyntaxException {
		
		Path path = FileSystems.getDefault().getPath("c:/Users/fjh/git/CCTV/CCTV/tmp");
		
		Configuration.setFileLocation("src\\test\\resources\\cctv.properties");
		assertEquals(path, Configuration.getOutputDirectory());
	}
	
	/**
	 * Test method for {@link net.effanee.Configuration#getUrlWithCredentials()}.
	 * @throws URISyntaxException 
	 */
	@Test
	public void testGetUrlWithCredentials() throws URISyntaxException {
		
		URI uri = new URI("rtsp://admin:12345@192.168.1.15:554/h264/ch1/main/av_stream");
		
		Configuration.setFileLocation("src\\test\\resources\\cctv.properties");
		assertEquals(uri, Configuration.getUrlWithCredentials());
	}	
}
