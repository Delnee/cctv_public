/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.upnp.discovery;

import static org.junit.Assert.*;

import java.net.InetAddress;
import java.util.List;
import java.util.Map;

import net.effanee.upnp.discovery.Search;

import org.junit.Test;

/**
 * Test UPnP search.
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2015</p>
 */
public class SearchTest {

	/**
	 * Ensure that at least one valid IP address is found.
	 */
	@Test
	public void testGetInterfaceAddresses() {
		
		List<InetAddress> addresses = new Search().getInterfaceAddresses();
		assertTrue(addresses.size() > 0);
	}

	/**
	 * Ensure that at least one valid IPv4 address is found.
	 */
	@Test
	public void testGetIPv4Addresses() {
		
		InetAddress address = new Search().getSiteLocalIPv4Address();
		assertNotNull(address);
	}

	/**
	 * Get servers.
	 */
	@Test
	public void testUpnP() {
		
		Map<String, String>friendlyNames = new Search().getUpnpServers();
		
		for(Map.Entry<String, String> keyValue: friendlyNames.entrySet()){
			System.out.println(keyValue.getKey() + " - " + keyValue.getValue());
		}
	}	
	
}
