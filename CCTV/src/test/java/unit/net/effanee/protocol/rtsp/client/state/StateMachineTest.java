/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.state;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.hamcrest.core.IsInstanceOf;
import org.junit.Test;

/**
 * Tests.
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class StateMachineTest {

	/**
	 * New StateMachines should be initialised to InitState.
	 * 
	 * Test method for {@link net.effanee.protocol.rtsp.client.state.StateMachine#StateMachine()}.
	 */
	@Test
	public void testConstructor() {

		StateMachine machine = new StateMachine();
		
		assertTrue(machine.getState() instanceof InitState);
		assertThat(machine.getState(), IsInstanceOf.instanceOf(InitState.class));
	}

	/**
	 * Transitions:-
	 * 
	 * 	InitState
	 * 		setup()
	 * 	Ready
	 * 		play() 
	 *  Playing
	 * 
	 * Test method for {@link net.effanee.protocol.rtsp.client.state.StateMachine#StateMachine()}.
	 */
	@Test
	public void testTransitions1() {

		StateMachine machine = new StateMachine();
		
		machine.setup();
		machine.play();
		
		assertThat(machine.getState(), IsInstanceOf.instanceOf(PlayingState.class));
	}
	
	/**
	 * Transitions:-
	 * 
	 * 	InitState
	 * 		setup()
	 * 	Ready
	 * 		play() 
	 *  Playing
	 *  	pause()
	 *  Ready
	 * 
	 * Test method for {@link net.effanee.protocol.rtsp.client.state.StateMachine#StateMachine()}.
	 */
	@Test
	public void testTransitions2() {

		StateMachine machine = new StateMachine();
		
		machine.setup();
		machine.play();
		machine.pause();
		
		assertThat(machine.getState(), IsInstanceOf.instanceOf(ReadyState.class));
	}
	
	
	/**
	 * Transitions:-
	 * 
	 * 	InitState
	 * 		setup()
	 * 	Ready
	 * 		play() 
	 *  Playing
	 *  	pause()
	 *  Ready
	 *  	record()
	 *  Recording
	 * 
	 * Test method for {@link net.effanee.protocol.rtsp.client.state.StateMachine#StateMachine()}.
	 */
	@Test
	public void testTransitions3() {

		StateMachine machine = new StateMachine();
		
		machine.setup();
		machine.play();
		machine.pause();
		machine.record();
		
		assertThat(machine.getState(), IsInstanceOf.instanceOf(RecordingState.class));
	}
	
	/**
	 * Transitions:-
	 * 
	 * 	InitState
	 * 		setup()
	 * 	Ready
	 * 		play() 
	 *  Playing
	 *  	pause()
	 *  Ready
	 *  	record()
	 *  Recording
	 *  	teardown()
	 *  InitState
	 * 
	 * Test method for {@link net.effanee.protocol.rtsp.client.state.StateMachine#StateMachine()}.
	 */
	@Test
	public void testTransitions4() {

		StateMachine machine = new StateMachine();
		
		machine.setup();
		machine.play();
		machine.pause();
		machine.record();
		machine.teardown();
		
		assertThat(machine.getState(), IsInstanceOf.instanceOf(InitState.class));
	}
	
	/**
	 * Transitions:-
	 * 
	 * 	InitState
	 * 		setup()
	 * 	Ready
	 * 		play() 
	 *  Playing
	 *  	pause()
	 *  Ready
	 *  	record()
	 *  Recording
	 *  	teardown()
	 *  InitState
	 *  	setup()
	 *  Ready
	 *  	play()
	 *  Playing
	 *  	setup()
	 *  Playing
	 * 
	 * Test method for {@link net.effanee.protocol.rtsp.client.state.StateMachine#StateMachine()}.
	 */
	@Test
	public void testTransitions5() {

		StateMachine machine = new StateMachine();
		
		machine.setup();
		machine.play();
		machine.pause();
		machine.record();
		machine.teardown();
		machine.setup();
		machine.play();
		machine.setup();
		
		assertThat(machine.getState(), IsInstanceOf.instanceOf(PlayingState.class));
	}
	/**
	 * Transitions:-
	 * 
	 * 	InitState
	 * 		setup()
	 * 	Ready
	 * 		play()
	 * 	Playing
	 * 		record()
	 *  IllegalStateException
	 * 
	 * Test method for {@link net.effanee.protocol.rtsp.client.state.StateMachine#StateMachine()}.
	 * @throws Exception 
	 */
	@Test(expected = IllegalStateException.class)
	public void testTransitions6() throws Exception {

		StateMachine machine = new StateMachine();
		
		machine.setup();
		machine.play();
		machine.record();
	}	
}
