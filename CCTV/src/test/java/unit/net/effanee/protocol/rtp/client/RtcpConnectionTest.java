/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtp.client;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneOffset;
import java.util.function.Supplier;

import net.effanee.buffer.RingBuffer;

import org.junit.Test;

/**
 * Test RTCP connection.
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class RtcpConnectionTest {

	private static final String LOCAL_IPV4_ADDRESS = "192.168.1.5";
	private static final int CLIENT_PORT = 58845;
	private static final int SERVER_PORT = 8009;
	private static final int PACKET_SIZE = 1490;
	private static final String CAMERA_LOCATION = "rtsp://admin:12345@192.168.1.5:554/h264/ch1/main/av_stream";
	
	
	private static final byte version = (byte) 0b10000000;
	private static final byte padding = (byte) 0b00000000;
	private static final byte sourceCount = (byte) 0b0000001;
	private static final byte sdes = (byte) 0b11001010;
	
	
	
	/*
	 *
Sender Report
0000   80 c8 00 06 3d 0f 97 27 da 6f 89 50 b7 a4 11 a4  ....=..'.o.P....
0010   65 3d 24 1a ff ff f5 aa 05 4b 17 98              e=$......K..
Source Description
0000   81 ca 00 0a 3d 0f 97 27 01 0e 72 74 63 70 2d 34  ....=..'..rtcp-4
0010   35 38 30 34 35 35 37 35 02 0e 72 74 63 70 2d 34  58045575..rtcp-4
0020   35 38 30 34 35 35 37 35 00 00 00 00              58045575....

80C800063D0F9727DA6F88E0896EF2AF64A3129800000CB300460C3C81CA000A3D0F9727010E727463702D343538303435353735020E727463702D34353830343535373500000000
80C800063D0F9727DA6F88E49BEF2BB864A8AD70000014E20073121081CA000A3D0F9727010E727463702D343538303435353735020E727463702D34353830343535373500000000
80C800063D0F9727DA6F88EB3B3C3FC664B1BE5A0000225B00BD74B881CA000A3D0F9727010E727463702D343538303435353735020E727463702D34353830343535373500000000
80C800063D0F9727DA6F88F6F03440C664C1D7CE00003AC70144228C81CA000A3D0F9727010E727463702D343538303435353735020E727463702D34353830343535373500000000
80C800063D0F9727DA6F88FE567F1D5564CC01D400004A66019A48CC81CA000A3D0F9727010E727463702D343538303435353735020E727463702D34353830343535373500000000
80C800063D0F9727DA6F89090554005164DAADBC000060710213E15881CA000A3D0F9727010E727463702D343538303435353735020E727463702D34353830343535373500000000
	 * 
	 */
	/*
	 * 6.5 SDES: Source Description RTCP Packet
	 * 
	 *        0                   1                   2                   3
	 *        0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
	 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	 * header |V=2|P| SC     | PT=SDES=202   | length                         |
	 *        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
	 * chunk  |                         SSRC/CSRC_1                           |
	 *   1    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	 *        |                         SDES items                            |
	 *        |                             ...                               |
	 *        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
	 * chunk  |                         SSRC/CSRC_2                           |
	 *   2    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	 *        |                         SDES items                            |
	 *        |                             ...                               |
	 *        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
	 */
	private static byte[] getSourceDescription(){
		
		// NTP Epoch start January 1, 1900. Seconds before Java Epoch start
		// January 1, 1970.
		@SuppressWarnings("unused")
		long h = 2208988800l;
		
		// NTP seconds.
		long ntp1 = 3664742736l;

		// NTP fractional apart;
		long ntp2 = 3080982948l;
		
		/*
		 * RFC 868 - Time Protocol
		 * 
		 * The Time
		 * 
		 * The time is the number of seconds since 00:00 (midnight) 1 January
		 * 1900 GMT, such that the time 1 is 12:00:01 am on 1 January 1900 GMT;
		 * this base will serve until the year 2036.
		 * 
		 * For example: 2,208,988,800 corresponds to 00:00 1 Jan 1970 GMT.
		 */
		byte[] sdes1 = "TEST".getBytes(StandardCharsets.UTF_8);
		
		System.out.println(version | padding | sourceCount);
		System.out.println(sdes);
		
		@SuppressWarnings("unused")
		LocalDateTime l1 = LocalDateTime.of(1900, Month.JANUARY, 1, 0, 0);
		LocalDateTime l2 = LocalDateTime.ofEpochSecond(ntp1 - 2208988800l, 0, ZoneOffset.UTC);
		System.out.println(l2);
				
		System.out.println((LocalDateTime.parse("1900-01-01T00:00:00")).toEpochSecond(ZoneOffset.UTC));
		
		
		System.out.println(1000 * ntp2 / 4294967295l);
		
		return sdes1;
	}
	
	/**
	 * Extract a contiguous range of bits from a byte, and return the result as
	 * a byte where the requested bits are right aligned in the new byte.
	 *   
	 * @param b the byte containing the bits.
	 * @param startBit the start position of the first contiguous bit, the left
	 * most bit is numbered 0, positions are inclusive.
	 * @param endBit the start position of the last contiguous bit, the right
	 * most bit is numbered 7, positions are inclusive.
	 * @return the bits, right aligned in the new byte.
	 */
	byte getBits(byte b, int startBit, int endBit){
		
		int bits = 0;
		int mask = getBitMask((endBit - startBit) + 1);
		
		// Right align the bits we want.
		bits = (byte)(b >>> (7 - endBit));
		// Remove ones to the left of the bits we want.
		bits = (bits & mask);
		
		return (byte)bits;
	}
	
	/**
	 * Create a bit mask consisting of right aligned ones. This mask is intended
	 * to be used with 8 bit bytes, so the valid parameter values are 1 - 8. 
	 * 
	 * @param noOfBits a value in the range 1 to 8.
	 * @return a bit mask with the requested number of contiguous ones in the
	 * right most positions.
	 */
	int getBitMask(int noOfBits){
		
		int mask = 0;
		for(int i = 0; i < noOfBits; i++){
			mask = (int) (mask + Math.pow(2, i));
		}
		
		return mask;
	}
	
	/**
	 * Start and stop.
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.RtcpConnection#run()}.
	 * 
	 * @throws URISyntaxException 
	 * @throws IOException 
	 * @throws UnknownHostException 
	 */
	@Test
	public void testRun() throws URISyntaxException, UnknownHostException, IOException {
		
		URI uri = new URI(CAMERA_LOCATION);
		
		RtcpConnection conn = new RtcpConnection(uri, CLIENT_PORT, SERVER_PORT, PACKET_SIZE);
        Thread thread = new Thread(conn);
        thread.start();
		
		try {Thread.sleep(100);}
		catch (InterruptedException e) {e.printStackTrace();}
		assertEquals(Thread.State.RUNNABLE, thread.getState());
		
		DatagramSocket socket = new DatagramSocket(SERVER_PORT);
		socket.setReuseAddress(true);
		socket.setSoTimeout(1000);
		
	    //Get the query and send it.
	    byte[] message = getSourceDescription();
	    socket.send(new DatagramPacket(message, message.length, InetAddress.getLocalHost(), CLIENT_PORT));
	    
	    //Close socket.
	    socket.disconnect();
		socket.close();

		try {Thread.sleep(100);}
		catch (InterruptedException e) {e.printStackTrace();}
		
		System.out.println(new String (conn.getLastMessage(), StandardCharsets.UTF_8));
		
		conn.stop();
		try {Thread.sleep(100);}
		catch (InterruptedException e) {e.printStackTrace();}
		assertEquals(Thread.State.TERMINATED, thread.getState());
	}
	
	/**
	 * Check IP address is found. LAZY!
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.RtpConnection#getIpv4InterfaceAddress()}.
	 * @throws UnknownHostException 
	 * @throws URISyntaxException 
	 */
	@Test
	public void testGetIpv4InterfaceAddress() throws UnknownHostException, URISyntaxException {
		
		URI uri = new URI(CAMERA_LOCATION);
		Supplier<ByteBuffer> generator  = () -> ByteBuffer.allocate(PACKET_SIZE);
		RingBuffer<ByteBuffer> buffer = new RingBuffer<>(1000, generator);
		
		RtpConnection conn = new RtpConnection(uri, CLIENT_PORT, SERVER_PORT, buffer, false);
		InetAddress addr = conn.getIpv4InterfaceAddress();
		
		assertEquals(InetAddress.getByName(LOCAL_IPV4_ADDRESS), addr);
	}
	
}
