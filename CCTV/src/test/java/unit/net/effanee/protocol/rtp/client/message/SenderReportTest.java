/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtp.client.message;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.time.Month;

import javax.xml.bind.DatatypeConverter;

import org.junit.Test;

/**
 * Test SenderReport.
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class SenderReportTest {

	private static final String SR_HEX = "80c800063d0f9727da6f8950b7a411a4653d241afffff5aa054b1798";
	private static final byte[] SR_BYTES =  DatatypeConverter.parseHexBinary(SR_HEX);
	
//	/**
//	 * Is this a Sender Report.
//	 * 
//	 * Test method for {@link net.effanee.protocol.rtp.client.message.SenderReport#isMessageType()}.
//	 */
//	@Test
//	public void testIsMessageType() {
//		
//		RtcpMessage sr = new SenderReport();
//		assertEquals(true, sr.isMessageType(SR_BYTES));
//	}
	
//	/**
//	 * Valid message.
//	 * 
//	 * Test method for {@link net.effanee.protocol.rtp.client.message.SenderReport#handle(byte[])}.
//	 */
//	@Test
//	public void testHandle() {
//		
//		RtcpMessage sr = new SenderReport();
//		sr.handle(SR_BYTES);
//	}

	/**
	 * Too few bytes in header.
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.message.SenderReport#parse(byte[])}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testParse1() {
		
		// 6 bytes.
		final String hex = "40c800063d0f";
		final byte[] bytes =  DatatypeConverter.parseHexBinary(hex);
		
		RtcpMessage sr = new SenderReport();
		sr.parse(bytes);
	}
	
	/**
	 * Wrong version.
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.message.SenderReport#parse(byte[])}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testParse2() {
		
		// Version 1.
		final String hex = "40c800063d0f9727da6f8950b7a411a4653d241afffff5aa054b1798";
		final byte[] bytes =  DatatypeConverter.parseHexBinary(hex);
		
		RtcpMessage sr = new SenderReport();
		sr.parse(bytes);
	}

	/**
	 * Padding is true.
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.message.SenderReport#parse(byte[])}.
	 */
	@Test
	public void testParse3() {
		
		final String hex = "a0c800063d0f9727da6f8950b7a411a4653d241afffff5aa054b1798";
		final byte[] bytes =  DatatypeConverter.parseHexBinary(hex);
		
		RtcpMessage sr = new SenderReport();
		sr.parse(bytes);
		
		assertTrue(sr.isPadded());
	}
	
	/**
	 * Padding is false.
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.message.SenderReport#parse(byte[])}.
	 */
	@Test
	public void testParse4() {
		
		RtcpMessage sr = new SenderReport();
		sr.parse(SR_BYTES);
		
		assertFalse(sr.isPadded());
	}
	
	/**
	 * Count is zero.
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.message.SenderReport#parse(byte[])}.
	 */
	@Test
	public void testParse5() {
		
		RtcpMessage sr = new SenderReport();
		sr.parse(SR_BYTES);
		
		assertEquals(0, sr.getCount());
	}
	
	/**
	 * Count is 16.
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.message.SenderReport#parse(byte[])}.
	 */
	@Test
	public void testParse6() {
		
		final String hex = "b0c800063d0f9727da6f8950b7a411a4653d241afffff5aa054b1798";
		final byte[] bytes =  DatatypeConverter.parseHexBinary(hex);
		
		RtcpMessage sr = new SenderReport();
		sr.parse(bytes);
		
		assertEquals(16, sr.getCount());
	}
	
	/**
	 * Sender Report packet.
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.message.SenderReport#parse(byte[])}.
	 */
	@Test
	public void testParse7() {
		
		RtcpMessage sr = new SenderReport();
		sr.parse(SR_BYTES);
		
		assertEquals(RtcpPacket.SENDER_REPORT, sr.getRtcpPacket());
	}
	
	/**
	 * Null report packet - 205
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.message.SenderReport#parse(byte[])}.
	 */
	@Test
	public void testParse8() {
		
		final String hex = "b0cd00063d0f9727da6f8950b7a411a4653d241afffff5aa054b1798";
		final byte[] bytes =  DatatypeConverter.parseHexBinary(hex);
		
		RtcpMessage sr = new SenderReport();
		sr.parse(bytes);
		
		assertEquals(null, sr.getRtcpPacket());
	}

	/**
	 * Length is 6.
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.message.SenderReport#parse(byte[])}.
	 */
	@Test
	public void testParse9() {
		
		RtcpMessage sr = new SenderReport();
		sr.parse(SR_BYTES);
		
		assertEquals(6, sr.getLength());
	}

	/**
	 * Length is ffff - 65535.
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.message.SenderReport#parse(byte[])}.
	 */
	@Test
	public void testParse10() {
		
		final String hex = "b0cdffff3d0f9727da6f8950b7a411a4653d241afffff5aa054b1798";
		final byte[] bytes =  DatatypeConverter.parseHexBinary(hex);
		
		RtcpMessage sr = new SenderReport();
		sr.parse(bytes);
		
		assertEquals(65535, sr.getLength());
	}
	
	/**
	 * Length is ea60 - 60000.
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.message.SenderReport#parse(byte[])}.
	 */
	@Test
	public void testParse11() {
		
		final String hex = "b0cdea603d0f9727da6f8950b7a411a4653d241afffff5aa054b1798";
		final byte[] bytes =  DatatypeConverter.parseHexBinary(hex);
		
		RtcpMessage sr = new SenderReport();
		sr.parse(bytes);
		
		assertEquals(60000, sr.getLength());
	}
	
	/**
	 * SSRC Id is 5f7f278e - 1602168718
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.message.SenderReport#parse(byte[])}.
	 */
	@Test
	public void testParse12() {
		
		final String hex = "b0cdea605f7f278eda6f8950b7a411a4653d241afffff5aa054b1798";
		final byte[] bytes =  DatatypeConverter.parseHexBinary(hex);
		
		RtcpMessage sr = new SenderReport();
		sr.parse(bytes);
		
		assertEquals(1602168718, sr.getSsrcId());
	}
	
	/**
	 * SSRC Id is 3d0f9727 - 1024431911.
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.message.SenderReport#parse(byte[])}.
	 */
	@Test
	public void testParse13() {
		
		final String hex = "b0cdea603d0f9727da6f8950b7a411a4653d241afffff5aa054b1798";
		final byte[] bytes =  DatatypeConverter.parseHexBinary(hex);
		
		RtcpMessage sr = new SenderReport();
		sr.parse(bytes);
		
		assertEquals(1024431911, sr.getSsrcId());
	}
	
	/**
	 * SSRC Id is 00000000 - 0.
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.message.SenderReport#parse(byte[])}.
	 */
	@Test
	public void testParse14() {
		
		final String hex = "b0cdea6000000000da6f8950b7a411a4653d241afffff5aa054b1798";
		final byte[] bytes =  DatatypeConverter.parseHexBinary(hex);
		
		RtcpMessage sr = new SenderReport();
		sr.parse(bytes);
		
		assertEquals(0, sr.getSsrcId());
	}
	
	/**
	 * SSRC Id is ffffffff - 4294967295.
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.message.SenderReport#parse(byte[])}.
	 */
	@Test
	public void testParse15() {
		
		final String hex = "b0cdea60ffffffffda6f8950b7a411a4653d241afffff5aa054b1798";
		final byte[] bytes =  DatatypeConverter.parseHexBinary(hex);
		
		RtcpMessage sr = new SenderReport();
		sr.parse(bytes);
		
		assertEquals(4294967295l, sr.getSsrcId());
	}

	/**
	 * Too few bytes in body.
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.message.SenderReport#parse(byte[])}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testParse16() {
		
		// 27 bytes.
		final String hex = "b0cdea60ffffffffda6f8950b7a411a4653d241afffff5aa054b17";
		final byte[] bytes =  DatatypeConverter.parseHexBinary(hex);
		
		RtcpMessage sr = new SenderReport();
		sr.parse(bytes);
	}	
	
	/**
	 * getNtpSeconds is da6f8950 - 3664742736.
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.message.SenderReport#parse(byte[])}.
	 */
	@Test
	public void testParse17() {
		
		final String hex = "b0cdea60ffffffffda6f8950b7a411a4653d241afffff5aa054b1798";
		final byte[] bytes =  DatatypeConverter.parseHexBinary(hex);
		
		SenderReport sr = new SenderReport();
		sr.parse(bytes);
		
		assertEquals(3664742736l, sr.getNtpSeconds());
	}
	
	/**
	 * getNtpSecondsAsDate is da730ddd - 3664973277.
	 * 3c654224 - 1013269028
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.message.SenderReport#parse(byte[])}.
	 */
	@Test
	public void testParse18() {
		
		final String hex = "b0cdea60ffffffffda730ddd3c654224653d241afffff5aa054b1798";
		final byte[] bytes =  DatatypeConverter.parseHexBinary(hex);
		
		SenderReport sr = new SenderReport();
		sr.parse(bytes);
		
		// Feb 20, 2016 16:07:57.235920000.
		LocalDateTime localDate = LocalDateTime.of(2016, Month.FEBRUARY, 20, 16, 7, 57);
		assertEquals(localDate, sr.getNtpSecondsAsDate());
	}

	/**
	 * getNtpFractional is 3c654224 - 1013269028
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.message.SenderReport#parse(byte[])}.
	 */
	@Test
	public void testParse19() {
		
		final String hex = "b0cdea60ffffffffda730ddd3c654224653d241afffff5aa054b1798";
		final byte[] bytes =  DatatypeConverter.parseHexBinary(hex);
		
		SenderReport sr = new SenderReport();
		sr.parse(bytes);
		
		// Feb 20, 2016 16:07:57.235920000.
		assertEquals(1013269028, sr.getNtpFractional());
	}
	
	/**
	 * getNtpFractionalAsMilliSeconds is 3c654224 - 1013269028
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.message.SenderReport#parse(byte[])}.
	 */
	@Test
	public void testParse20() {
		
		final String hex = "b0cdea60ffffffffda730ddd3c654224653d241afffff5aa054b1798";
		final byte[] bytes =  DatatypeConverter.parseHexBinary(hex);
		
		SenderReport sr = new SenderReport();
		sr.parse(bytes);
		
		// Feb 20, 2016 16:07:57.235920000.
		assertEquals(235, sr.getNtpFractionalAsMilliSeconds());
	}

	/**
	 * getRtpTimestamp is 39fedf82 - 973004674
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.message.SenderReport#parse(byte[])}.
	 */
	@Test
	public void testParse21() {
		
		final String hex = "b0cdea60ffffffffda730ddd3c65422439fedf82fffff5aa054b1798";
		final byte[] bytes =  DatatypeConverter.parseHexBinary(hex);
		
		SenderReport sr = new SenderReport();
		sr.parse(bytes);
		
		assertEquals(973004674, sr.getRtpTimestamp());
	}
	
	/**
	 * getSendersPacketCount is fffff089 - 4294963337
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.message.SenderReport#parse(byte[])}.
	 */
	@Test
	public void testParse22() {
		
		final String hex = "b0cdea60ffffffffda730ddd3c65422439fedf82fffff089054b1798";
		final byte[] bytes =  DatatypeConverter.parseHexBinary(hex);
		
		SenderReport sr = new SenderReport();
		sr.parse(bytes);
		
		assertEquals(4294963337l, sr.getSendersPacketCount());
	}

	/**
	 * getSendersOctetCount is 052e2fb0 - 86912944
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.message.SenderReport#parse(byte[])}.
	 */
	@Test
	public void testParse23() {
		
		final String hex = "b0cdea60ffffffffda730ddd3c65422439fedf82fffff089052e2fb0";
		final byte[] bytes =  DatatypeConverter.parseHexBinary(hex);
		
		SenderReport sr = new SenderReport();
		sr.parse(bytes);
		
		assertEquals(86912944, sr.getSendersOctetCount());
		System.out.println(sr);
	}
}
