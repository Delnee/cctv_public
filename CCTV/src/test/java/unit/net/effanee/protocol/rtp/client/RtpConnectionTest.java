/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtp.client;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.List;
import java.util.function.Supplier;

import net.effanee.buffer.RingBuffer;
import net.effanee.packetize.rtp.RtpPacket;

import org.junit.Test;

/**
 * Tests.
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class RtpConnectionTest {

	private static final Path rtpPath = FileSystems.getDefault().getPath("./src/test/resources/2016-05-19T14-18-30.rtp");
	private static final int CLIENT_PORT = 58844;
	private static final int SERVER_PORT = 8008;
	private static final int PACKET_SIZE = 1460;
	private static final String CAMERA_LOCATION = "rtsp://admin:12345@192.168.1.15:554/h264/ch1/main/av_stream";
	
	/**
	 * Start and stop against test data.
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.RtpConnection#run()}.
	 */
	@Test
	public void testRun() throws URISyntaxException, IOException {
		
		int sendInBatchesOf = 400;
		
		// Get the RTP packets from the test file.
		List<RtpPacket>rtpPackets = new RtpFileReader().getRtpPackets(rtpPath);
		int expectedPackets = rtpPackets.size();
		
		// Receive RTP packets.	
		URI uri = new URI("rtsp://" + Inet4Address.getLocalHost());
		Supplier<ByteBuffer> generator  = () -> ByteBuffer.allocate(PACKET_SIZE);
		RingBuffer<ByteBuffer> buffer = new RingBuffer<>(expectedPackets + 10, generator);
		
		RtpConnection conn = new RtpConnection(uri, CLIENT_PORT, SERVER_PORT, buffer, true);
        Thread thread = new Thread(conn);
        thread.start();
        
        // Pause to give chance to start up.
		try {Thread.sleep(1000);}
		catch (InterruptedException e) {e.printStackTrace();}
		assertEquals(Thread.State.RUNNABLE, thread.getState());
		
		// Send RTP packets.
		DatagramSocket datagramSocket = null;
		try{
			datagramSocket = new DatagramSocket(SERVER_PORT);
			InetAddress clientAddress = InetAddress.getLocalHost();
			DatagramPacket packet = null;
			
			int countSent = 0;
			for(RtpPacket rtpPacket: rtpPackets){
				
				// Pause to ensure the required number of packets per second
				// are sent.
				if(countSent++ % sendInBatchesOf == 0){
					
					try {Thread.sleep(1000);}
					catch (InterruptedException e) {e.printStackTrace();}
				}
				
				packet = new DatagramPacket(rtpPacket.getPayload(),
											rtpPacket.getPayload().length,
											clientAddress,
											CLIENT_PORT);
				datagramSocket.send(packet);
			}
		}
		catch(IOException e){
			e.printStackTrace();
			throw e;
		}
		finally{
			datagramSocket.close();
		}
		
		assertEquals(sendInBatchesOf, conn.getPacketsPerSecond());
		conn.stop();
	}
	
	/**
	 * Start and stop against live camera.
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.RtpConnection#run()}.
	 * @throws URISyntaxException 
	 */
	@Test
	public void testRunLive() throws URISyntaxException {
		
		URI uri = new URI(CAMERA_LOCATION);
		Supplier<ByteBuffer> generator  = () -> ByteBuffer.allocate(PACKET_SIZE);
		RingBuffer<ByteBuffer> buffer = new RingBuffer<>(1000, generator);
		
		RtpConnection conn = new RtpConnection(uri, CLIENT_PORT, SERVER_PORT, buffer, false);
        Thread thread = new Thread(conn);
        thread.start();
        
		try {Thread.sleep(1000);}
		catch (InterruptedException e) {e.printStackTrace();}
		assertEquals(Thread.State.RUNNABLE, thread.getState());
		
		conn.stop();
		try {Thread.sleep(1000);}
		catch (InterruptedException e) {e.printStackTrace();}
		assertEquals(Thread.State.TERMINATED, thread.getState());
	}

	/**
	 * Check IP address is found. LAZY!
	 * 
	 * Test method for {@link net.effanee.protocol.rtp.client.RtpConnection#getIpv4InterfaceAddress()}.
	 * @throws UnknownHostException 
	 * @throws URISyntaxException 
	 */
	@Test
	public void testGetIpv4InterfaceAddress() throws UnknownHostException, URISyntaxException {
		
		URI uri = new URI(CAMERA_LOCATION);
		Supplier<ByteBuffer> generator  = () -> ByteBuffer.allocate(PACKET_SIZE);
		RingBuffer<ByteBuffer> buffer = new RingBuffer<>(1000, generator);
		
		RtpConnection conn = new RtpConnection(uri, CLIENT_PORT, SERVER_PORT, buffer, false);
		InetAddress addr = conn.getIpv4InterfaceAddress();
		
		assertEquals(Inet4Address.getLocalHost(), addr);
	}
}