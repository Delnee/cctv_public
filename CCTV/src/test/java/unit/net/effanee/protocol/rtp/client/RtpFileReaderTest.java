/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtp.client;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.List;

import net.effanee.packetize.h264.H264Depacketize;
import net.effanee.packetize.h264.H264Packet;
import net.effanee.packetize.rtp.RtpPacket;

import org.junit.Test;

/**
 * 
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class RtpFileReaderTest {

	private static final Path filePath =
			FileSystems.getDefault().getPath(
			"./src/test/resources/2016-05-19T14-18-30.rtp");
	
	@SuppressWarnings("unused")
	@Test
	public void test() {

		List<RtpPacket>rtpPackets = new RtpFileReader().getRtpPackets(filePath);
		List<H264Packet> h264Packets = new H264Depacketize().getDefragmentedPackets(rtpPackets);
	}

	
//	/**
//	 * ??
//	 * 
//	 * Test method for {@link net.effanee.protocol.rtp.client.RtpFileReader#doit(Path)}.
//	 */
//	@Test
//	public void testLargeFile() {
//
//		Path filePath =FileSystems.getDefault().getPath("./tmp/2016-04-11T16-56-52.h264");
//		new RtpFileReader().doit(filePath);;
//	
//		filePath =FileSystems.getDefault().getPath("./tmp/2016-04-11T16-57-48.h264");
//		new RtpFileReader().doit(filePath);;		
//		
//	}
	
	
	
	
	
	
}
