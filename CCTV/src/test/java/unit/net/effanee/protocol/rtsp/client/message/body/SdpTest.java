/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.protocol.rtsp.client.message.body;

import static org.junit.Assert.assertEquals;
import net.effanee.protocol.rtsp.client.message.body.Sdp;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * SDP tests.
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class SdpTest {

	private static final String[] MESSAGE = new String[]{
			"v=0\r\n", 
			"o=- 1407352941213957 1407352941213957 IN IP4 192.168.1.15\r\n", 
			"s=Media Presentation\r\n", 
			"e=NONE\r\n", 
			"b=AS:5050\r\n", 
			"t=0 0\r\n", 
			"a=control:rtsp://192.168.1.15:554/h264/ch1/main/av_stream/\r\n", 
			"m=video 0 RTP/AVP 96\r\n", 
			"b=AS:5000\r\n", 
			"a=control:rtsp://192.168.1.15:554/h264/ch1/main/av_stream/trackID=1\r\n", 
			"a=rtpmap:96 H264/90000\r\n", 
			"a=fmtp:96 profile-level-id=420029; packetization-mode=1; sprop-parameter-sets=Z00AKZpmA8ARPyzUBAQFAAADA+gAAMNQBA==,aO48gA==\r\n", 
			"a=Media_header:MEDIAINFO=494D4B48010100000400010000000000000000000000000000000000000000000000000000000000;\r\n", 
			"a=appversion:1.0",
	};
	
	private Sdp sdp;
	
	@Before
	public void setUp() throws Exception {
		
		sdp = new Sdp(MESSAGE);
	}

	@After
	public void tearDown() throws Exception {
		
		sdp = null;
	}

	@Test
	public void test() {
		
		int mode = sdp.getMediaSections().get(0).getPacketizationMode();
		assertEquals(1, mode);
	}

}
