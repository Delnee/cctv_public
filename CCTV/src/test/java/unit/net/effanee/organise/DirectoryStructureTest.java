/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.organise;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests.
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2016</p>
 */
public class DirectoryStructureTest {

	private static final Logger LOGGER = Logger.getLogger(DirectoryStructureTest.class.getName());
	
	private static final String BASE_DIRECTORY = "./src/test/tmp";
	private static final String TEST_FILE = "2016-05-19T14-18-30.rtp";
	private static final String FRIENDLY_NAME = "GarageFront";
	private static final String H264_FILE_EXTENSION = ".h264";
	
	private final Path baseDirectory = FileSystems.getDefault().getPath(BASE_DIRECTORY);
	private final Path testFile = FileSystems.getDefault().getPath("./src/test/resources/" + TEST_FILE);
	
	@Before
	public void setup() throws Exception {
		
		// Create directory.
	    if(!baseDirectory.toFile().exists()){
	    	baseDirectory.toFile().mkdirs();
	    }
	}
	
	@After
	public void tearDown() throws Exception {
		
		// Clear directory.
		List<FileAttributes> files = getFiles(baseDirectory);
		deleteFiles(files, baseDirectory);
	}	
	
	/**
	 * Delete the supplied files and any parent directories that are empty.
	 * 
	 * @param files - the list of files to delete.
	 */
	private void deleteFiles(List<FileAttributes> files, Path baseDirectory){
		
		Path filePath = null;
		File file = null;
		File parentDirectory = null;
		
		for(FileAttributes fileAttrs: files){
			
			// Get the file and delete it.
			filePath = FileSystems.getDefault().getPath(fileAttrs.getPath());
			file = filePath.toFile();
			parentDirectory = file.getParentFile();
			file.delete();
			
			// Delete any empty parent directories.
			while(parentDirectory.isDirectory() && parentDirectory.list().length == 0){
				
				// Don't delete the base directory.
				if(!parentDirectory.equals(baseDirectory.toFile())){
					//System.out.println(parentDirectory + " = " + baseDirectory);
					parentDirectory.delete();
				}
				parentDirectory = parentDirectory.getParentFile();
			}
		}
    }
	
	/**
	 * Get a list of files under the supplied directory.
	 * 
	 * @param baseDirectory - the directory to search under.
	 * @return a list of file attributes, for the files under the supplied path.
	 */
	private List<FileAttributes> getFiles(Path baseDirectory){
		
		final List<FileAttributes> files = new ArrayList<>();
		
        try {
			Files.walkFileTree(baseDirectory, new SimpleFileVisitor<Path>(){

				@Override
				public FileVisitResult visitFile(Path file,
						BasicFileAttributes attrs) throws IOException {
					
					// Store the file attributes.
					files.add(new FileAttributes(attrs.creationTime().toMillis(), file.toString(), attrs.size()));
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFileFailed(Path file,
						IOException e) throws IOException {
					
					LOGGER.log(Level.SEVERE, file + " could not be visited.", e);
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult postVisitDirectory(Path dir,
						IOException e) throws IOException {

					if (e != null){
						LOGGER.log(Level.SEVERE, "The iteration of the directory " + dir + " terminated prematurely.", e);
					}
			        return FileVisitResult.CONTINUE;
				}
			});
		}
		catch (IOException e) {
			LOGGER.log(Level.SEVERE, e.getMessage());
		}
        
        return files;
    }

	/**
	 * Get a new file.
	 * 
	 * Test method for {@link net.effanee.organise.DirectoryStructure#getFile(Path, String, String)}.
	 */
	@Test
	public void testGetFile() throws IOException {

		Path file = new DirectoryStructure().getFile(baseDirectory, TEST_FILE, FRIENDLY_NAME);
		Path fileExpected = FileSystems.getDefault().getPath(BASE_DIRECTORY + "/2016/05/19/14/" + FRIENDLY_NAME + H264_FILE_EXTENSION);
		
		assertEquals(fileExpected.toString(), file.toString());
	}
	
	/**
	 * Directory size.
	 * 
	 * Test method for {@link net.effanee.organise.DirectoryStructure#getDirectorySize(Path)}.
	 * @throws InterruptedException 
	 */
	@Test
	public void testGetDirectorySize() throws IOException, InterruptedException {

		// Copy the test file.
		Path file = FileSystems.getDefault().getPath(BASE_DIRECTORY + "/" + FRIENDLY_NAME);
		Files.copy(testFile, file);
		Thread.sleep(100);
		
		assertEquals(testFile.toFile().length(), new DirectoryStructure().getDirectorySize(baseDirectory));
	}
	
	/**
	 * Store the file attributes of interest.
	 *
	 * @author Francis J. Hammell [hammell.francis@gmail.com]
	 * <p><em>Copyright</em>
	 * \u00A9 Francis J. Hammell 2016</p>
	 */
	private class FileAttributes{
		
		private final long creationTime;
		private final String path;
		private final long size;
		
		FileAttributes(long creationTime, String path, long size){
			
			this.creationTime = creationTime;
			this.path = path;
			this.size = size;
		}

		@SuppressWarnings("unused")
		long getCreationTime() {
			return creationTime;
		}

		String getPath() {
			return path;
		}

		@SuppressWarnings("unused")
		long getSize() {
			return size;
		}
		
		@Override
		public String toString() {
			return "FileAttributes ["
					+ ("creationTime=" + creationTime + ",")
					+ ("path=" + path + ",")
					+ ("size=" + size)
					+ "]";
		}
	}
	// End FileAttributes class.
}
