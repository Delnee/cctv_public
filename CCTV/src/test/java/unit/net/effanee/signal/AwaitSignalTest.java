/**
 * <p>This file is part of CCTV.</p>
 *
 * <p>CCTV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>CCTV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with CCTV.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package net.effanee.signal;

import static org.junit.Assert.*;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Test;

/**
 * Test {@link net.effanee.signal.AwaitSignal}. class.
 *
 * @author Francis J. Hammell [hammell.francis@gmail.com]
 * <p><em>Copyright</em>
 * \u00A9 Francis J. Hammell 2015</p>
 */
public class AwaitSignalTest {

	private static final Logger LOGGER = Logger.getLogger(AwaitSignalTest.class.getName());
	
	private volatile int threadEndCount = 0;

	/**
	 * Ensure that threads get blocked, and that all get woken and terminate.
	 */
	@Test
	public void testWaitOnSignal1() throws InterruptedException{
		
		// Create 4 threads to wait on signal.
		ThreadGroup threadGroup = new ThreadGroup("TestThread");
		
		Thread thread = null;
		for(int i = 0; i < 4; i++){
			thread = new Thread(threadGroup, new AwaitSignalTest.TestThread(), "Thread " + i);
			thread.setDaemon(false);
			thread.start();
			threadEndCount++;
		}
		Thread.sleep(500);
		
		// Check all threads are running.
		assertEquals(threadGroup.activeCount(), threadEndCount);
				
		// Wake up threads.
		AwaitSignal.issueSigInt();
		Thread.sleep(500);
		
		// Check all threads are terminated.
		assertEquals(threadGroup.activeCount(), threadEndCount);
	}

	/**
	 * Ensure that multiple signal requests don't cause an exception.
	 */
	@Test
	public void testWaitOnSignal2() throws InterruptedException{
		
		// Create 4 threads to wait on signal.
		ThreadGroup threadGroup = new ThreadGroup("TestThread");
		
		Thread thread = null;
		for(int i = 0; i < 4; i++){
			thread = new Thread(threadGroup, new AwaitSignalTest.TestThread(), "Thread " + i);
			thread.setDaemon(false);
			thread.start();
			threadEndCount++;
		}
		Thread.sleep(500);
		
		// Check all threads are running.
		assertEquals(threadGroup.activeCount(), threadEndCount);
				
		// Wake up threads.
		AwaitSignal.issueSigInt();
		AwaitSignal.issueSigInt();
		AwaitSignal.issueSigInt();
		AwaitSignal.issueSigInt();
		Thread.sleep(500);
		
		// Check all threads are terminated.
		assertEquals(threadGroup.activeCount(), threadEndCount);
	}

	/**
	 * Miscellaneous test thread.
	 */
	public class TestThread implements Runnable {

		public void run() {
			
			LOGGER.log(Level.FINE, "Instantiated " + Thread.currentThread().getName());
			
			int exitStatus = AwaitSignal.waitOnSignal();
			assertEquals(AwaitSignal.GOOD_EXIT, exitStatus);
			
			--threadEndCount;
			LOGGER.log(Level.FINE, "Exit status for " + Thread.currentThread().getName() + " was " + exitStatus);
		}
	}
}
