@echo off
echo Running CCTV Application in Separate Command Window...

rem ### About ###
rem Run the CCTV application. 
rem Used during development, since the Eclipse Console does not allow "Ctrl + c" to be issued,
rem which is used to exit the application cleanly.

rem ### Configuration ###
set JAR_NAME=cctv.jar
set APP_PATH=C:/Users/fjh/git/CCTV/CCTV/target
set JAVA_JDK=C:/Program Files/Java/jdk1.8.0_31
set JAVA_EXE=%JAVA_JDK%/bin/java.exe
set JAVA_CLASS_PATH=%APP_PATH%/%JAR_NAME%;./config;./lib/*
set JAVA_START_CLASS=net.effanee.upnp.discovery.Search
set JAVA_VM_ARGS=-server
set JAVA_PROP_ARGS=-Djava.net.preferIPv4Stack=true -Djava.util.logging.config.file=./config/logging.properties

rem ### Set Console Window Size and Start ###
rem ### Must change into the application directory before starting, so that relative file paths work. ###
start cmd.exe /c "mode con: cols=160 lines=78 & cd "%APP_PATH%" & "%JAVA_EXE%" %JAVA_VM_ARGS% %JAVA_PROP_ARGS% -cp %JAVA_CLASS_PATH% %JAVA_START_CLASS% & pause"

rem /c will close the window after the commands have been executed.
rem /k will wait after the commands have been executed, useful for seeing the last logging messages, instead of using pause.

rem ### NOTES ###
rem START "runas /user:administrator" cmd /K "dir"